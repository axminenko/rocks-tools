=== Plugin Name ===
Contributors: axminenko
Tags: tools, template, theme, wordpress, shortcodes, sidebars, icons
Requires at least: 3.8
Tested up to: 4.4
Stable tag: 0.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A powerful plugin to extend functionality to your WordPress theme offering shortcodes, font icons and useful widgets.

== Description ==

A powerful plugin to extend functionality to your WordPress theme offering shortcodes, font icons and useful widgets.

== Installation ==

You can use automatic installer or install it manually:

1. Extract and upload file contents `rocks-tools.zip` to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress Admin Area.

== Changelog ==

= 1.0.0 =
Initial Release
