<?php
/**
 * Plugin Name: Create.Rocks Tools
 * Plugin URI: http://create.rocks/plugin/tools
 * Description: A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * License: GPLv2 or later
 *
 * Version: 0.1.0
 * Author: the Create.Rocks team
 * Author URI: https://create.rocks
 * Author Email: support@create.rocks
 *
 * Text Domain: rocks
 * Domain Path: /languages/
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
*/

// Disable direct access
if ( ! function_exists( 'add_action' ) ) {
    echo 'Hi there! I\'m just a plugin, not much I can do when called directly.';
    exit;
}

// Plugin update checker
require plugin_dir_path( __FILE__ ) . 'vendor/plugin-update-checker/plugin-update-checker.php';

/**
 * Create.Rocks Tools class
 *
 * @package Create_Rocks_Tools
 */
class Rocks_Tools {
    /**
     * Plugin version
     *
     * @var    string
     * @access protected
     */
    protected $__version = '0.1.0';

    /**
     * All theme flags
     *
     * @var    array
     * @access private
     */
    private $__flags = null;

    /**
     * List of allowed by theme shortcodes
     *
     * @var    array
     * @access private
     */
    private $__shortcodes = null;

    /**
     * Icon fonts objects
     *
     * @var    arrays
     * @access private
     */
    private $__icon_fonts = array( );

    /**
     * Constructor loads components and adds required WordPress actions
     *
     * @param  array $flags array of theme flags
     * @access public
     */
    function __construct( $flags = null ) {
        if ( $flags !== null and is_array( $flags ) ) {
            $this->__flags = $flags;
        }

        // Load needed components
        $this->__load_components( );

        // Load icon fonts
        $this->__load_icon_fonts( );

        // Load shortcodes
        $this->__load_shortcodes( );

        // Localization
        add_action( 'plugins_loaded', array( &$this, 'load_textdomain' ) );
    }

    /**
     * Load plugin localization
     *
     * @access public
     */
    public function load_textdomain( ) {
        load_plugin_textdomain( 'rocks', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }

    /**
     * Get the plugin version
     *
     * @return string
     * @access public
     */
    public function get_version( ) {
        return $this->__version;
    }

    /**
     * Check updates
     *
     * @access public
     * @static
     */
    public static function check_updates( ) {
        if ( class_exists( 'PucFactory' ) ) {
            PucFactory::buildUpdateChecker( 'http://update.create.rocks/?action=get_metadata&slug=rocks-tools', __FILE__ );
        }
    }

    /**
     * Check whether the theme option
     *
     * @return boolean
     * @access public
     */
    public function theme_support( ) {
        if ( func_num_args( ) < 1 ) {
            return false;
        }

        $args = func_get_args( );
        $args = array_reverse( $args );

        return $this->__theme_support( $this->__flags, $args );
    }

    /**
     * Check whether the theme option (recurse)
     *
     * @param  array $flags
     * @param  array $args
     * @return boolean
     * @access private
     */
    private function __theme_support( $flags, $args ) {
        if ( count( $args ) > 0 ) {
            foreach ( $args as $key => $arg ) {
                unset( $args[$key] );

                if ( isset( $flags[$arg] ) ) {
                    return $this->__theme_support( $flags[$arg], $args );
                } else if ( in_array( $arg, $flags ) ) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if ( is_array( $flags ) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get theme option
     *
     * @param  string $option
     * @param  string $default
     * @return string
     * @access public
     */
    public function get_option( $option, $default = false ) {
        if ( array_key_exists( 'options', $this->__flags ) and array_key_exists( $option, $this->__flags['options'] ) ) {
            return $this->__flags['options'][$option];
        }

        return $default;
    }

    /**
     * Get shortcodes list
     *
     * @return boolean|array
     * @access public
     */
    public function get_shortcodes( ) {
        if ( $this->__shortcodes === null ) {
            return false;
        }

        return $this->__shortcodes;
    }

    /**
     * Check whether the theme shortcode
     *
     * @param  string $name
     * @return boolean
     * @access public
     */
    public function theme_support_shortcode( $name ) {
        if ( ! is_array( $this->__shortcodes ) ) {
            return false;
        }

        $prefix = $this->get_option( 'shortcodes-prefix', '' );

        if ( ! empty( $prefix ) ) {
            $name = str_replace( $prefix, '', $name );
        }

        return in_array( $name, $this->__shortcodes );
    }

    /**
     * Load needed components
     *
     * @return boolean
     * @access private
     */
    private function __load_components( ) {
        if ( $this->__flags === null or ! $this->theme_support( 'components' ) ) {
            return false;
        }

        return $this->__load_components_recurse( $this->__flags['components'], plugin_dir_path( __FILE__ ) . 'components/' );
    }

    /**
     * Load needed components (recurse)
     *
     * @param  array  $components
     * @param  string $location
     * @param  string $previous
     * @return boolean
     * @access private
     */
    private function __load_components_recurse( $components = null, $location = null, $previous = null ) {
        if ( is_array( $components ) and count( $components ) > 0 ) {
            foreach ( $components as $key => $value ) {
                $path     = is_array( $value ) ? $key : $value;
                $file     = realpath( $location . $path . '/' . $path . '.php' );
                $file_alt = realpath( $location . $path . '.php' );

                if ( $file !== false ) {
                    include $file;
                } else if ( $file_alt !== false ) {
                    include $file_alt;
                }

                // Ignore shortcodes
                if ( $previous == 'shortcodes' ) {
                    continue;
                }

                if ( is_array( $value ) ) {
                    $this->__load_components_recurse( $value, $location . $path . '/', $path );
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Load shortcodes
     *
     * @return boolean
     * @access private
     */
    private function __load_shortcodes( ) {
        if ( ! $this->theme_support( 'shortcodes', 'components' ) ) {
            return false;
        }

        $this->__shortcodes = &$this->__flags['components']['shortcodes'];

        return true;
    }

    /**
     * Check hex color code
     *
     * @param  string $color
     * @return boolean
     * @access public
     */
    public static function check_color( $color ) {
        if ( preg_match( '/^#[a-f0-9]{6}$/i', $color ) ) {
            return true;
        }

        return false;
    }

    /**
     * Get file body (local files only)
     *
     * @param  string $filepath
     * @return bool|string
     * @access public
     */
    public function get_file_body( $filepath ) {
        $function = 'file' . '_get_contents';

        return $function( $filepath );
    }

    /**
     * Get featured or first image src from post
     *
     * @param  WP_Post $post
     * @param  string  $default
     * @return string
     * @access public
     * @static
     */
    public static function get_post_image_src( $post = null ) {
        $post     = ( $post === null ) ? get_post( ) : $post;
        $featured = get_post_thumbnail_id( $post->ID );

        if ( ! empty( $featured ) ) {
            return wp_get_attachment_image_src( $featured, 'full' );
        }

        if ( preg_match( '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches ) ) {
            $original = $matches[0];
            $image    = array(
                0 => $matches[1],
                1 => '',
                2 => '',
                3 => false,
            );

            if ( preg_match( '/width=[\'"](\d+)[\'"]/i', $original, $matches ) ) {
                $image[1] = $matches[1];
            }

            if ( preg_match( '/height=[\'"](\d+)[\'"]/i', $original, $matches ) ) {
                $image[2] = $matches[1];
            }

            return $image;
        }

        return false;
    }

    /**
     * Get featured or first image from post
     *
     * @param  WP_Post $post
     * @param  string  $alt
     * @return string
     * @access public
     * @static
     */
    public static function get_post_image( $post = null, $alt = '' ) {
        $image = self::get_post_image_src( $post );

        if ( $image ) {
            return '<img src="' . esc_url( $image[0] ) . '"' . ( ! empty( $image[1] ) ? ' width="' . absint( $image[1] ) . '"' : '' ) . ( ! empty( $image[2] ) ? ' height="' . absint( $image[2] ) . '"' : '' ) . ' alt="' . esc_attr( $alt ) . '">';
        }

        return false;
    }

    /**
     * Visual Composer support
     *
     * @return boolean
     * @access public
     */
    public function vc_support( ) {
        return ( $this->theme_support( 'shortcodes', 'components' ) && function_exists( 'vc_map' ) );
    }

    /**
     * Format class name
     *
     * @param  string $string
     * @return string
     * @access public
     */
    public function get_formatted_name( $string ) {
        return ucwords( str_replace( '-', '_', $string ), '_' );
    }

    /**
     * Load icon fonts
     *
     * @access private
     */
    private function __load_icon_fonts( ) {
        if ( ! $this->theme_support( 'icon-fonts', 'components' ) ) {
            return;
        }

        $forced = $this->get_option( 'icon-fonts' );

        sort( $this->__flags['components']['icon-fonts'] );

        foreach ( $this->__flags['components']['icon-fonts'] as $library ) {
            $class = 'Rocks_Tools_Icon_Fonts_' . $this->get_formatted_name( $library );

            if ( class_exists( $class ) ) {
                $safe_name = str_replace( '-', '_', $library );
                $this->__icon_fonts[$library] = new $class( $library );

                if ( $forced !== false ) {
                    if ( ( is_array( $forced ) and in_array( $library, $forced ) ) or ( ! is_array( $forced ) and $forced == $library ) ) {
                        $this->__icon_fonts[$library]->wp_enqueue( );
                    }
                }

                if ( $this->vc_support( ) ) {
                    add_filter( 'vc_iconpicker-type-' . $library, array( &$this->__icon_fonts[$library], 'get_vc_icons' ), 11 );
                }
            }
        }
    }

    /**
     * Get icon font
     *
     * @param  string $library
     * @return mixed
     * @access public
     */
    public function get_icon_font( $library = '' ) {
        if ( empty( $library ) ) {
            if ( $forced = $this->get_option( 'icon-fonts' ) ) {
                if ( is_array( $forced ) ) {
                    $library = reset( $forced );
                } else {
                    $library = $forced;
                }
            } else {
                return false;
            }
        }

        if ( array_key_exists( $library, $this->__icon_fonts ) ) {
            return $this->__icon_fonts[$library];
        }

        return false;
    }

    /**
     * Get array of icon fonts
     *
     * @return array
     * @access public
     */
    public function get_icon_fonts( ) {
        return $this->__icon_fonts;
    }

    /**
     * Enqueue icon fonts
     *
     * @param  boolean|string $by_name (false or name)
     * @param  boolean|string $area (false, wp, admin)
     * @return boolean
     * @access public
     */
    public function icon_fonts_enqueue( $by_name = false, $area = false ) {
        $list = array( );

        if ( $by_name ) {
            if ( array_key_exists( $by_name, $this->__icon_fonts ) ) {
                $list = array( $by_name => $this->__icon_fonts[$by_name] );
            }
        } else {
            $list = &$this->__icon_fonts;
        }

        if ( count( $list ) > 0 ) {
            foreach ( $list as $object ) {
                if ( $area == 'wp' ) {
                    $object->wp_enqueue( );
                } else if ( $area == 'admin' ) {
                    $object->admin_enqueue( );
                } else {
                    $object->direct_enqueue( );
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Get info about the social (by URL)
     */
    public function get_social_by_url( $url ) {
        $networks = array(
            'facebook'    => '',
            'twitter'     => '',
            'google-plus' => 'plus.google',
            'behance'     => '',
            'linkedin'    => '',
            'github'      => '',
            'vimeo'       => '',
            'youtube'     => 'youtu',
            'soundcloud'  => '',
            'lastfm'      => 'last.fm',
            'instagram'   => '',
            'pinterest'   => '',
            'dribbble'    => '',
            'flickr'      => '',
            'tumblr'      => '',
            'deviantart'  => '',
            'digg'        => '',
            'foursquare'  => '',
            'medium'      => '',
            'reddit'      => '',
        );

        $names = array(
            'linkedin'    => __( 'LinkedIn', 'rocks' ),
            'soundcloud'  => __( 'SoundCloud', 'rocks' ),
            'youtube'     => __( 'YouTube', 'rocks' ),
            'deviantart'  => __( 'DeviantArt', 'rocks' ),
            'google-plus' => __( 'Google+', 'rocks' ),
            'lastfm'      => __( 'Last.fm', 'rocks' ),
            'stumbleupon' => __( 'StumbleUpon', 'rocks' ),
        );

        $url = strtolower( $url );

        foreach ( $networks as $id => $address ) {
            if ( substr_count( $url, ( empty( $address ) ? $id : $address ) ) > 0 ) {
                return array(
                    'id'    => $id,
                    'label' => array_key_exists( $id, $names ) ? $names[$id] : ucwords( str_replace( '-', ' ', $id ) ),
                );
            }
        }

        return false;
    }
}

// Initialization
if ( file_exists( get_template_directory( ) . '/components/theme-flags.php' ) ) {
    require_once get_template_directory( ) . '/components/theme-flags.php';

    if ( isset( $theme_flags ) and is_array( $theme_flags ) ) {
        // Global access
        global $rocks_tools;

        // Instance
        $rocks_tools = new Rocks_Tools( $theme_flags );

        // Hook
        do_action( 'rocks_tools_loaded', $rocks_tools );
    }
}

// Check updates
Rocks_Tools::check_updates( );

// @Alya, Thank you so much I so love you <3
// Timestamp: 4/29/2015 10:46PM GMT+3 :)
