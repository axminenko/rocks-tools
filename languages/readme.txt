Official info about translation: http://codex.wordpress.org/Translating_WordPress

In two words: open default.po file and save new copy as rocks-es_ES.po for Spanish.
And then use translating program (e.g. Poedit) to translate it to Spanish language.