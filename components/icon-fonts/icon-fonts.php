<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Icon fonts base class
 *
 * @package    Create_Rocks_Tools
 * @subpackage Icon_Fonts
 */
class Rocks_Tools_Icon_Fonts {
	/**
	 * Font version
	 *
	 * @var    string
	 * @access public
	 */
	public $version;

	/**
	 * Font name
	 *
	 * @var    string
	 * @access private
	 */
	public $name;

	/**
	 * Stylesheet path
	 *
	 * @var    string
	 * @access public
	 */
	public $stylesheet;

	/**
	 * Allow deregister stylesheet
	 *
	 * @var    string
	 * @access public
	 */
	public $deregister = false;

	/**
	 * Default icon name
	 * 
	 * @var    string
	 * @access public
	 */
	public $default_icon = '';

	/**
	 * Classes before icon name
	 * 
	 * @var    string
	 * @access public
	 */
	public $classes_before = '';

	/**
	 * Icons list
	 *
	 * @var    string
	 * @access public
	 */
	public $icons_list;

	/**
	 * Constructor
	 *
	 * @var    string $name
	 * @access public
	 */
	function __construct( $name ) {
		$this->name = $name;
	}

	/**
	 * Get font name
	 * 
	 * @return string
	 * @access public
	 */
	public function get_name( ) {
		return $this->name;
	}

	/**
	 * Get default icon name
	 * 
	 * @return string
	 * @access public
	 */
	public function get_default_icon( ) {
		return $this->default_icon;
	}

	/**
	 * Get classes before icon name
	 * 
	 * @return string
	 * @access public
	 */
	public function get_classes_before( ) {
		return $this->classes_before;
	}

	/**
	 * Deregister another stylesheets
	 * 
	 * @return string
	 * @access public
	 */
	public function deregister( ) {
		if ( $this->deregister ) {
			if ( wp_style_is( $this->get_name( ), 'registered' ) ) {
				wp_deregister_style( $this->get_name( ) );
			}

			if ( wp_style_is( 'vc_' . $this->get_name( ), 'registered' ) ) {
				wp_deregister_style( 'vc_' . $this->get_name( ) );
			}
		}
	}

	/**
	 * Register stylesheet
	 * 
	 * @return string
	 * @access public
	 */
	public function direct_enqueue( ) {
		$this->deregister( );

		wp_enqueue_style( $this->name, plugins_url( $this->name . '/' . $this->stylesheet, __FILE__ ), false, $this->version );
	}

	/**
	 * Register stylesheet for user area
	 * 
	 * @return string
	 * @access public
	 */
	public function wp_enqueue( ) {
		add_action( 'wp_enqueue_scripts', array( &$this, 'direct_enqueue' ) );
	}

	/**
	 * Register stylesheet for admin area
	 * 
	 * @return string
	 * @access public
	 */
	public function admin_enqueue( ) {
		add_action( 'admin_enqueue_scripts', array( &$this, 'direct_enqueue' ) );
	}

	/**
	 * Get icons list
	 * 
	 * @return string
	 * @access public
	 */
	public function get_icons( ) {
		return explode( ' ', $this->icons_list );
	}

	/**
	 * Get icons list for Visual Composer
	 *
	 * @return array
	 * @access public
	 */
	public function get_vc_icons( ) {
		$icons = array( );
		$list  = $this->get_icons( );

		foreach ( $list as $icon ) {
			$code = $this->get_classes_before( ) . $icon;

			$icons[][$code] = ucwords( str_replace( array( '-', '_' ), ' ', $icon ) );
		}

		return $icons;
	}
}