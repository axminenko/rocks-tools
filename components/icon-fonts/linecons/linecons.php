<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Linecons
 *
 * @package    Create_Rocks_Tools
 * @subpackage Icon_Fonts_Linecons
 */
class Rocks_Tools_Icon_Fonts_Linecons extends Rocks_Tools_Icon_Fonts {
	/**
	 * Font version
	 *
	 * @var    string
	 * @access public
	 */
	public $version = '20130204';

	/**
	 * Stylesheet path
	 *
	 * @var    string
	 * @access public
	 */
	public $stylesheet = 'css/linecons.min.css';

	/**
	 * Allow deregister stylesheet
	 *
	 * @var    string
	 * @access public
	 */
	public $deregister = true;

	/**
	 * Font options
	 * 
	 * @var    string
	 * @access public
	 */
	public $default_icon = 'heart';

	/**
	 * Classes before icon name
	 * 
	 * @var    string
	 * @access public
	 */
	public $classes_before = 'li_';

	/**
	 * Icons list
	 *
	 * @var    string
	 * @access public
	 */
	public $icons_list = 'heart cloud star tv sound video trash user key search settings camera tag lock bulb pen diamond display location eye bubble stack cup phone news mail like photo note clock paperplane params banknote data music megaphone study lab food t-shirt fire clip shop calendar vallet vynil truck world';
}