<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Helpers functions for Visual Composer shortcodes
 *
 * @package    Create_Rocks_Tools
 * @subpackage Shortcodes_Visual_Composer_Helpers
 */
class Rocks_Tools_Shortcodes_Helpers {
	/**
	 * Get image destination by ID
	 *
	 * @return void|string
	 * @access public
	 * @static
	 */
	public static function get_image( ) {
		$image = intval( $_REQUEST['image'] );
		$src   = wp_get_attachment_thumb_url( $image );

		if ( $src === false ) {
			return;
		}

		return $src;
	}

	/**
	 * Get images destinations by IDs
	 *
	 * @return void|string
	 * @access public
	 * @static
	 */
	public static function get_images( ) {
		$data   = array( );
		$images = explode( ',', $_REQUEST['images'] );

		if ( ! is_array( $images ) or count( $images ) < 1 ) {
			return;
		}

		foreach ( $images as $image_id ) {
			$image = wp_get_attachment_thumb_url( $image_id );

			if ( $image !== false ) {
				$data[] = $image;
			}
		}

		return json_encode( $data );
	}
}
