<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * WordPress shortcodes library
 *
 * @package    Create_Rocks_Tools
 * @subpackage Shortcodes_Library
 */
class Rocks_Tools_Shortcodes_Library {
    /**
     * Parse and build HTML arributes for the element
     *
     * @param  array $attr
     * @return string
     * @access private
     * @static
     */
    private static function __attributes( $attr ) {
        if ( ! is_array( $attr ) or count( $attr ) < 1 ) {
            return '';
        }

        $output = array( );

        foreach ( $attr as $key => $value ) {
            if ( $value === false ) {
                continue;
            }

            if ( $key == 'class' ) {
                $classes = '';

                foreach ( $value as $x => $y ) {
                    if ( $y === false ) {
                        continue;
                    }

                    $classes .= $y . ' ';
                }

                if ( ! empty( $classes ) ) {
                    $output[] = $key . '="' . esc_attr( trim( $classes ) ) . '"';
                }
            } else if ( $key == 'src' or $key == 'href' ) {
                $output[] = $key . '="' . esc_url( $value ) . '"';
            } else if ( $key == 'style' and count( $value ) > 0 ) {
                $styles = '';

                foreach ( $value as $x => $y ) {
                    if ( $y === false ) {
                        continue;
                    }

                    $styles .= $x . ': ' . $y . '; ';
                }

                if ( ! empty( $styles ) ) {
                    $output[] = $key . '="' . esc_attr( trim( $styles ) ) . '"';
                }
            } else if ( $key == 'data' and count( $value ) > 0 ) {
                foreach ( $value as $x => $y ) {
                    if ( $y === true ) {
                        $y = 'true';
                    } else if ( $y === false ) {
                        continue;
                    }

                    $output[] = 'data-' . $x . '="' . esc_attr( $y ) . '"';
                }
            } else {
                if ( $value !== false ) {
                    $output[] = $key . '="' . esc_attr( $value ) . '"';
                }
            }
        }

        return ' ' . trim( implode( ' ', $output ) );
    }

    /**
     * Convert fractions to columns number
     *
     * @param  string $fraction
     * @return int
     * @access private
     * @static
     */
    private static function __convert_fractions( $fraction ) {
        $fraction = explode( '/', $fraction );

        $x = intval( $fraction[0] );
        $y = intval( $fraction[1] );

        if ( $x < 0 or $y < 1 ) {
            $x = 1;
            $y = 2;
        }

        return round( 12 * $x / $y );
    }

    /**
     * Enqueue scripts
     *
     * @param  array   $names
     * @param  boolean $primary
     * @access private
     * @static
     */
    private static function __enqueue_scripts( $names = array( ), $primary = true ) {
        if ( $primary ) {
            wp_enqueue_script( 'rocks-tools-shortcodes' );
        }

        if ( count( $names ) > 0 ) {
            foreach ( $names as $name ) {
                wp_enqueue_script( $name );
            }
        }
    }

    /**
     * Load shortcode template
     *
     * @param  string $name
     * @access private
     * @static
     */
    private static function __load_template( $name ) {
        $path = false;
        $name = sanitize_file_name( $name . '.php' );

        if ( file_exists( get_stylesheet_directory( ) . '/components/shortcodes/' . $name ) ) {
            $path = get_stylesheet_directory( ) . '/components/shortcodes/' . $name;
        } else if ( file_exists( get_template_directory( ) . '/components/shortcodes/' . $name ) ) {
            $path = get_template_directory( ) . '/components/shortcodes/' . $name;
        } else if ( file_exists( plugin_dir_path( __FILE__ ) . 'templates/' . $name ) ) {
            $path = plugin_dir_path( __FILE__ ) . 'templates/' . $name;
        }

        if ( $path ) {
            load_template( $path, false );
        }
    }

    /**
     * Get icon from attributes
     *
     * @param  array $atts
     * @param  mixed $default
     * @return boolean|mixed
     * @access private
     * @static
     */
    private static function __get_icon( $atts, $default = '' ) {
        if ( ! is_array( $atts ) or count( $atts ) < 1 ) {
            return $default;
        }

        if ( array_key_exists( 'icon', $atts ) ) {
            return $atts['icon'];
        }

        $matches = array( );

        foreach ( $atts as $name => $value ) {
            if ( preg_match( '/^icon_([A-Z0-9_]+)/is', $name, $matches ) ) {
                return $value;
            }
        }

        return $default;
    }

    private static function __get_animation( $animation ) {
        if ( ! empty( $animation ) ) {
            wp_enqueue_style( 'animate-css' );

            self::__enqueue_scripts( array(
                'waypoints'
            ) );

            return 'wpb_animate_when_almost_visible wpb_' . $animation . ' ' . $animation;
        }

        return false;
    }

    /*
     * Shortcodes
     */

    /**
     * Icon
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function icon( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'icon'    => '',
            'library' => '',
            'color'   => '',
            'size'    => '',
            'class'   => '',
        ), $atts ) );

        $icon = self::__get_icon( $atts );
        $attr = array( );

        if ( ! $icon_font = $rocks_tools->get_icon_font( $library ) ) {
            return;
        }

        $icon   = empty( $icon ) ? $icon_font->get_default_icon( ) : $icon;
        $before = $icon_font->get_classes_before( );

        if ( ! empty( $before ) ) {
            if ( substr_count( $icon, $before ) > 0 ) {
                $icon = str_replace( $before, '', $icon );
            }

            $icon = $before . $icon;
        }

        $icon_font->direct_enqueue( );

        $attr['class'][] = $icon;

        if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
            $attr['style']['color'] = $color;
        }

        if ( ! empty( $size ) ) {
            $attr['style']['font-size'] = intval( $size ) . 'px';
        }

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        return '<i' . self::__attributes( $attr ) . '></i>';
    }

    /**
     * Button
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function button( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'url'           => site_url( ),
            'target'        => '_self',
            'background'    => 'solid',
            'color'         => '',
            'size'          => 'default',
            'icon'          => '',
            'library'       => '',
            'align'         => 'default',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $icon = self::__get_icon( $atts );
        $attr = array( );

        $attr['href']    = $url;
        $attr['class'][] = 'button';

        if ( ! empty( $target ) and $target != '_self' ) {
            $attr['target'] = $target;
        }

        if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
            $attr['style']['background']   = $color;
            $attr['style']['border-color'] = $color;

            if ( $background != 'solid' ) {
                $attr['style']['color'] = $color . ' !important';
            }

            $attr['class'][] = 'button-with-color';
        }

        if ( ! empty( $size ) and $size != 'default' ) {
            $attr['class'][] = 'button-' . $size;
        }

        if ( $background != 'solid' ) {
            $attr['class'][] = 'button-transparent';
        }

        if ( ! empty( $icon ) ) {
            $icon            = self::icon( array( 'icon' => $icon, 'library' => $library ) );
            $attr['class'][] = 'button-with-icon';
        }

        $animation = trim( self::__get_animation( $css_animation ) );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $output = '<a' . self::__attributes( $attr ) . '>' . $icon . trim( strip_tags( $content ) ) . '</a>';

        if ( ! empty( $align ) and $align != 'default' ) {
            $output = '<div class="align-' . esc_attr( $align ) . '">' . $output . '</div>';
        }

        if ( $animation ) {
            $output = '<div class="animation-holder' . ( ! empty( $animation ) ? ' ' . $animation : '' ) . '">' . $output . '</div>';
        }

        return $output;
    }

    /**
     * Alert
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function alert( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'type'          => 'notice',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr = array( );

        $attr['class'][] = 'alert';
        $attr['class'][] = 'alert-' . $type;

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        return '<div' . self::__attributes( $attr ) . '>' . do_shortcode( wpautop( $content ) ) . '</div>';
    }

    /**
     * Google Map
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function google_map( $atts, $content = null ) {
        global $rocks_tools;

        self::__enqueue_scripts( array(
            'google-maps',
            'jquery-gmap',
        ) );

        extract( shortcode_atts( array(
            'maptype' => 'roadmap',
            'marker'  => '',
            'scheme'  => $rocks_tools->get_option( 'map-scheme', 'default' ),
            'width'   => '100%',
            'height'  => '300px',
            'zoom'    => '11',
            'class'   => '',
        ), $atts ) );

        $default = '40.6700, -73.9400';

        $internal  = 0;
        $matches   = array( );

        $attr = array( );
        $attr['data']['points'] = $internal;

        if ( count( $atts ) < 1 or empty( $atts['point-1'] ) ) {
            $atts['point-1'] = $default;
        }

        foreach ( $atts as $name => $value ) {
            if ( preg_match( '/point-([\\d]+)/i', $name ) > 0 ) {
                $internal ++;

                $attr['data']['point-' . $internal] = trim( $value );

                if ( isset( $atts['marker-' . $internal] ) ) {
                    if ( $image = wp_get_attachment_image_src( intval( $atts['marker-' . $internal] ), 'full' ) ) {
                        $attr['data']['marker-' . $internal] = $image[0];
                    }
                }
            }
        }

        if ( ! empty( $marker ) ) {
            if ( $image = wp_get_attachment_image_src( intval( $marker ), 'full' ) ) {
                $attr['data']['marker'] = $image[0];
            }
        }

        $attr['data']['points']  = $internal;
        $attr['data']['maptype'] = strtoupper( $maptype );
        $attr['data']['zoom']    = intval( $zoom );

        if ( ! empty( $scheme ) and $scheme != 'default' ) {
            $attr['data']['scheme'] = $scheme;
        }

        $attr['class'][] = 'google-map';

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        if ( ! empty( $width ) ) {
            $attr['style']['width'] = $width;
        }

        if ( ! empty( $height ) ) {
            $attr['style']['height'] = $height;
        }

        return '<div' . self::__attributes( $attr ) . '></div>';
    }

    /**
     * Dropcap
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function dropcap( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'style' => '',
            'color' => '',
        ), $atts ) );

        $attr = array( );
        $attr['class'][] = 'dropcap';

        if ( ! empty( $style ) ) {
            $attr['class'][] = 'dropcap-' . $style;
        }

        if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
            if ( $style == 'circle' ) {
                $attr['style']['background'] = $color;
            } else {
                $attr['style']['color'] = $color;
            }
        }

        $letter  = '<span' . self::__attributes( $attr ) . '>' . substr( trim( $content ), 0, 1 ) . '</span>';
        $content = $letter . substr( $content, 1 );

        return wpautop( do_shortcode( $content ) );
    }

    /**
     * Pullquote
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function pullquote( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'align'         => 'none',
            'author'        => '',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr = array( );

        if ( ! empty( $align ) and $align != 'none' ) {
            $attr['class'][] = 'pull-' . $align;
        }

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        if ( ! empty( $author ) ) {
            $author = '<footer><span>' . esc_html( $author ) . '</span></footer>';
        }

        return '<blockquote' . self::__attributes( $attr ) . '>' . wpautop( do_shortcode( $content ) ) . $author . '</blockquote>';
    }

    /**
     * Highlight
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function highlight( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'color' => '',
        ), $atts ) );

        $attr            = array( );
        $attr['class'][] = 'highlight';

        if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
            $attr['style']['background-color'] = $color;
        }

        return '<span' . self::__attributes( $attr ) . '>' . trim( strip_tags( $content ) ) . '</span>';
    }

    /**
     * Image
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function image( $atts, $content = null ) {
        self::__enqueue_scripts( array(
            'lity',
        ) );

        extract( shortcode_atts( array(
            'id'            => '',
            'size'          => 'full',
            'align'         => 'none',
            'onclick'       => 'none',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $id    = intval( $id );
        $attr  = array( );
        $image = wp_get_attachment_image_src( $id, $size );

        if ( ! $image ) {
            return;
        }

        $attr['src']    = $image[0];
        $attr['width']  = $image[1];
        $attr['height'] = $image[2];
        $attr['alt']    = __( 'Image', 'rocks' );

        if ( ! empty( $align ) and $align != 'none' ) {
            $attr['class'][] = 'align-' . $align;
        }

        $attr['class'][] = 'image-round';
        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $image = '<img' . self::__attributes( $attr ) . '>';

        if ( $onclick != 'none' ) {
            if ( $size == 'full' ) {
                $fullwidth = $attr['src'];
            } else {
                $fullwidth = wp_get_attachment_image_src( $id, 'full' );
                $fullwidth = $fullwidth[0];
            }
        }

        if ( $onclick == '_self' or $onclick == '_blank' ) {
            $image = '<a href="' . $fullwidth . '" target="' . esc_attr( $onclick ) . '">' . $image . '</a>';
        } else if ( $onclick == 'lightbox' ) {
            $image = '<a href="' . $fullwidth . '" data-lity>' . $image . '</a>';
        }

        return $image;
    }

    /**
     * Pricing table
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function pricing_table( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'table'         => 'default',
            'plan'          => 'default',
            'heading'       => __( 'Pricing Table', 'rocks' ),
            'price'         => __( '$0', 'rocks' ),
            'period'        => __( 'Month', 'rocks' ),
            'button'        => __( 'Sign up now', 'rocks' ),
            'url'           => site_url( ),
            'target'        => '_self',
            'color'         => '',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr = array( );

        if ( ! empty( $color ) and ! $rocks_tools->check_color( $color ) ) {
            $color = '';
        }

        $attr['class'][] = 'pricing-table';
        $attr['class'][] = 'pricing-table-style-' . $table;
        $attr['class'][] = 'pricing-table-plan-' . $plan;

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $button = self::button( array(
            'url'    => $url,
            'target' => $target,
            'color'  => $color,
        ), $button );

        if ( ! empty( $period ) ) {
            $period = '<div class="pricing-table-period">' . esc_html( $period ) . '</div>';
        }

        if ( substr_count( $content, '<li>' ) < 1 and ! empty( $content ) ) {
            $lines   = explode( "\n", $content );
            $content = '<ul>';

            foreach ( $lines as $line ) {
                $content .= '<li>' . $line . '</li>';
            }

            $content .= '</ul>';
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            <div class="pricing-table-heading"' . ( empty( $color ) ? '' : ' style="color: ' . esc_attr( $color ) . '"' ) . '>
                <h5 class="pricing-table-title">' . esc_html( $heading ) . '</h5>
                <div class="pricing-table-price">' . esc_html( $price ) . $period . '</div>
            </div>
            ' . ( empty( $content ) ? '' : '<div class="pricing-table-content">' . do_shortcode( $content ) . '</div>' ) . '
            <div class="pricing-table-button">' . $button . '</div>
        </div>';
    }

    /**
     * Testimonial
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function testimonial( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'author'        => __( 'John Doe', 'rocks' ),
            'company'       => '',
            'image'         => '',
            'rating'        => 'none',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr            = array( );
        $attr['class'][] = 'testimonial';

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $image = wp_get_attachment_image_src( intval( $image ), 'large' );

        if ( ! $image ) {
            return;
        }

        if ( $rating != 'none' ) {
            $stars  = absint( $rating );
            $rating = '';

            if ( $stars > 0 and $stars < 6 ) {
                $rating = '<div class="testimonial-rating">';

                for ( $i = 1; $i < 6; $i ++ ) {
                    $rating .= '<i class="fa fa-star' . ( $stars >= $i ? '' : '-o' ) . '"></i>';
                }

                $rating .= '</div>';
            }
        } else {
            $rating = '';
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            <div class="testimonial-image" style="background-image: url(\'' . esc_attr( $image[0] ) . '\')"></div>
            <h4 class="testimonial-author">' . esc_html( $author ) . '</h4>
            ' . ( empty( $company ) ? '' : '<div class="testimonial-company">' . esc_html( $company ) . '</div>' ) . '
            <div class="testimonial-text">' . do_shortcode( wpautop( $content ) ) . '</div>
            ' . $rating . '
        </div>';
    }

    /**
     * Team member
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global $rocks_tools Rocks_Tools
     */
    public static function team_member( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'style'         => 'one',
            'name'          => __( 'John Doe', 'rocks' ),
            'skill'         => '',
            'image'         => '',
            'url'           => '',
            'links'         => '',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $image = wp_get_attachment_image_src( intval( $image ), 'large' );

        if ( ! $image ) {
            return;
        }

        $attr   = array( );
        $social = '';

        $attr['class'][] = 'team-member';

        if ( ! empty( $style ) ) {
            $attr['class'][] = 'team-member-style-' . $style;
        }

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $social_color = 'brands';

        if ( $style == 'two' ) {
            $social_color = 'grayscale';
        } else if ( $style == 'three' ) {
            $social_color = 'primary';
        }

        if ( $social = self::social_icons( array( 'links' => $links, 'color' => $social_color ) ) ) {
            $social = '<div class="team-member-social">' . $social . '</div>';
        }

        $return = '
        <div' . self::__attributes( $attr ) . '>
            <div class="team-member-image-holder">
                <div class="team-member-image" style="background-image: url(\'' . esc_attr( $image[0] ) . '\')"></div>';

        if ( $style == 'three' ) {
            $return .= $social;
        }

        $return .= '
            </div>
            <div class="team-member-details">
                <h4 class="team-member-name">' . esc_html( $name ) . '</h4>
                ' . ( empty( $skill ) ? '' : '<div class="team-member-skill">' . esc_html( $skill ) . '</div>' );

        if ( $style == 'two' ) {
            $return .= $social;
        }

        $return .= '
            <div class="team-member-text">' . do_shortcode( wpautop( $content ) ) . '</div>';

        if ( $style == 'one' ) {
            $return .= $social;
        }

        if ( ! empty( $url ) ) {
            $return .= '
            <div class="team-member-page">' . self::button( array( 'url' => $url ), __( 'Read More', 'rocks' ) ) . '</div>';
        }

        $return .= '
            </div>
        </div>';

        return $return;
    }

    /**
     * Clear
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function clear( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'gap'       => '0',
            'hidden_lg' => '',
            'hidden_md' => '',
            'hidden_sm' => '',
            'hidden_xs' => '',
        ), $atts ) );

        $attr   = array( );
        $gap    = intval( $gap );

        $attr['class'][] = 'clear';

        if ( ! empty( $hidden_md ) ) {
            $attr['class'][] = 'hidden-md';
        }

        if ( ! empty( $hidden_sm ) ) {
            $attr['class'][] = 'hidden-sm';
        }

        if ( ! empty( $hidden_xs ) ) {
            $attr['class'][] = 'hidden-xs';
        }

        if ( ! empty( $hidden_lg ) ) {
            $attr['class'][] = 'hidden-lg';
        }

        if ( $gap > 0 ) {
            $attr['style']['height'] = $gap . 'px';
        }

        return '<div' . self::__attributes( $attr ) . '></div>';
    }

    /**
     * Progress bar
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function progress( $atts, $content = null ) {
        global $rocks_tools;

        self::__enqueue_scripts( );

        extract( shortcode_atts( array(
            'type'      => 'horizontal',
            'skill'     => '60',
            'unit'      => __( '%', 'rocks' ),
            'color'     => '',
            'class'     => '',
        ), $atts ) );

        if ( $type != 'vertical' ) {
            $type = 'horizontal';
        }

        $attr  = array( );
        $skill = absint( $skill );

        $attr['class'][] = 'progress-fill';

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        if ( $type == 'horizontal' ) {
            $attr['style']['width'] = $skill . '%';
        } else {
            $attr['style']['height'] = $skill . '%';
        }

        $attr['data']['current']  = 0;
        $attr['data']['progress'] = $skill;
        $attr['data']['unit']     = esc_html( $unit );

        if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
            $attr['style']['background-color'] = $color;
        }

        $label = '
        <div class="progress-label">
            <div class="progress-skill">' . trim( strip_tags( $content ) ) . '</div>
            <div class="progress-value">' . $skill . esc_html( $unit ) . '</div>
        </div>';

        return '
        <div class="progress progress-' . esc_attr( $type ) . '">
            ' . ( $type != 'vertical' ? $label : '' ) . '
            <div class="progress-bar">
                <div' . self::__attributes( $attr ) . '></div>
            </div>
            ' . ( $type == 'vertical' ? $label : '' ) . '
        </div>';
    }

    /**
     * Call to action
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function action( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'heading'       => __( 'Hello, World!', 'rocks' ),
            'align'         => 'left',
            'button'        => __( 'Sign up now', 'rocks' ),
            'color'         => '',
            'url'           => site_url( ),
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr = array( );

        $attr['class'][] = 'call-to-action';
        $attr['class'][] = 'call-to-action-' . $align;

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            <h2 class="call-to-action-heading">' . esc_html( $heading ) . '</h2>
            <div class="call-to-action-text">
                ' . do_shortcode( wpautop( $content ) ) . '
            </div>
            <div class="call-to-action-button">
                ' . self::button( array( 'url' => $url, 'color' => $color ), $button ) . '
            </div>
        </div>';
    }

    /**
     * Counter
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function counter( $atts, $content = null ) {
        global $rocks_tools;

        self::__enqueue_scripts( );

        extract( shortcode_atts( array(
            'skill'         => '60',
            'unit'          => '',
            'icon'          => '',
            'library'       => '',
            'before'        => '',
            'after'         => '',
            'color'         => '',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $icon  = self::__get_icon( $atts );
        $attr  = array( );
        $skill = intval( $skill );

        $attr['class'][] = 'counter';
        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $attr['data']['current']  = 0;
        $attr['data']['progress'] = $skill;
        $attr['data']['unit']     = esc_html( $unit );
        $attr['data']['before']   = esc_html( $before );
        $attr['data']['after']    = esc_html( $after );

        if ( ! empty( $icon ) ) {
            if ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) {
                $icon = self::icon( array( 'icon' => $icon, 'library' => $library, 'color' => $color ) );
            } else {
                $icon = self::icon( array( 'icon' => $icon, 'library' => $library ) );
            }

            $icon = '<div class="counter-icon">' . $icon . '</div>';
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            ' . $icon . '
            <div class="counter-skill">' . esc_html( $before ) . $skill . esc_html( $unit ) . esc_html( $after ) . '</div>
            <div class="counter-label">' . trim( strip_tags( $content ) ) . '</div>
            <div class="counter-line"' . ( ( ! empty( $color ) and $rocks_tools->check_color( $color ) ) ? ' style="background: ' . esc_attr( $color ) . ';"' : '' ) . '></div>
        </div>';
    }

    /**
     * Icon box
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function icon_box( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'heading'       => __( 'Hello, World!', 'rocks' ),
            'align'         => 'vertical',
            'style'         => 'one',
            'icon'          => '',
            'library'       => '',
            'color'         => '',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $icon = self::__get_icon( $atts );
        $attr = array( );

        $attr['class'][] = 'icon-box';
        $attr['class'][] = 'icon-box-' . $align;
        $attr['class'][] = 'icon-box-style-' . $style;
        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        if ( ! empty( $heading ) ) {
            $heading = '
            <div class="icon-box-heading">
                ' . esc_html( $heading ) . '
            </div>';
        }

        if ( ! empty( $icon ) ) {
            $icon = '
            <div class="icon-box-icon">
                ' . self::icon( array( 'icon' => $icon, 'library' => $library, 'color' => $color ) ) . '
            </div>';
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            ' . $icon . '
            ' . $heading . '
            <div class="icon-box-content">
                ' . do_shortcode( wpautop( $content ) ) . '
            </div>
        </div>';
    }

    /**
     * Separator
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function separator( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'style' => 'solid',
        ), $atts ) );

        $attr = array( );

        $attr['class'][] = 'separator';
        $attr['class'][] = 'separator-' . $style;

        if ( ! empty( $content ) ) {
            $attr['class'][] = 'separator-with-text';
        }

        return '
        <div' . self::__attributes( $attr ) . '>' . ( ! empty( $content ) ? '<span>' . trim( strip_tags( $content ) ) . '</span>' : '' ) . '</div>';
    }

    /**
     * Image slider
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function slider( $atts, $content = null ) {
        self::__enqueue_scripts( array(
            'owl-carousel',
        ) );

        extract( shortcode_atts( array(
            'images'        => '',
            'autoplay'      => '',
            'navigation'    => 'arrows',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $slides    = '';
        $images    = explode( ',', $images );
        $count     = count( $images );
        $animation = self::__get_animation( $css_animation );

        if ( ! is_array( $images ) or $count < 1 ) {
            return;
        }

        foreach ( $images as $image_id ) {
            $image_id = intval( $image_id );

            if ( $image = wp_get_attachment_image_src( $image_id, 'full' ) ) {
                $slides .= '[vc_tta_section]' . self::image( array( 'id' => $image_id ) ) . '[/vc_tta_section]';
            } else {
                $count --;
            }
        }

        if ( $count < 1 ) {
            return;
        }

        return do_shortcode( '[vc_tta_pageable navigation="' . esc_attr( $navigation ) . '" ' . ( $autoplay > 1 ? ' autoplay="' . $autoplay . '"' : '' ) . ' class="image-slider' . ( $animation !== false ? ' ' . $animation : '' ) . '"]' . $slides . '[/vc_tta_pageable]' );
    }

    /**
     * Heading
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function heading( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'tag'           => 'h2',
            'align'         => 'none',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr = array( );
        $tags = array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' );
        $tag  = strtolower( $tag );

        if ( ! in_array( $tag, $tags ) ) {
            $tag = 'h2';
        }

        if ( ! empty( $align ) and $align != 'none' ) {
            $attr['class'][] = 'align-' . $align;
        }

        $attr['class'][] = self::__get_animation( $css_animation );

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        return '<' . $tag . self::__attributes( $attr ) . '>' . do_shortcode( $content ) . '</' . $tag . '>';
    }

    /**
     * Social icons
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global $rocks_tools Rocks_Tools
     */
    public static function social_icons( $atts, $content = null ) {
        global $rocks_tools;

        extract( shortcode_atts( array(
            'style' => 'one',
            'color' => 'brands',
            'links' => '',
        ), $atts ) );

        $links = strip_tags( $links );

        if ( empty( $links ) ) {
            return '';
        }

        $list = '';
        $links = explode( "\n", $links );

        if ( count( $links ) < 1 ) {
            return '';
        }

        $attr = array( );

        $attr['class'][] = 'social-icons';
        $attr['class'][] = 'social-icons-' . $style;
        $attr['class'][] = 'social-icons-' . $color;

        foreach ( $links as $url ) {
            $url = trim( $url );

            if ( $network = $rocks_tools->get_social_by_url( $url ) ) {
                $icon = self::icon( array( 'icon' => $network['id'], 'library' => 'font-awesome' ) );
                $list .= '<li class="' . esc_attr( $network['id'] ) . '"><a href="' . esc_url( $url ) . '" title="' . esc_attr( $network['label'] ) . '" target="_blank">' . $icon . ( ( $style == 'five' or $style == 'six' ) ? esc_html( $network['label'] ) : '' ) . '</a></li>';
            }
        }

        if ( empty( $list ) ) {
            return '';
        }

        return '
        <ul' . self::__attributes( $attr ) . '>
            ' . $list . '
        </ul>';
    }

    /**
     * Posts grid
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     * @global WP_Post     $post
     */
    public static function posts_grid( $atts, $content = null ) {
        global $rocks_tools, $post;

        extract( shortcode_atts( array(
            'width'      => '1/4',
            'post_type'  => 'post',
            'per_page'   => '12',
            'category'   => '',
            'orderby'    => 'date',
            'order'      => 'desc',
            'class'      => '',
        ), $atts ) );

        $attr = array( );
        $item = array( );

        $types = get_post_types( array( 'public' => true ) );
        $width = self::__convert_fractions( $width );

        if ( ! in_array( $post_type, $types ) ) {
            $post_type = 'post';
        }

        $posts = array(
            'post_type'      => $post_type,
            'posts_per_page' => $per_page,
            'category_name'  => $category,
            'orderby'        => $orderby,
            'order'          => strtoupper( $order ),
        );

        if ( $orderby == 'view_count' ) {
            if ( $rocks_tools->theme_support( 'post-views', 'components' ) ) {
                $posts['orderby']  = 'meta_value_num';
                $posts['meta_key'] = 'post_views_count';
            } else {
                $posts['orderby'] = 'date';
            }
        }

        $posts = get_posts( $posts );

        if ( count( $posts ) < 1 ) {
            return;
        }

        $attr['class'][] = 'posts-grid';

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        $item['class'][] = 'posts-grid-item';

        $item = self::__attributes( $item );

        ob_start( );

        foreach ( $posts as $post ) {
            setup_postdata( $post );

            echo '
            <div class="col-md-' . $width . ' col-sm-' . $width . '">
                <div' . $item . '>';

            self::__load_template( __FUNCTION__ );

            echo '
                </div>
            </div>';
        }

        $output = ob_get_clean( );

        wp_reset_postdata( );

        return '
        <div' . self::__attributes( $attr ) . '>
            <div class="row">
                ' . $output . '
            </div>
        </div>';
    }

    /**
     * Masonry grid
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     * @global WP_Post     $post
     */
    public static function masonry_grid( $atts, $content = null ) {
        global $rocks_tools, $post;

        self::__enqueue_scripts( array(
            'jquery-isotope',
        ) );

        extract( shortcode_atts( array(
            'width'      => '1/4',
            'post_type'  => 'post',
            'pagination' => 'disabled',
            'per_page'   => '12',
            'offset'     => '',
            'category'   => '',
            'orderby'    => 'date',
            'order'      => 'desc',
            'class'      => '',
        ), $atts ) );

        $attr = array( );
        $item = array( );

        $types      = get_post_types( array( 'public' => true ) );
        $pagination = ( $pagination == 'enabled' );

        if ( ! in_array( $post_type, $types ) ) {
            $post_type = 'post';
        }

        $offset = absint( $offset );
        $posts  = array(
            'post_type'      => $post_type,
            'posts_per_page' => $per_page,
            'offset'         => $offset,
            'category_name'  => $category,
            'orderby'        => $orderby,
            'order'          => strtoupper( $order ),
        );

        if ( $orderby == 'view_count' ) {
            if ( $rocks_tools->theme_support( 'post-views', 'components' ) ) {
                $posts['orderby']  = 'meta_value_num';
                $posts['meta_key'] = 'post_views_count';
            } else {
                $posts['orderby'] = 'date';
            }
        }

        $posts = get_posts( $posts );

        if ( count( $posts ) < 1 ) {
            return;
        }

        $attr['class'][] = 'masonry';
        $attr['class'][] = 'masonry-posts';
        $attr['class'][] = 'masonry-columns-' . self::__convert_fractions( $width );

        $attr['data']['width']     = $width;
        $attr['data']['post_type'] = $post_type;

        if ( $pagination ) {
            $attr['data']['per_page']  = $per_page;
            $attr['data']['offset']    = $offset;
            $attr['data']['category']  = $category;
            $attr['data']['orderby']   = $orderby;
            $attr['data']['order']     = $order;
        }

        $item['class'][] = 'masonry-item';

        $item = self::__attributes( $item );

        ob_start( );

        foreach ( $posts as $post ) {
            setup_postdata( $post );

            if ( ! $rocks_tools->get_post_image_src( $post ) ) {
                continue;
            }

            echo '
            <div' . $item . '>';

            self::__load_template( __FUNCTION__ );

            echo '
            </div>';
        }

        $output = ob_get_clean( );

        wp_reset_postdata( );

        if ( $offset > 0 ) {
            return $output;
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            <div class="masonry-items">
                ' . $output . '
            </div>
            ' . ( $pagination ? '<div class="masonry-more"><a href="#">' . __( 'Load more', 'rocks' ) . '</a></div>' : '' ) . '
        </div>';
    }

    /**
     * Masonry media grid
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function masonry_media( $atts, $content = null ) {
        self::__enqueue_scripts( array(
            'jquery-isotope',
            'lity',
        ) );

        extract( shortcode_atts( array(
            'width'         => '1/4',
            'images'        => '',
            'onclick'       => 'none',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        $attr   = array( );
        $images = explode( ',', $images );
        $output = '';

        if ( ! is_array( $images ) or count( $images ) < 1 ) {
            return;
        }

        $attr['class'][] = 'masonry';
        $attr['class'][] = 'masonry-media';
        $attr['class'][] = 'masonry-columns-' . self::__convert_fractions( $width );

        $target   = '';
        $lightbox = false;

        ob_start( );

        foreach ( $images as $id ) {
            global $image;

            $image = wp_get_attachment_image_src( intval( $id ), 'full' );

            if ( ! $image ) {
                continue;
            }

            $item = array( );
            $item['class'][] = 'masonry-item';

            $permalink = $image[0];

            if ( $onclick == '_self' or $onclick == '_blank' ) {
                $target = $onclick;
            } else if ( $onclick == 'lightbox' ) {
                $lightbox = true;
            } else {
                $permalink = '#';
            }

            $item['class'][] = self::__get_animation( $css_animation );

            echo '
            <div' . self::__attributes( $item ) . '>
                <a href="' . esc_url( $permalink ) . '"' . ( ! empty( $target ) ? ' target="' . esc_attr( $target ) . '"' : '' ) . ( $lightbox ? ' data-lity' : '' ) . '>';

            self::__load_template( __FUNCTION__ );

            echo '
                </a>
            </div>';
        }

        $output = ob_get_clean( );

        return '
        <div' . self::__attributes( $attr ) . '>
            <div class="masonry-items">
                ' . $output . '
            </div>
        </div>';
    }

    /**
     * Clients
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function clients( $atts, $content = null ) {
        self::__enqueue_scripts( array(
            'owl-carousel',
        ) );

        extract( shortcode_atts( array(
            'images'        => '',
            'carousel'      => 'disabled',
            'autoplay'      => '',
            'items'         => '6',
            'align'         => 'left',
            'css_animation' => '',
            'class'         => '',
        ), $atts ) );

        if ( empty( $images ) ) {
            return;
        }

        $attr = array( );
        $list = '';

        $attr['class'][] = 'clients';

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        if ( $carousel != 'disabled' ) {
            $attr['class'][] = 'carousel';

            if ( ! empty( $autoplay ) ) {
                $attr['data']['autoplay'] = absint( $autoplay );
            }

            $attr['data']['items'] = $items;
        }

        if ( $align != 'left' ) {
            $attr['class'][] = 'align-' . $align;
        }

        $images = explode( ',', $images );
        $css_animation = empty( $css_animation ) ? '' : ' ' . self::__get_animation( $css_animation );

        foreach ( $images as $image_id ) {
            $image = wp_get_attachment_image_src( intval( $image_id ), 'full' );

            if ( $image ) {
                $list .= '<div class="clients-item' . $css_animation . '"><img src="' . esc_url( $image[0] ) . '" alt="' . __( 'Image', 'rocks' ) . '" width="' . esc_attr( $image[1] ) . '" height="' . esc_attr( $image[2] ) . '"></div>';
            }
        }

        return '
        <div' . self::__attributes( $attr ) . '>
            ' . $list . '
        </div>';
    }

    /**
     * List with icons
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function custom_list( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'icon'      => '',
            'library'   => '',
            'color'     => '',
            'align'     => 'left',
            'class'     => '',
        ), $atts ) );

        $icon = self::__get_icon( $atts );
        $attr = array( );

        $list = '';

        $content = str_replace( array( '<br/>', '<br />' ), '<br>',  strip_tags( $content, '<br>' ) );
        $strings = explode( '<br>', $content );
        $icon = self::icon( array( 'icon' => $icon, 'library' => $library, 'color' => $color, 'class' => 'icon-list-item' ) );

        foreach ( $strings as $string ) {
            $string = trim( $string );

            if ( ! empty( $string ) ) {
                $list .= '<li>' . ( $align == 'left' ? $icon : '' ) . $string . ( $align == 'right' ? $icon : '' ) . '</li>';
            }
        }

        $attr['class'][] = 'icon-list';

        if ( ! empty( $align ) and $align != 'left' ) {
            $attr['class'][] = 'align-' . $align;
        }

        if ( ! empty( $class ) ) {
            $attr['class'][] = $class;
        }

        return '
        <ul' . self::__attributes( $attr ) . '>
            ' . $list . '
        </ul>';
    }

    /**
     * Twitter feed
     *
     * @param  array  $atts
     * @param  string $content
     * @return string
     * @access public
     * @static
     */
    public static function twitter_feed( $atts, $content = null ) {
        self::__enqueue_scripts( array(
            'owl-carousel',
        ) );

        // Include Twitter library
        require_once plugin_dir_path( __FILE__ ) . 'includes/twitter.php';

        extract( shortcode_atts( array(
            'id'         => '@envato',
            'autoplay'   => '',
            'limit'      => '',
            'class'      => '',
        ), $atts ) );

        $id    = str_replace( '@', '', $id );
        $limit = absint( $limit );
        $limit = $limit == 0 ? 10 : $limit;
        $alert = self::alert( array( 'type' => 'error' ), __( 'Twitter feed currently unavailable.', 'rocks' ) );

        $twitter = new Rocks_Tools_Shortcodes_Twitter( $id, $limit );
        $twitter->set_details( 'crt_twitter_' );

        if ( ! $tweets = $twitter->get_feed( ) ) {
            return $alert;
        }

        $slides  = '';
        $limit   = count( $tweets );

        foreach ( $tweets as $tweet ) {
            if ( array_key_exists( 'text', $tweet ) ) {
                $slides .= '[vc_tta_section]<div class="row"><div class="col-md-10 col-md-offset-1"><div class="twitter-feed-text">' . $tweet['text'] . '</div></div></div>[/vc_tta_section]';
            } else {
                $limit --;
            }
        }

        if ( $limit < 1 ) {
            return $alert;
        }

        $before = '
        <div class="twitter-feed-brand">
            <a href="' . esc_url( 'https://twitter.com/' . $id ) . '" target="_blank">
                <i class="fa fa-twitter"></i>
            </a>
        </div>';

        $after = '
        <div class="twitter-feed-id">
            <a href="#" class="button">Follow <span class="at">@</span>' . $id . '</a>
        </div>';

        return $before . do_shortcode( '[vc_tta_pageable' . ( $autoplay > 1 ? ' autoplay="' . $autoplay . '"' : '' ) . ' class="twitter-feed"]' . $slides . '[/vc_tta_pageable]' ) . $after;
    }
}
