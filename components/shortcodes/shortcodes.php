<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

// Require shortcodes
require plugin_dir_path( __FILE__ ) . 'library.php';
require plugin_dir_path( __FILE__ ) . 'tweaks.php';

/**
 * Visual Composer shortcodes
 *
 * @package    Create_Rocks_Tools
 * @subpackage Shortcodes
 */
class Rocks_Tools_Shortcodes {
    /**
     * Plugin instance
     *
     * @var    Rocks_Tools
     * @access private
     */
    private $__plugin_instance;

    /**
     * Shortcodes names
     *
     * @var    array
     * @access private
     */
    private $__names;

    /**
     * Constructor
     *
     * @param  Rocks_Tools $plugin_instance
     * @access public
     */
    function __construct( $plugin_instance = null ) {
        $this->__plugin_instance = $plugin_instance;

        if ( ! $this->__plugin_instance->vc_support( ) ) {
            return;
        }

        add_action( 'vc_before_init',                         array( &$this, 'before_init' ) );
        add_action( 'vc_after_init',                          array( &$this, 'after_init' ) );
        add_action( 'wp_enqueue_scripts',                     array( &$this, 'enqueue_scripts' ) );
        add_action( 'admin_enqueue_scripts',                  array( &$this, 'admin_enqueue_scripts' ) );
        add_action( 'wp_ajax_rocks_tools_shortcodes_helpers', array( &$this, 'helpers' ) );
    }

    /**
     * Helpers functions for shortcodes
     *
     * @access public
     * @static
     */
    public static function helpers( ) {
        $name = $_REQUEST['name'];

        if ( empty( $name ) ) {
            return;
        }

        require_once plugin_dir_path( __FILE__ ) . 'helpers.php';

        if ( method_exists( 'Rocks_Tools_Shortcodes_Helpers', $name ) ) {
            echo Rocks_Tools_Shortcodes_Helpers::$name( );
        }

        die( );
    }

    /**
     * Create class instance
     *
     * @param  Rocks_Tools $plugin_instance
     * @return Rocks_Tools_Shortcodes
     * @access public
     * @static
     */
    public static function instance( $plugin_instance = null ) {
        return new self( $plugin_instance );
    }

    public function get_plugin_instance( ) {
        return $this->__plugin_instance;
    }

    /**
     * Before Visual Composer init
     *
     * @access public
     */
    public function before_init( ) {
        vc_set_as_theme( );
        vc_set_shortcodes_templates_dir( plugin_dir_path( __FILE__ ) . 'templates' );

        $this->__load_shortcodes( );
    }

    /**
     * After Visual Composer init
     *
     * @access public
     */
    public function after_init( ) {
        vc_disable_frontend( );
    }

    /**
     * Load predefined layouts
     *
     * @return array
     * @access public
     * @static
     */
    public static function load_layouts( ) {
        require_once plugin_dir_path( __FILE__ ) . 'layouts.php';

        if ( isset( $rocks_tools_layouts_vc ) and is_array( $rocks_tools_layouts_vc ) ) {
            return $rocks_tools_layouts_vc;
        }

        return array( );
    }

    /**
     * Enqueue admin scripts
     *
     * @param  string $hook
     * @access public
     *
     * @global Rocks_Tools $rocks_tools
     */
    public function admin_enqueue_scripts( $hook ) {
        global $rocks_tools;

        if ( $hook == 'post.php' or $hook == 'post-new.php' ) {
            $post = get_post( );

            if ( $post !== null ) {
                if ( in_array( $post->post_type, vc_editor_post_types( ) ) ) {
                    $this->__plugin_instance->icon_fonts_enqueue( );

                    wp_enqueue_style( 'rocks-tools-shortcodes-vc', plugins_url( 'css/visual-composer.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );
                    wp_enqueue_script( 'rocks-tools-shortcodes-vc', plugins_url( 'js/custom-views.min.js', __FILE__ ), array( 'jquery', 'vc_accordion_script', 'vc-backend-actions-js', 'vc-backend-min-js' ), $rocks_tools->get_version( ), true );
                }
            }
        }
    }

    /**
     * Enqueue scripts
     *
     * @access public
     *
     * @global Rocks_Tools $rocks_tools
     */
    public function enqueue_scripts( ) {
        global $rocks_tools;

        // Google Maps API Key
        $maps_api = get_option( 'crt_google_maps_key', false ) ? get_option( 'crt_google_maps_key' ) : 'AIzaSyAvHX9ex-AjC83bBfMpia7Ksaoic_FQdiY';

        // Styles
        wp_enqueue_style( 'rocks-tools-shortcodes-vendor', plugins_url( 'css/vendor.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );

        // Scripts
        wp_register_script( 'rocks-tools-shortcodes', plugins_url( 'js/shortcodes.min.js', __FILE__ ), array( 'jquery' ), $rocks_tools->get_version( ), true );
        wp_register_script( 'google-maps', 'https://maps.google.com/maps/api/js?key=' . $maps_api, array( 'jquery' ), '3', true );
        wp_register_script( 'jquery-gmap', plugins_url( 'js/jquery.gmap3.min.js', __FILE__ ), array( 'jquery', 'google-maps' ), '7.1', true );
        wp_register_script( 'jquery-isotope', plugins_url( 'js/jquery.isotope.min.js', __FILE__ ), array( 'jquery' ), '2.2.0', true );
        wp_register_script( 'owl-carousel', plugins_url( 'js/owl.carousel.min.js', __FILE__ ), array( 'jquery' ), '1.3.3', true );
        wp_register_script( 'lity', plugins_url( 'js/lity.min.js', __FILE__ ), array( 'jquery' ), '3.0.0', true );
    }

    /**
     * Load shortcodes
     *
     * @return boolean
     * @access private
     */
    private function __load_shortcodes( ) {
        // Include options
        require_once plugin_dir_path( __FILE__ ) . 'register.php';

        if ( isset( $rocks_tools_shortcodes ) ) {
            $list = &$rocks_tools_shortcodes;
        } else {
            return false;
        }

        if ( is_array( $list ) ) {
            $prefix     = $this->__plugin_instance->get_option( 'shortcodes-prefix', '' );
            $shortcodes = $this->__plugin_instance->get_shortcodes( );

            if ( is_array( $shortcodes ) and count( $shortcodes ) > 0 ) {
                foreach ( $shortcodes as $name ) {
                    if ( array_key_exists( $name, $list['add'] ) ) {
                        $base = $prefix . $name;
                        $function = preg_replace( '/-/', '_', $name );

                        $details         = $list['add'][$name];
                        $details['base'] = $base;

                        // Register shortcode
                        if ( method_exists( 'Rocks_Tools_Shortcodes_Library', $function ) ) {
                            $this->__names[] = $base;

                            add_shortcode( $base, array( 'Rocks_Tools_Shortcodes_Library', $function ) );
                            vc_map( $details );
                        }
                    }
                }

                // Filter shortcodes
                if ( count( $this->__names ) > 0 ) {
                    add_filter( 'the_content', array( &$this, 'filter_shortcodes' ) );
                }
            }

            // Register other WordPress shortcodes
            // @todo
            add_shortcode( $prefix . 'highlight', array( 'Rocks_Tools_Shortcodes_Library', 'highlight' ) );

            // Update shortcodes
            if ( isset( $list['update'] ) and count( $list['update'] ) > 0 ) {
                Rocks_Tools_Shortcodes_Tweaks::init( );

                foreach ( $list['update'] as $name ) {
                    if ( method_exists( 'Rocks_Tools_Shortcodes_Tweaks', $name ) ) {
                        Rocks_Tools_Shortcodes_Tweaks::$name( );
                    }
                }
            }

            // Remove shortcodes
            if ( isset( $list['remove'] ) and count( $list['remove'] ) > 0 ) {
                foreach ( $list['remove'] as $name ) {
                    vc_remove_element( $name );
                }
            }
        }

        return true;
    }

    /**
     * Filter shortcodes
     *
     * @param  string $content
     * @return string
     * @access public
     */
    public function filter_shortcodes( $content ) {
        $this->__names[] = 'vc_row';
        $names = join( '|', $this->__names );

        $content = preg_replace( "/(<p>)?\[($names)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content );
        $content = preg_replace( "/(<p>)?\[\/($names)](<\/p>|<br \/>)?/", "[/$2]", $content );

        return $content;
    }

    /**
     * Get post types
     *
     * @return array
     * @access public
     */
    public function get_post_types( ) {
        $types = get_post_types( array( 'public' => true ) );

        foreach ( $types as &$type ) {
            $type = ucwords( $type );
        }

        return array_flip( $types );
    }

    public function add_field_iconpicker( $param = 'icon', $empty = false ) {
        $libraries  = array( );
        $icon_fonts = $this->__plugin_instance->get_icon_fonts( );
        $forced     = $this->__plugin_instance->get_option( 'icon-fonts' );
        $i          = 0;

        foreach ( $icon_fonts as $library ) {
            $libraries[$library->get_name( )] = ucwords( str_replace( '-', ' ', $library->get_name( ) ) );
        }

        $output = array( array(
            'heading'     => __( 'Icon Library', 'rocks' ),
            'description' => __( 'Choose the icon library', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'library',
            'std'         => $forced,
            'value'       => array_flip( $libraries ),
        ) );

        foreach ( $icon_fonts as $library ) {
            $output[++ $i] = array(
                'heading'     => __( 'Icon', 'js_composer' ),
                'description' => __( 'Choose an icon', 'rocks' ),
                'param_name'  => $param . '_' . str_replace( '-', '_', $library->get_name( ) ),
                'type'        => 'iconpicker',
                'value'       => '',
                'settings'    => array(
                    'type'         => $library->get_name( ),
                    'emptyIcon'    => $empty,
                    'iconsPerPage' => 4000,
                ),
                'dependency' => array(
                    'element' => 'library',
                    'value'   => $library->get_name( ),
                ),
            );
        }

        return $output;
    }

    public function add_field_class( $param = 'class' ) {
        return array( array(
            'heading'     => __( 'Extra Class', 'rocks' ),
            'description' => __( 'Enter class name, if you want to use one on frontend', 'rocks' ),
            'type'        => 'textfield',
            'param_name'  => $param,
        ) );
    }

    /**
     * Add animation field
     *
     * @param  string $param
     * @return array
     * @access public
     */
    public function add_field_animation( $param = 'css_animation', $label = false ) {
        return array( array(
            'heading'     => __( 'CSS Animation', 'rocks' ),
            'description' => __( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'rocks' ),
            'type'        => 'animation_style',
            'param_name'  => $param,
            'value'       => '',
            'admin_label' => $label,
            'settings'    => array(
                'type'   => 'in',
                'custom' => array(
                    array(
                        'label'  => __( 'Default', 'rocks' ),
                        'values' => array(
                            __( 'Top to bottom', 'rocks' )      => 'top-to-bottom',
                            __( 'Bottom to top', 'rocks' )      => 'bottom-to-top',
                            __( 'Left to right', 'rocks' )      => 'left-to-right',
                            __( 'Right to left', 'rocks' )      => 'right-to-left',
                            __( 'Appear from center', 'rocks' ) => 'appear',
                        ),
                    ),
                ),
            ),
        ) );
    }
}

// Initialization
add_action( 'rocks_tools_loaded', array( 'Rocks_Tools_Shortcodes', 'instance' ) );

// Load layouts
add_filter( 'vc_load_default_templates', array( 'Rocks_Tools_Shortcodes', 'load_layouts' ) );
