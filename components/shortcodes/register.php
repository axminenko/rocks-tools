<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

 // Columns
 $rocks_tools_shortcodes_columns = array_flip( array(
    '1/12'  => __( '1 column &ndash; 1/12', 'rocks' ),
    '1/6'   => __( '2 columns &ndash; 1/6', 'rocks' ),
    '1/4'   => __( '3 columns &ndash; 1/4', 'rocks' ),
    '1/3'   => __( '4 columns &ndash; 1/3', 'rocks' ),
    '5/12'  => __( '5 columns &ndash; 5/12', 'rocks' ),
    '1/2'   => __( '6 columns &ndash; 1/2', 'rocks' ),
    '7/12'  => __( '7 columns &ndash; 7/12', 'rocks' ),
    '2/3'   => __( '8 columns &ndash; 2/3', 'rocks' ),
    '3/4'   => __( '9 columns &ndash; 3/4', 'rocks' ),
    '5/6'   => __( '10 columns &ndash; 5/6', 'rocks' ),
    '11/12' => __( '11 columns &ndash; 11/12', 'rocks' ),
    '1/1'   => __( '12 columns &ndash; 1/1', 'rocks' ),
 ) );

// Shortcodes configuration
$rocks_tools_shortcodes = array(
    // Remove shortcodes
    'remove' => array(
        'vc_facebook',
        'vc_tweetmeme',
        'vc_googleplus',
        'vc_pinterest',
        'vc_flickr',
        'vc_media_grid',
        'vc_posts_slider',
        'vc_round_chart',
        'vc_line_chart',
        'vc_toggle',
        'vc_pie',

        /*
         * Replaced
         */

        'vc_text_separator',
        'vc_single_image',
        'vc_gallery',
        'vc_images_carousel',
        'vc_btn',
        'vc_cta',
        'vc_empty_space',
        'vc_progress_bar',
        'vc_message',
        'vc_gmaps',
        'vc_icon',
        'vc_separator',
        'vc_custom_heading',
        'vc_basic_grid',
        'vc_masonry_grid',
        'vc_masonry_media_grid',

        /*
         * Deprecated
         */

        'vc_tabs',
        'vc_tour',
        'vc_accordion',
        'vc_button',
        'vc_button2',
        'vc_cta_button',
        'vc_cta_button2',
    ),

    // Update shortcodes
    'update' => array(
        'vc_wp_custommenu',
        'vc_wp_recentcomments',
        'vc_wp_text',
        'vc_tta_accordion',
        'vc_tta_section',
        'vc_tta_tabs',
        'vc_tta_tour',
        'vc_tta_pageable',
        'vc_widget_sidebar',
        'vc_section',
        'vc_video',
    ),

    // Add shortcodes
    'add' => array(
        // Button
        'button' => array(
            'name'        => __( 'Button', 'rocks' ),
            'description' => __( 'Eye catching button', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-ui-button',
            'js_view'     => 'Rocks_Tools_VC_Button',
            'weight'      => 48,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Button Text', 'rocks' ),
                    'description' => __( 'Add the button text', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'content',
                    'std'         => __( 'Button Text', 'rocks' ),
                    'holder'      => 'button',
                    'class'       => 'rocks-vcs-button',
                ),

                array(
                    'heading'     => __( 'Address', 'rocks' ),
                    'description' => __( 'Add the button url e.g. http://google.com', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'url',
                    'std'         => site_url( ),
                ),

                array(
                    'heading'     => __( 'Target', 'rocks' ),
                    'description' => __( 'Open in same window or in new window', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'target',
                    'std'         => '_self',
                    'value'       => array_flip( array(
                        '_self'  => __( 'Same window', 'rocks' ),
                        '_blank' => __( 'New window', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Background Type', 'rocks' ),
                    'description' => __( 'Select the background type', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'background',
                    'std'         => 'solid',
                    'value'       => array_flip( array(
                         'solid'       => __( 'Solid color', 'rocks' ),
                         'transparent' => __( 'Transparent', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for button', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),

                array(
                    'heading'     => __( 'Button Size', 'rocks' ),
                    'description' => __( 'Select the button size', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'size',
                    'std'         => 'default',
                    'value'       => array_flip( array(
                        'small'   => __( 'Small', 'rocks' ),
                        'default' => __( 'Default', 'rocks' ),
                        'large'   => __( 'Large', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose the button alignment', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'default',
                    'value'       => array_flip( array(
                        'default' => __( 'Default', 'rocks' ),
                        'left'    => __( 'Left', 'rocks' ),
                        'center'  => __( 'Center', 'rocks' ),
                        'right'   => __( 'Right', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_iconpicker( 'icon', true ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Alert
        'alert' => array(
            'name'        => __( 'Message Box', 'rocks' ),
            'description' => __( 'Notification Box', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-information-white',
            'js_view'     => 'Rocks_Tools_VC_Alert',
            'weight'      => 32,
            'params'       => array_merge( array(
                array(
                    'heading'     => __( 'Message Type', 'rocks' ),
                    'description' => __( 'Select the message type', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'type',
                    'std'         => 'notice',
                    'admin_label' => true,
                    'value'       => array_flip( array(
                        'notice'  => __( 'Notice', 'rocks' ),
                        'warning' => __( 'Warning', 'rocks' ),
                        'success' => __( 'Success', 'rocks' ),
                        'error'   => __( 'Error', 'rocks' ),
                        'info'    => __( 'Info', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Message', 'rocks' ),
                    'description' => __( 'Type the alert message text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'Message goes here.', 'rocks' ),
                    'holder'      => 'div',
                    'class'       => 'rocks-vcs-alert',
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Dropcap
        'dropcap' => array(
            'name'         => __( 'Dropcap', 'rocks' ),
            'description'  => __( 'Paragraph with dropcap', 'rocks' ),
            'category'     => __( 'Content', 'rocks' ),
            'icon'         => 'rocks-icon-wpb-dropcap',
            'weight'       => 35,
            'params'       => array(
                array(
                    'heading'     => __( 'Primary Text', 'rocks' ),
                    'description' => __( 'Enter a primary text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'I am text block. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Letter Style', 'rocks' ),
                    'description' => __( 'Select a letter style', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'style',
                    'std'         => 'default',
                    'admin_label' => true,
                    'value'       => array_flip( array(
                        'default' => __( 'Default', 'rocks' ),
                        'circle'  => __( 'Circle', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose a letter color', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),
            ),
        ),

        // Progress bar
        'progress' => array(
            'name'        => __( 'Progress Bar', 'rocks' ),
            'description' => __( 'Animated progress bar', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-graph',
            'weight'      => 33,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Bar Type', 'rocks' ),
                    'description' => __( 'Choose an bar type', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'type',
                    'std'         => 'horizontal',
                    'value'       => array_flip( array(
                        'horizontal' => __( 'Horizontal', 'rocks' ),
                        'vertical'   => __( 'Vertical', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Bar Text', 'rocks' ),
                    'description' => __( 'Enter the progress bar label', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'content',
                    'std'         => __( 'Skill', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Bar Value', 'rocks' ),
                    'description' => __( 'Enter the progress value', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'skill',
                    'std'         => '60',
                ),

                array(
                    'heading'     => __( 'Unit', 'rocks' ),
                    'description' => __( 'Enter the unit to display', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'unit',
                    'std'         => __( '%', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the bar', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),
            ), $this->add_field_class( ) ),
        ),

        // Call to action
        'action' => array(
            'name'        => __( 'Call to Action', 'rocks' ),
            'description' => __( 'Catch visitors attention with CTA block', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-call-to-action',
            'weight'      => 46,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Heading Line', 'rocks' ),
                    'description' => __( 'Enter text for first heading line', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'heading',
                    'std'         => __( 'Hello, World!', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Promotional Text', 'rocks' ),
                    'description' => __( 'Add the promotional text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'Promotional text goes here.', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose align in call to action block', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'left',
                    'value'       => array_flip( array(
                        'left'    => __( 'Left', 'rocks' ),
                        'center'  => __( 'Center', 'rocks' ),
                        'right'   => __( 'Right', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Button Text', 'rocks' ),
                    'description' => __( 'Add the button text', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'button',
                    'std'         => __( 'Sign up now', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the button', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),

                array(
                    'heading'     => __( 'Address', 'rocks' ),
                    'description' => __( 'Add the button url e.g. http://google.com', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'url',
                    'std'         => site_url( ),
                ),
            ) ),
        ),

        // Separator
        'separator' => array(
            'name'        => __( 'Separator', 'rocks' ),
            'description' => __( 'Horizontal separator line', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-ui-separator',
            'weight'      => 45,
            'params'      => array(
                array(
                    'heading'     => __( 'Line Style', 'rocks' ),
                    'description' => __( 'Choose a separator line style', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'style',
                    'std'         => 'solid',
                    'value'       => array_flip( array(
                        'solid'  => __( 'Solid', 'rocks' ),
                        'dashed' => __( 'Dashed', 'rocks' ),
                        'dotted' => __( 'Dotted', 'rocks' ),
                        'double' => __( 'Double', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Separator Text', 'rocks' ),
                    'description' => __( 'Add the sepator text (optional)', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'content',
                    'std'         => '',
                ),
            ),
        ),

        // Icon
        'icon' => array(
            'name'        => __( 'Icon', 'rocks' ),
            'description' => __( 'Icon from library', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-icon',
            'weight'      => 47,
            'params'      => array_merge( $this->add_field_iconpicker( ), array(
                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the icon', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),

                array(
                    'heading'     => __( 'Size', 'rocks' ),
                    'description' => __( 'Enter the icon size, in pixels e.g 16', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'size',
                    'std'         => '',
                ),
            ), $this->add_field_class( ) ),
        ),

        // Icon Box
        'icon-box' => array(
            'name'        => __( 'Icon Box', 'rocks' ),
            'description' => __( 'Icon with some text', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-vc_icon',
            'weight'      => 47,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Heading Line', 'rocks' ),
                    'description' => __( 'Enter text for first heading line', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'heading',
                    'std'         => __( 'Hello, World!', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Primary Text', 'rocks' ),
                    'description' => __( 'Add the primary text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'Text goes here.', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose align in icon box', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'vertical',
                    'value'       => array_flip( array(
                        'vertical'   => __( 'Vertical', 'rocks' ),
                        'horizontal' => __( 'Horizontal', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Style', 'rocks' ),
                    'description' => __( 'Choose the icon box style', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'style',
                    'std'         => 'one',
                    'value'       => array_flip( array(
                        'one'   => sprintf( __( 'Style %d', 'rocks' ), '1' ),
                        'two'   => sprintf( __( 'Style %d', 'rocks' ), '2' ),
                        'three' => sprintf( __( 'Style %d', 'rocks' ), '3' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the icon', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),
            ), $this->add_field_iconpicker( ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Counter
        'counter' => array(
            'name'        => __( 'Counter', 'rocks' ),
            'description' => __( 'Display an automatic counter', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-counter',
            'weight'      => 34,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Counter Text', 'rocks' ),
                    'description' => __( 'Enter the counter text', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'heading',
                    'std'         => __( 'Hello, World!', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Counter Value', 'rocks' ),
                    'description' => __( 'Enter the counter value', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'skill',
                    'std'         => '60',
                ),

                array(
                    'heading'     => __( 'Unit', 'rocks' ),
                    'description' => __( 'Enter the unit to display', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'unit',
                    'std'         => __( '%', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Before', 'rocks' ),
                    'description' => __( 'You can enter some text before the counter value', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'before',
                ),

                array(
                    'heading'     => __( 'After', 'rocks' ),
                    'description' => __( 'You can enter some text after the counter value', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'after',
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the icon', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),
            ), $this->add_field_iconpicker( ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Pricing table
        'pricing-table' => array(
            'name'        => __( 'Pricing Table', 'rocks' ),
            'description' => __( 'A beauty pricing table', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-pricing-table',
            'weight'      => 36,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Table Style', 'rocks' ),
                    'description' => __( 'Choose a table style', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'table',
                    'std'         => 'default',
                    'value'       => array_flip( array(
                        'default'    => __( 'Default', 'rocks' ),
                        'additional' => __( 'Additional', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Plan Style', 'rocks' ),
                    'description' => __( 'Choose a plan style', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'plan',
                    'std'         => 'default',
                    'value'       => array_flip( array(
                        'default'  => __( 'Default', 'rocks' ),
                        'featured' => __( 'Featured', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Plan Name', 'rocks' ),
                    'description' => __( 'Enter the plan name', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'heading',
                    'std'         => __( 'Pricing Table', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Price Value', 'rocks' ),
                    'description' => __( 'Enter the price value e.g. $19', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'price',
                    'std'         => __( '$0', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Period', 'rocks' ),
                    'description' => __( 'Enter the period e.g. Month', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'period',
                    'std'         => __( 'Month', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Features', 'rocks' ),
                    'description' => __( 'New line is an list element', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                ),

                array(
                    'heading'     => __( 'Button Text', 'rocks' ),
                    'description' => __( 'Add the button text', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'button',
                    'std'         => __( 'Sign up now', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Button Target', 'rocks' ),
                    'description' => __( 'Open in same window or in new window', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'target',
                    'std'         => '_self',
                    'value'       => array_flip( array(
                        '_self'  => __( 'Same window', 'rocks' ),
                        '_blank' => __( 'New window', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose color for the plan', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Testimonial
        'testimonial' => array(
            'name'        => __( 'Testimonial', 'rocks' ),
            'description' => __( 'Output a testimonial widget', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-testimonial',
            'weight'      => 37,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Author Name', 'rocks' ),
                    'description' => __( 'Enter the author name', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'author',
                    'std'         => __( 'John Doe', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Company Name', 'rocks' ),
                    'description' => __( 'Enter the company e.g. Company Inc.', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'company',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Author Image', 'rocks' ),
                    'description' => __( 'Choose an user photo', 'rocks' ),
                    'type'        => 'attach_image',
                    'param_name'  => 'image',
                ),

                array(
                    'heading'     => __( 'Testimonial Text', 'rocks' ),
                    'description' => __( 'Enter the testimonial text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'Testimonial goes here.', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Rating', 'rocks' ),
                    'description' => __( 'Choose a rating', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'rating',
                    'std'         => 'none',
                    'value'       => array_flip( array(
                        'none' => __( 'None', 'rocks' ),
                        '1'    => __( 'One star', 'rocks' ),
                        '2'    => __( 'Two stars', 'rocks' ),
                        '3'    => __( 'Three stars', 'rocks' ),
                        '4'    => __( 'Four stars', 'rocks' ),
                        '5'    => __( 'Five stars', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Single image
        'image' => array(
            'name'        => __( 'Single Image', 'rocks' ),
            'description' => __( 'Single image with filter', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-single-image',
            'js_view'     => 'Rocks_Tools_VC_Image',
            'weight'      => 43,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Image', 'rocks' ),
                    'description' => __( 'Choose an image', 'rocks' ),
                    'type'        => 'attach_image',
                    'param_name'  => 'id',
                ),

                array(
                    'heading'     => __( 'Image Size', 'rocks' ),
                    'description' => __( 'Choose image size', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'size',
                    'std'         => 'full',
                    'admin_label' => true,
                    'value'       => array_flip( array(
                        'thumbnail' => __( 'Thumbnail (default 150x150)', 'rocks' ),
                        'medium'    => __( 'Medium (default 300x300)', 'rocks' ),
                        'large'     => __( 'Large (default 640x640)', 'rocks' ),
                        'full'      => __( 'Full (original image resolution)', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose image alignment', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'none',
                    'value'       => array_flip( array(
                        'none'   => __( 'None', 'rocks' ),
                        'left'   => __( 'Left', 'rocks' ),
                        'center' => __( 'Center', 'rocks' ),
                        'right'  => __( 'Right', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Click', 'rocks' ),
                    'description' => __( 'Choose action on click image', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'onclick',
                    'std'         => 'none',
                    'value'       => array_flip( array(
                        'none'     => __( 'None', 'rocks' ),
                        '_self'    => __( 'Open image in same window', 'rocks' ),
                        '_blank'   => __( 'Open image in new window', 'rocks' ),
                        'lightbox' => __( 'Open image in Lightbox', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Image slider
        'slider' => array(
            'name'        => __( 'Image Slider', 'rocks' ),
            'description' => __( 'Responsive image slider', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-images-stack',
            'js_view'     => 'Rocks_Tools_VC_Slider',
            'weight'      => 42,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Images', 'rocks' ),
                    'description' => __( 'Choose your images', 'rocks' ),
                    'type'        => 'attach_images',
                    'param_name'  => 'images',
                ),

                array(
                    'heading'     => __( 'Autoplay', 'rocks' ),
                    'description' => __( 'Enter any number for example 5 to play every 5 seconds', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'autoplay',
                ),

                array(
                    'heading'     => __( 'Navigation', 'rocks' ),
                    'description' => __( 'Select a navigation mode', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'navigation',
                    'std'         => 'arrows',
                    'value'       => array_flip( array(
                        'slides' => __( 'Slides (bottom)', 'rocks' ),
                        'arrows' => __( 'Arrows (prev. and next buttons)', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Google map
        'google-map' => array(
            'name'        => __( 'Google Maps', 'rocks' ),
            'description' => __( 'The Google Map block', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-map-pin',
            'weight'      => 30,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Map Type', 'rocks' ),
                    'description' => __( 'Choose the map type', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'maptype',
                    'std'         => 'roadmap',
                    'value'       => array_flip( array(
                        'roadmap'   => __( 'Default road map', 'rocks' ),
                        'satellite' => __( 'Google Earth satellite images', 'rocks' ),
                        'hybrid'    => __( 'Mixture of normal and satellite views', 'rocks' ),
                        'terrain'   => __( 'Physical map', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 1 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-1',
                    'type'        => 'textfield',
                    'std'         => '40.6700, -73.9400',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 2 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-2',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 3 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-3',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 4 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-4',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 5 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-5',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 6 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-6',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 7 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-7',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 8 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-8',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 9 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-9',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => sprintf( __( 'Point %d', 'rocks' ), 10 ),
                    'description' => __( 'Enter the place address or coordinates', 'rocks' ),
                    'group'       => __( 'Points', 'rocks' ),
                    'param_name'  => 'point-10',
                    'type'        => 'textfield',
                    'std'         => '',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Color Scheme', 'rocks' ),
                    'description' => __( 'Select an color scheme', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'scheme',
                    'std'         => $this->get_plugin_instance( )->get_option( 'map-scheme', 'default' ),
                    'value'       => array_flip( array(
                        'apple-maps-esque'  => __( 'Apple Maps-esque', 'rocks' ),
                        'blue-essence'      => __( 'Blue Essence', 'rocks' ),
                        'bright-and-bubbly' => __( 'Bright and Bubbly', 'rocks' ),
                        'default'           => __( 'Default', 'rocks' ),
                        'light-monochrome'  => __( 'Light Monochrome', 'rocks' ),
                        'muted-blue'        => __( 'Muted Blue', 'rocks' ),
                        'pale-dawn'         => __( 'Pale Dawn', 'rocks' ),
                        'paper'             => __( 'Paper', 'rocks' ),
                        'shades-of-grey'    => __( 'Shades of Grey', 'rocks' ),
                        'ultra-light'       => __( 'Ultra Light', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Marker', 'rocks' ),
                    'description' => __( 'Choose the marker image (optional)' ),
                    'param_name'  => 'marker',
                    'type'        => 'attach_image',
                ),

                array(
                    'heading'     => __( 'Width', 'rocks' ),
                    'description' => __( 'Enter the map width', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'width',
                    'std'         => '100%',
                ),

                array(
                    'heading'     => __( 'Height', 'rocks' ),
                    'description' => __( 'Enter the map height', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'height',
                    'std'         => '300px',
                ),

                array(
                    'heading'     => __( 'Zoom Level', 'rocks' ),
                    'description' => __( 'Enter the map zoom level between 0-21. Highest value zooms in and lowest zooms out', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'zoom',
                    'std'         => '11',
                ),
            ), $this->add_field_class( ) ),
        ),

        // Twitter feed
        'twitter-feed' => array(
            'name'        => __( 'Twitter Feed', 'rocks' ),
            'description' => __( 'Output a Twitter feed', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-tweetme',
            'weight'      => 30,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Twitter Account', 'rocks' ),
                    'description' => __( 'Enter the account name e.g. @envato', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'id',
                    'std'         => '@envato',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Number of Tweets', 'rocks' ),
                    'description' => __( 'Number of tweets to show on a page', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'limit',
                    'std'         => '10',
                ),

                array(
                    'heading'     => __( 'Autoplay', 'rocks' ),
                    'description' => __( 'Enter any number for example 5 to play every 5 seconds', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'autoplay',
                ),
            ), $this->add_field_class( ) ),
        ),

        // Empty space
        'clear' => array(
            'name'        => __( 'Empty Space', 'rocks' ),
            'description' => __( 'Blank space with custom height', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-ui-empty_space',
            'weight'      => 44,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Height of Gap', 'rocks' ),
                    'description' => __( 'Enter the height of gap', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'gap',
                    'std'         => '0',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Hidden on large devices', 'rocks' ),
                    'description' => __( 'Option hide this element on large devices (monitors)', 'rocks' ),
                    'group'       => __( 'Responsive Options', 'rocks' ),
                    'type'        => 'checkbox',
                    'param_name'  => 'hidden_lg',
                ),

                array(
                    'heading'     => __( 'Hidden on medium devices', 'rocks' ),
                    'description' => __( 'Option hide this element on medium devices (some monitors)', 'rocks' ),
                    'group'       => __( 'Responsive Options', 'rocks' ),
                    'type'        => 'checkbox',
                    'param_name'  => 'hidden_md',
                ),

                array(
                    'heading'     => __( 'Hidden on small devices', 'rocks' ),
                    'description' => __( 'Option hide this element on small devices (tablets)', 'rocks' ),
                    'group'       => __( 'Responsive Options', 'rocks' ),
                    'type'        => 'checkbox',
                    'param_name'  => 'hidden_sm',
                ),

                array(
                    'heading'     => __( 'Hidden on extra small devices', 'rocks' ),
                    'description' => __( 'Option hide this element on extra small devices (phones)', 'rocks' ),
                    'group'       => __( 'Responsive Options', 'rocks' ),
                    'type'        => 'checkbox',
                    'param_name'  => 'hidden_xs',
                ),
            ) ),
        ),

        // Quote
        'pullquote' => array(
            'name'        => __( 'Block Quote', 'rocks' ),
            'description' => __( 'Display a block with quote', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-quote',
            'weight'      => 35,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose quote alignment', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'left',
                    'value'       => array_flip( array(
                        'none'   => __( 'None', 'rocks' ),
                        'left'   => __( 'Left', 'rocks' ),
                        'right'  => __( 'Right', 'rocks' )
                    ) ),
                ),

                array(
                    'heading'     => __( 'Text', 'rocks' ),
                    'description' => __( 'Add the quote text', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'content',
                    'std'         => __( 'Quote goes here.', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Author', 'rocks' ),
                    'description' => __( 'Enter the author name', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'author',
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Custom heading
        'heading' => array(
            'name'        => __( 'Custom Heading', 'rocks' ),
            'description' => __( 'Output custom text on your page', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-ui-custom_heading',
            'weight'      => 50,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Text', 'rocks' ),
                    'description' => __( 'Add the custom text', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'value'       => __( 'This is custom heading element', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Tag', 'rocks' ),
                    'description' => __( 'Select element tag', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'tag',
                    'std'         => 'h2',
                    'value'       => array_flip( array(
                        'h1' => __( 'H1', 'rocks' ),
                        'h2' => __( 'H2', 'rocks' ),
                        'h3' => __( 'H3', 'rocks' ),
                        'h4' => __( 'H4', 'rocks' ),
                        'h5' => __( 'H5', 'rocks' ),
                        'h6' => __( 'H6', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Select element tag', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'none',
                    'value'       => array_flip( array(
                        'none'   => __( 'None', 'rocks' ),
                        'left'   => __( 'Left', 'rocks' ),
                        'center' => __( 'Center', 'rocks' ),
                        'right'  => __( 'Right', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Social icons
        'social-icons' => array(
            'name'        => __( 'Social Icons', 'rocks' ),
            'description' => __( 'Beauty social icons', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-social',
            'weight'      => 35,
            'params'      => array(
                array(
                    'heading'     => __( 'Style', 'rocks' ),
                    'description' => __( 'Choose a style for icons', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'style',
                    'std'         => 'one',
                    'value'       => array_flip( array(
                        'one'   => sprintf( __( 'Style %d', 'rocks' ), '1' ),
                        'two'   => sprintf( __( 'Style %d', 'rocks' ), '2' ),
                        'three' => sprintf( __( 'Style %d', 'rocks' ), '3' ),
                        'four'  => sprintf( __( 'Style %d', 'rocks' ), '4' ),
                        'five'  => sprintf( __( 'Style %d', 'rocks' ), '5' ),
                        'six'   => sprintf( __( 'Style %d', 'rocks' ), '6' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Color', 'rocks' ),
                    'description' => __( 'Choose a color mode for icons', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'color',
                    'std'         => 'brands',
                    'value'       => array_flip( array(
                        'brands'    => __( 'Brand Colors', 'rocks' ),
                        'primary'   => __( 'Primary Color', 'rocks' ),
                        'grayscale' => __( 'Monochrome', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Links', 'rocks' ),
                    'description' => __( 'Paste any social links', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'links',
                    'admin_label' => true,
                ),
            ),
        ),

        // Posts grid
        'posts-grid' => array(
            'name'        => __( 'Posts Grid', 'rocks' ),
            'description' => __( 'Posts, pages or custom posts in grid', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'icon-wpb-application-icon-large',
            'weight'      => 30,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Post Width', 'rocks' ),
                    'description' => __( 'Select a post width', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'width',
                    'std'         => '1/4',
                    'value'       => $rocks_tools_shortcodes_columns,
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Post Type', 'rocks' ),
                    'description' => __( 'You can use any post type of defined', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'post_type',
                    'std'         => 'post',
                    'value'       => $this->get_post_types( ),
                ),

                array(
                    'heading'     => __( 'Number of Posts', 'rocks' ),
                    'description' => __( 'Number of posts to show on a page', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'per_page',
                    'value'       => '12',
                ),

                array(
                    'heading'     => __( 'Categories', 'rocks' ),
                    'description' => __( 'Optional, slugs separated by comma', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'category',
                ),

                array(
                    'heading'     => __( 'Sort', 'rocks' ),
                    'description' => __( 'Sort retrieved posts by parameter', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'orderby',
                    'std'         => 'date',
                    'value' => array_merge( array(
                        'date'          => __( 'Recent', 'rocks' ),
                        'rand'          => __( 'Random order', 'rocks' ),
                        'comment_count' => __( 'Most commented', 'rocks' ),
                    ), ( $this->get_plugin_instance( )->theme_support( 'post-views', 'components' ) ? array( 'view_count' => __( 'Most viewed', 'rocks' ) ) : array( ) ) ),
                ),

                array(
                    'heading'     => __( 'Order', 'rocks' ),
                    'description' => __( 'Choose the order mode', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'order',
                    'std'         => 'desc',
                    'value'       => array_flip( array(
                        'asc'  => __( 'Ascending order', 'rocks' ),
                        'desc' => __( 'Descending order', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Masonry grid
        'masonry-grid' => array(
            'name'        => __( 'Masonry Grid', 'rocks' ),
            'description' => __( 'Posts, pages or custom posts in masonry grid', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'vc_icon-vc-masonry-grid',
            'weight'      => 30,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Post Width', 'rocks' ),
                    'description' => __( 'Select a post width', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'width',
                    'std'         => '1/4',
                    'value'       => $rocks_tools_shortcodes_columns,
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Post Type', 'rocks' ),
                    'description' => __( 'You can use any post type of defined', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'post_type',
                    'std'         => 'post',
                    'value'       => $this->get_post_types( ),
                ),

                array(
                    'heading'     => __( 'Pagination', 'rocks' ),
                    'description' => __( 'You can enable or disable pagination', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'pagination',
                    'std'         => 'disabled',
                    'value'       => array_flip( array(
                        'disabled' => __( 'Disabled', 'rocks' ),
                        'enabled'  => __( 'Enabled', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Posts Per Page', 'rocks' ),
                    'description' => __( 'Number of posts to show on a page', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'per_page',
                    'value'       => '12',
                ),

                array(
                    'heading'     => __( 'Categories', 'rocks' ),
                    'description' => __( 'Optional, slugs separated by comma', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'category',
                ),

                array(
                    'heading'     => __( 'Sort', 'rocks' ),
                    'description' => __( 'Sort retrieved posts by parameter', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'orderby',
                    'std'         => 'date',
                    'value' => array_merge( array(
                        'date'          => __( 'Recent', 'rocks' ),
                        'rand'          => __( 'Random order', 'rocks' ),
                        'comment_count' => __( 'Most commented', 'rocks' ),
                    ), ( $this->get_plugin_instance( )->theme_support( 'post-views', 'components' ) ? array( 'view_count' => __( 'Most viewed', 'rocks' ) ) : array( ) ) ),
                ),

                array(
                    'heading'     => __( 'Order', 'rocks' ),
                    'description' => __( 'Choose the order mode', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'order',
                    'std'         => 'desc',
                    'value'       => array_flip( array(
                        'asc'  => __( 'Ascending order', 'rocks' ),
                        'desc' => __( 'Descending order', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Masonry media
        'masonry-media' => array(
            'name'        => __( 'Masonry Media', 'rocks' ),
            'description' => __( 'Masonry media grid from media library', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'vc_icon-vc-masonry-media-grid',
            'weight'      => 30,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Image Width', 'rocks' ),
                    'description' => __( 'Select an image width', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'width',
                    'std'         => '1/4',
                    'value'       => $rocks_tools_shortcodes_columns,
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Images', 'rocks' ),
                    'description' => __( 'Choose your images', 'rocks' ),
                    'type'        => 'attach_images',
                    'param_name'  => 'images',
                ),

                array(
                    'heading'     => __( 'Click', 'rocks' ),
                    'description' => __( 'Choose action on click image', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'onclick',
                    'std'         => 'none',
                    'value'       => array_flip( array(
                        'none'     => __( 'None', 'rocks' ),
                        '_self'    => __( 'Open image in same window', 'rocks' ),
                        '_blank'   => __( 'Open image in new window', 'rocks' ),
                        'lightbox' => __( 'Open image in Lightbox', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Team member
        'team-member' => array(
            'name'        => __( 'Team Member', 'rocks' ),
            'description' => __( 'Story of team member', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-team-member',
            'weight'      => 35,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Style', 'rocks' ),
                    'description' => __( 'Select a block width', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'style',
                    'std'         => 'one',
                    'options' => array_flip( array(
                        'one'   => sprintf( __( 'Style %d', 'rocks' ), '1' ),
                        'two'   => sprintf( __( 'Style %d', 'rocks' ), '2' ),
                        'three' => sprintf( __( 'Style %d', 'rocks' ), '3' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Photo', 'rocks' ),
                    'description' => __( 'Choose a photo', 'rocks' ),
                    'type'        => 'attach_image',
                    'param_name'  => 'image',
                ),

                array(
                    'heading'     => __( 'Name', 'rocks' ),
                    'description' => __( 'Enter the name', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'name',
                    'std'         => __( 'John Doe', 'rocks' ),
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Skill', 'rocks' ),
                    'description' => __( 'You can enter any information about any skills e.g. Developer', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'skill',
                ),

                array(
                    'heading'     => __( 'Member Story', 'rocks' ),
                    'description' => __( 'Enter a small story or biography', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                    'std'         => __( 'Story goes here.', 'rocks' ),
                ),

                array(
                    'heading'     => __( 'Read More Button', 'rocks' ),
                    'description' => __( 'Paste any link for the "Read More" button', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'url',
                ),

                array(
                    'heading'     => __( 'Social Links', 'rocks' ),
                    'description' => __( 'Paste any social links', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'links',
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // Clients
        'clients' => array(
            'name'        => __( 'Clients', 'rocks' ),
            'description' => __( 'Your favorite clients', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-clients',
            'weight'      => 35,
            'params'      => array_merge( array(
                array(
                    'heading'     => __( 'Images', 'rocks' ),
                    'description' => __( 'Choose your images', 'rocks' ),
                    'type'        => 'attach_images',
                    'param_name'  => 'images',
                ),

                array(
                    'heading'     => __( 'Carousel', 'rocks' ),
                    'description' => __( 'Enable or disable animated carousel', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'carousel',
                    'std'         => 'disabled',
                    'options' => array_flip( array(
                        'disabled' => __( 'Disabled', 'rocks' ),
                        'enabled'  => __( 'Enabled', 'rocks' ),
                    ) ),
                ),

                array(
                    'heading'     => __( 'Autoplay', 'rocks' ),
                    'description' => __( 'Enter any number for example 5 to play every 5 seconds', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'autoplay',
                ),

                array(
                    'heading'     => __( 'Items Per Row', 'rocks' ),
                    'description' => __( 'Number of items in carousel per row', 'rocks' ),
                    'type'        => 'textfield',
                    'param_name'  => 'items',
                    'std'         => '6',
                    'admin_label' => true,
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose alignment for the block', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'left',
                    'options' => array_flip( array(
                        'left'   => __( 'Left', 'rocks' ),
                        'center' => __( 'Center', 'rocks' ),
                        'right'  => __( 'Right', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_animation( ), $this->add_field_class( ) ),
        ),

        // List with icons
        'custom-list' => array(
            'name'        => __( 'List with Icons', 'rocks' ),
            'description' => __( 'Output a custom list', 'rocks' ),
            'category'    => __( 'Content', 'rocks' ),
            'icon'        => 'rocks-icon-wpb-custom-list',
            'weight'      => 35,
            'params'      => array_merge( $this->add_field_iconpicker( ), array(
                array(
                    'heading'     => __( 'Elements', 'rocks' ),
                    'description' => __( 'New line is an list element', 'rocks' ),
                    'type'        => 'textarea',
                    'param_name'  => 'content',
                ),

                array(
                    'heading'     => __( 'Icon Color', 'rocks' ),
                    'description' => __( 'Choose color for icons', 'rocks' ),
                    'type'        => 'colorpicker',
                    'param_name'  => 'color',
                    'std'         => '',
                ),

                array(
                    'heading'     => __( 'Alignment', 'rocks' ),
                    'description' => __( 'Choose text and icons alignment', 'rocks' ),
                    'type'        => 'dropdown',
                    'param_name'  => 'align',
                    'std'         => 'left',
                    'options' => array_flip( array(
                        'left'   => __( 'Left', 'rocks' ),
                        'right'  => __( 'Right', 'rocks' ),
                    ) ),
                ),
            ), $this->add_field_class( ) ),
        ),
    ),
);
