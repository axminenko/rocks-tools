<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Visual Composer shortcodes list
 *
 * @package    Create_Rocks_Tools
 * @subpackage Shortcodes_Tweaks
 */
class Rocks_Tools_Shortcodes_Tweaks {
    /**
     * Initialization
     *
     * @access public
     * @static
     */
    public static function init( ) {
        $details = array(
            'vc_row'           => 50,
            'vc_section'       => 51,
            'vc_column_text'   => 49,
            'vc_tta_accordion' => 40,
            'vc_tta_pageable'  => 40,
            'vc_tta_tabs'      => 39,
            'vc_tta_tour'      => 38,
            'vc_video'         => 31,
            'vc_raw_html'      => 29,
            'vc_raw_js'        => 28,
        );

        foreach( $details as $name => $weight ) {
            vc_map_update( $name, array(
                'weight' => $weight,
            ) );
        }
    }

    /**
     * Remove shortcode params
     *
     * @param string       $shortcode
     * @param array|string $params
     * @access private
     * @static
     */
    private static function __remove_params( $shortcode, $params ) {
        if ( is_array( $params ) ) {
            foreach ( $params as $name ) {
                vc_remove_param( $shortcode, $name );
            }
        } else {
            vc_remove_param( $shortcode, $params );
        }
    }

    /**
     * Widgetized Sidebar
     *
     * @access public
     * @static
     */
    public static function vc_widget_sidebar( ) {
        vc_map_update( __FUNCTION__, array(
            'name' => __( 'Custom Sidebar', 'rocks' ),
        ) );
    }

    /**
     * WP Custom Menu
     *
     * @access public
     * @static
     */
    public static function vc_wp_custommenu( ) {
        vc_map_update( __FUNCTION__, array(
            'description' => __( 'One of your custom menus as a widget', 'rocks' ),
        ) );
    }

    /**
     * WP Text
     *
     * @access public
     * @static
     */
    public static function vc_wp_text( ) {
        vc_map_update( __FUNCTION__, array(
            'description' => __( 'Output custom text on your page', 'rocks' ),
        ) );
    }

    /**
     * WP Recent Comments
     *
     * @access public
     * @static
     */
    public static function vc_wp_recentcomments( ) {
        vc_map_update( __FUNCTION__, array(
            'name' => __( 'WP Comments', 'rocks' ),
        ) );
    }

    /**
     * Accordion
     *
     * @access public
     * @static
     */
    public static function vc_tta_accordion( ) {
        self::__remove_params( __FUNCTION__, array(
            'title',
            'style',
            'shape',
            'color',
            'no_fill',
            'spacing',
            'gap',
            'c_align',
            'autoplay',
            'c_icon',
            'c_position',
            'active_section',
        ) );
    }

    /**
     * Section
     *
     * @access public
     * @static
     */
    public static function vc_tta_section( ) {
        self::__remove_params( __FUNCTION__, array(
            'i_type',
            'i_icon_fontawesome',
            'i_icon_openiconic',
            'i_icon_typicons',
            'i_icon_entypo',
            'i_icon_linecons',
            'i_icon_monosocial',
            'i_icon_material',
        ) );
    }

    /**
     * Tabs
     *
     * @access public
     * @static
     */
    public static function vc_tta_tabs( $name = false ) {
        self::__remove_params( ( ! $name ? __FUNCTION__ : $name ), array(
            'title',
            'style',
            'shape',
            'color',
            'spacing',
            'gap',
            'alignment',
            'pagination_style',
            'pagination_color',
        ) );
    }

    /**
     * Tour
     *
     * @access public
     * @static
     */
    public static function vc_tta_tour( ) {
        self::vc_tta_tabs( __FUNCTION__ );
    }

    /**
     * Pageable container
     *
     * @access public
     * @static
     */
    public static function vc_tta_pageable( ) {
        self::__remove_params( __FUNCTION__, array(
            'title',
            'autoplay',
            'pagination_color',
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Autoplay', 'rocks' ),
            'description' => __( 'Enter any number for example 5 to play every 5 seconds', 'rocks' ),
            'type'        => 'textfield',
            'param_name'  => 'autoplay',
            'weight'      => 1,
            'std'         => '',
        ) );
    }

    /**
     * Section
     *
     * @access public
     * @static
     */
    public static function vc_section( ) {
        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Section width', 'rocks' ),
            'description' => __( 'Select a section mode', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'section_width',
            'weight'      => 1,
            'std'         => 'normal',
            'value'       => array_flip( array(
                'normal'    => __( 'Normal', 'rocks' ),
                'fullwidth' => __( 'Fullwidth', 'rocks' ),
            ) ),
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Offset sizes', 'rocks' ),
            'description' => __( 'Select a size of offsets', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'section_offsets',
            'weight'      => 1,
            'std'         => 'default',
            'value'       => array_flip( array(
                'default' => __( 'Default', 'rocks' ),
                'small'   => __( 'Small', 'rocks' ),
                'none'    => __( 'None', 'rocks' ),
            ) ),
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Background color', 'rocks' ),
            'description' => __( 'Select a background color for the section', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'section_background',
            'weight'      => 1,
            'std'         => 'auto',
            'value'       => array_flip( array(
                'auto'    => __( 'Automatically', 'rocks' ),
                'primary' => __( 'Primary', 'rocks' ),
                'white'   => __( 'White', 'rocks' ),
                'gray'    => __( 'Gray', 'rocks' ),
            ) ),
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Content color', 'rocks' ),
            'description' => __( 'Select a color mode', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'section_colors',
            'weight'      => 1,
            'std'         => 'dark',
            'value'       => array_flip( array(
                'dark'  => __( 'Dark', 'rocks' ),
                'light' => __( 'Light', 'rocks' ),
            ) ),
        ) );
    }

    /**
     * Video Player
     *
     * @access public
     * @static
     */
    public static function vc_video( ) {
        self::__remove_params( __FUNCTION__, array(
            'title',
            'el_width',
            'el_aspect',
            'align',
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Insert Mode', 'rocks' ),
            'description' => __( 'Choose a video insert mode', 'rocks' ),
            'type'        => 'dropdown',
            'param_name'  => 'mode',
            'weight'      => 1,
            'std'         => 'direct',
            'value'       => array_flip( array(
                'direct'   => __( 'Direct Insert', 'rocks' ),
                'lightbox' => __( 'With Lightbox', 'rocks' ),
            ) ),
        ) );

        vc_add_param( __FUNCTION__ , array(
            'heading'     => __( 'Preview Image', 'rocks' ),
            'description' => __( 'Preview image needed only for the lightbox insert mode', 'rocks' ),
            'type'        => 'attach_image',
            'param_name'  => 'preview',
            'weight'      => 1,
        ) );
    }
}
