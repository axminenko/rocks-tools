/*!
 * Create.Rocks Tools | Version 0.1.0 | http://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */

jQuery( document ).ready( function( $ ) {
    'use strict';

    // Rocks Tools
    var Rocks_Tools = {
        // Options
        options: {
            progress_speed: 1000,
            counter_speed:  1000,
            slider_delay:   5000,
        },

        // Events
        events: {
            theme_loaded: 'theme.loaded',
        },

        // Constructor
        construct: function( ) {
            this.animations( );
            this.accordion( );
            this.tta( );
            this.progress( ); // +update
            this.counter( ); // +update

            // Dynamic shortcodes
            this.do_update( );
        },

        // Do update for dynamic shortcodes
        do_update: function( ) {
            this.maps( );
            this.masonry( );
            this.carousel( );
        },

        // Is touch
        is_touch: function( ) {
            if ( typeof Modernizr === 'undefined' ) {
                return false;
            }

            return $( 'html' ).hasClass( 'touch' );
        },

        // Is init
        is_init: function( element ) {
            if ( $( element ).data( 'data-rocks-tools-shortcode' ) === true ) {
                return true;
            } else {
                $( element ).data( 'data-rocks-tools-shortcode', true );
            }

            return false;
        },

        // On scroll
        onscroll: function( items, test, complete ) {
            var that = this;

            function scroll_event( ) {
                var scroll_top = $( window ).scrollTop( ),
                    item_top;

                items.each( function( ) {
                    item_top = $( this ).offset( ).top - $( window ).height( ) + $( this ).height( ) / 2.5;

                    if ( scroll_top > item_top && test( $( this ) ) ) {
                        complete( $( this ) );
                    }
                } );
            }

            setTimeout( function( ) {
                // Event
                $( window ).on( 'scroll', scroll_event );

                // Manually run
                scroll_event( );
            }, 1 );
        },

        // Format number
        format_number: function( number, dec_places, thou_separator, dec_separator ) {
            dec_places     = isNaN( dec_places = Math.abs( dec_places ) ) ? 0 : dec_places;
            dec_separator  = dec_separator === undefined ? '.' : dec_separator;
            thou_separator = thou_separator === undefined ? ',' : thou_separator;

            var sign = number < 0 ? '-' : '',
                i    = parseInt( number = Math.abs( + number || 0 ).toFixed( dec_places ) ) + '',
                j    = ( j = i.length ) > 3 ? j % 3 : 0;

            return sign + ( j ? i.substr( 0, j ) + thou_separator : '' ) + i.substr( j ).replace( /(\d{3})(?=\d)/g, "$1" + thou_separator ) + ( dec_places ? dec_separator + Math.abs( number - i ).toFixed( dec_places ).slice( 2 ) : '' );
        },

        // Animations
        animations: function( ) {
            var that  = this,
                items = $( '.wpb_animate_when_almost_visible' ),
                delay;

            items.each( function( i ) {
                if ( $( this ).hasClass( 'wpb_start_animation' ) || $( this ).hasClass( 'wpb_none' ) ) {
                    return true;
                }

                var this_top = $( this ).offset( ).top,
                    prev_top = $( items.get( i - 1 ) ).offset( ).top;

                if ( i > 0 && (
                    this_top      == prev_top ||
                    this_top - 20 <= prev_top ||
                    this_top + 20 <= prev_top
                ) ) {
                    delay ++;
                } else {
                    delay = 0;
                }

                $( this ).css( {
                    '-webkit-animation-delay': delay * 150 + 'ms',
                    '-moz-animation-delay':    delay * 150 + 'ms',
                    '-ms-animation-delay':     delay * 150 + 'ms',
                    'animation-delay':         delay * 150 + 'ms',
                } );
            } );
        },

        // Accordion
        accordion: function( ) {
            $( document ).on( 'click', '.accordion .accordion-heading', function( e ) {
                e.preventDefault( );

                var section   = $( this ).parent( ),
                    is_closed = ! section.hasClass( 'accordion-active' ),
                    opened    = section.parent( ).find( '.accordion-active' );

                opened.find( '.accordion-content' ).slideUp( 250, function( ) {
                    opened.removeClass( 'accordion-active' );
                } );

                if ( is_closed ) {
                    section.find( '.accordion-content' ).slideDown( 250, function( ) {
                        section.addClass( 'accordion-active' );
                    } );
                }
            } );
        },

        // Tabs and tour
        tta: function( ) {
            $( document ).on( 'click', '.tabs > .tabs-heading li, .tour > .tour-heading li', function( e ) {
                e.preventDefault( );

                if ( ! $( this ).hasClass( 'tabs-active' ) && ! $( this ).hasClass( 'tour-active' ) ) {
                    var tabs = $( this ).parent( ).parent( ).parent( ),
                        mode,
                        tab,
                        last;

                    if ( tabs.hasClass( 'tabs' ) ) {
                        mode = 'tabs';
                    } else if ( tabs.hasClass( 'tour' ) ) {
                        mode = 'tour';
                    } else {
                        return;
                    }

                    tab  = tabs.find( '.' + mode + '-section' ).eq( $( this ).index( ) );
                    last = tabs.find( '.' + mode + '-section:visible' );

                    $( this ).parent( ).find( 'li.' + mode + '-active' ).removeClass( mode + '-active' );
                    $( this ).addClass( mode + '-active' );

                    last.animate( { opacity: 0 }, { duration: 100, queue: false, complete: function( ) {
                        last.hide( );
                        tab.show( ).animate( { opacity: 1 }, { duration: 100, queue: false } );
                    } } );
                }
            } );
        },

        // Google maps
        maps: function( ) {
            var that = this;

            // Color shemes
            // https://snazzymaps.com/
            function map_style( scheme ) {
                // Ultra Light
                if ( scheme == 'ultra-light' ) {
                    return [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];
                }

                // Shades of Grey
                else if ( scheme == 'shades-of-grey' ) {
                    return [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}];
                }

                // Pale Dawn
                else if ( scheme == 'pale-dawn' ) {
                    return [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]}];
                }

                // Blue Essence
                else if ( scheme == 'blue-essence' ) {
                    return [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}];
                }

                // Apple Maps-esque
                else if ( scheme == 'apple-maps-esque' ) {
                    return [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}];
                }

                // Light Monochrome
                else if ( scheme == 'light-monochrome' ) {
                    return [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}];
                }

                // Paper
                else if ( scheme == 'paper' ) {
                    return [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"},{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]}];
                }

                // Muted Blue
                else if ( scheme == 'muted-blue' ) {
                    return [{"featureType":"all","stylers":[{"saturation":0},{"hue":"#e7ecf0"}]},{"featureType":"road","stylers":[{"saturation":-70}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"visibility":"simplified"},{"saturation":-60}]}];
                }

                // Bright and Bubbly
                else if ( scheme == 'bright-and-bubbly' ) {
                    return [{"featureType":"water","stylers":[{"color":"#19a0d8"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"weight":6}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#e85113"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-40}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#efe9e4"},{"lightness":-20}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"road.highway","elementType":"labels.icon"},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","stylers":[{"lightness":20},{"color":"#efe9e4"}]},{"featureType":"landscape.man_made","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":-100}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"hue":"#11ff00"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"lightness":100}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"hue":"#4cff00"},{"saturation":58}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#f0e4d3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#efe9e4"},{"lightness":-10}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"simplified"}]}];
                }

                // Something else or Default
                return [];
            }

            $( '.google-map' ).each( function( ) {
                if ( that.is_init( this ) ) {
                    return true;
                }

                var maptype = $( this ).attr( 'data-maptype' )          || false,
                    scheme  = $( this ).attr( 'data-scheme' )           || '',
                    zoom    = parseInt( $( this ).attr( 'data-zoom' ) ) || 11,
                    count   = parseInt( $( this ).attr( 'data-points' ) ),
                    points  = [],
                    center,
                    i;

                if ( maptype == 'TERRAIN' ) {
                    maptype = google.maps.MapTypeId.TERRAIN;
                } else if ( maptype == 'HYBRID' ) {
                    maptype = google.maps.MapTypeId.HYBRID;
                } else if ( maptype == 'SATELLITE' ) {
                    maptype = google.maps.MapTypeId.SATELLITE;
                } else {
                    maptype = google.maps.MapTypeId.ROADMAP;
                }

                if ( count > 0 ) {
                    for ( i = 1; i <= count; i += 1 ) {
                        var point     = $( this ).attr( 'data-point-' + i ),
                            matches   = point.match( /([\d.+-]+)(?:([,\s]+))([\d.+-]+)/ ),
                            is_coords = matches ? true : false,
                            image     = null;

                        if ( $( this ).attr( 'data-marker-' + i ) ) {
                            image = $( this ).attr( 'data-marker-' + i );
                        } else if ( $( this ).attr( 'data-marker' ) ) {
                            image = $( this ).attr( 'data-marker' );
                        }

                        if ( is_coords ) {
                            points.push( {
                                position: [ matches[1], matches[3] ],
                                icon:     image,
                            } );
                        } else {
                            points.push( {
                                address: point,
                                icon:    image,
                            } );
                        }
                    }
                }

                if ( typeof points[0].address === 'undefined' ) {
                    $( this ).gmap3( {
                        center:            points[0].position,
                        zoom:              zoom,
                        scrollwheel:       false,
                        MapTypeId:         maptype,
                        styles:            map_style( scheme ),
                        streetViewControl: false,
                    } ).marker( points );
                } else {
                    $( this ).gmap3( {
                        address:           points[0].address,
                        zoom:              zoom,
                        scrollwheel:       false,
                        MapTypeId:         maptype,
                        styles:            map_style( scheme ),
                        streetViewControl: false,
                    } ).marker( points );
                }
            } );
        },

        // Progress bars
        progress: function( ) {
            var that  = this,
                items = $( '.progress' ).not( '.progress-completed' );

            items.each( function( ) {
                if ( $( this ).hasClass( 'progress-vertical' ) ) {
                    $( this ).find( '.progress-fill' ).css( { height: '0%' } );
                } else {
                    $( this ).find( '.progress-fill' ).css( { width: '0%' } );
                }

                $( this ).find( '.progress-value' ).text( '0' + $( this ).find( '.progress-fill' ).attr( 'data-unit' ) );
            } );

            that.onscroll( items, function( item ) {
                return ( item.find( '.progress-fill' ).attr( 'data-current' ) < 1 );
            }, function( item ) {
                var value       = 0,
                    number      = 0,
                    bar         = item.find( '.progress-fill' ),
                    is_vertical = item.hasClass( 'progress-vertical' );

                $( { value: 0 } ).animate( { value: parseInt( bar.attr( 'data-progress' ) ) }, { duration: that.options.progress_speed, queue: false, step: function( ) {
                    number = Math.ceil( this.value );

                    if ( is_vertical ) {
                        bar.css( { height: this.value + '%' } ).attr( 'data-current', number );
                    } else {
                        bar.css( { width: this.value + '%' } ).attr( 'data-current', number );
                    }

                    item.find( '.progress-value' ).text( number + bar.attr( 'data-unit' ) );
                }, complete: function( ) {
                    item.addClass( 'progress-completed' );
                } } );
            } );
        },

        // Counter
        counter: function( ) {
            var that  = this,
                items = $( '.counter' ).not( '.counter-completed' );

            items.each( function( ) {
                var item = $( this ),
                    node = item.find( '.counter-skill' );

                node.text( item.attr( 'data-before' ) + '0' + item.attr( 'data-unit' ) + item.attr( 'data-after' ) );
            } );

            that.onscroll( items, function( item ) {
                return ( item.attr( 'data-current' ) < 1 );
            }, function( item ) {
                var value  = 0,
                    number = 0,
                    before = item.attr( 'data-before' ) || '',
                    after = item.attr( 'data-after' ) || '';

                $( { value: 0 } ).animate( { value: parseInt( item.attr( 'data-progress' ) ) }, { duration: that.options.counter_speed, queue: false, step: function( ) {
                    number = Math.ceil( this.value );
                    number = ( number > 999 ) ? that.format_number( number ) : number;

                    item.attr( 'data-current', number );
                    item.find( '.counter-skill' ).text( item.attr( 'data-before' ) + number + item.attr( 'data-unit' ) + item.attr( 'data-after' ) );
                }, complete: function( ) {
                    item.addClass( 'counter-completed' );
                } } );
            } );
        },

        // Masonry grids
        masonry: function( ) {
            var that = this;

            // Get column width
            function get_width( node ) {
                var width = $( window ).width( ),
                    size  = node.attr( 'class' ).match( /\d+/g );

                size = parseInt( size[0] );

                if ( size < 1 ) {
                    size = 1;
                } else if ( size > 12 ) {
                    size = 12;
                }

                if ( width <= 767 ) {
                    size = 1;
                } else if ( width >= 768 && width <= 991 ) {
                    size = 2;
                }

                width = node.outerWidth( true ) - 30;

                return Math.floor( width / size );
            }

            // Initialization
            $( '.masonry' ).each( function( ) {
                if ( that.is_init( this ) ) {
                    return true;
                }

                $( this ).find( '.masonry-item' ).css( {
                    width: get_width( $( this ) ),
                } );

                $( this ).isotope( {
                    itemSelector:  '.masonry-item',
                    masonry: {
                        columnWidth: get_width( $( this ) ),
                    },
                } );
            } );
        },

        // Carousels
        carousel: function( ) {
            var that = this;

            // Trigger: After move
            var after_move = function( carousel ) {
                var owl          = carousel.data( 'owlCarousel' ),
                    mode         = carousel.attr( 'data-navigation-mode' ),
                    container    = carousel.closest( carousel.attr( 'data-container' ) ),
                    navigation   = container.find( carousel.attr( 'data-navigation' ) + ' li' ),
                    active_class = carousel.attr( 'data-container' ).split( '.' ).pop( ) + '-active';

                if ( mode == 'arrows' ) {
                    var prev = navigation.eq( 0 ),
                        next = navigation.eq( 1 );

                    if ( owl.currentItem == owl.owl.owlItems.length - 1 ) {
                        prev.addClass( 'in' ).animate( { opacity: 1 }, { duration: 200, queue: false } );
                        next.animate( { opacity: 0 }, { duration: 250, queue: false, complete: function( ) {
                            $( this ).removeClass( 'in' );
                        } } );
                    } else if ( owl.currentItem === 0 ) {
                        next.addClass( 'in' ).animate( { opacity: 1 }, { duration: 200, queue: false } );
                        prev.animate( { opacity: 0 }, { duration: 250, queue: false, complete: function( ) {
                            $( this ).removeClass( 'in' );
                        } } );
                    } else {
                        if ( ! next.hasClass( 'in' ) ) {
                            next.addClass( 'in' ).animate( { opacity: 1 }, { duration: 200, queue: false } );
                        }

                        if ( ! prev.hasClass( 'in' ) ) {
                            prev.addClass( 'in' ).animate( { opacity: 1 }, { duration: 200, queue: false } );
                        }
                    }
                } else {
                    container.find( '.' + active_class ).removeClass( active_class );
                    navigation.eq( owl.currentItem ).addClass( active_class );
                }
            };

            // Each...
            $( '.carousel' ).each( function( ) {
                if ( that.is_init( this ) ) {
                    return true;
                }

                var with_autoplay    = $( this ).attr( 'data-autoplay' )         ? true : false,
                    // with_autoheight  = $( this ).attr( 'data-autoheight' )      ? true : false,
                    navigation_mode  = $( this ).attr( 'data-navigation-mode' ) || false,
                    navigation       = $( this ).attr( 'data-navigation' )      || '',
                    autoplay         = $( this ).attr( 'data-autoplay' )        || 5,
                    items            = $( this ).attr( 'data-items' )           || 1;

                items         = parseInt( items );
                with_autoplay = ( with_autoplay && autoplay > 1 ) ? true : false;

                if ( navigation_mode == 'arrows' ) {
                    $( this ).parent( ).find( navigation + ' li' ).eq( 1 ).addClass( 'in' );
                }

                $( this ).owlCarousel( {
                    items:      items,
                    singleItem: items == 1,
                    autoPlay:   with_autoplay ? parseInt( autoplay ) * 1000 : false,
                    autoHeight: true,
                    mouseDrag:  true,
                    pagination: false,
                    afterMove:  navigation_mode !== false ? after_move : false,
                } );

                if ( navigation_mode !== false ) {
                    $( this ).parent( ).find( navigation ).find( 'a' ).on( 'click', function( e ) {
                        e.preventDefault( );

                        var carousel = $( this ).closest( navigation ).parent( ).find( '.carousel' ),
                            owl      = carousel.data( 'owlCarousel' ),
                            mode     = carousel.attr( 'data-navigation-mode' ),
                            index    = $( this ).parent( ).index( );

                        if ( mode == 'arrows' ) {
                            if ( index === 0 ) {
                                owl.prev( );
                            } else {
                                owl.next( );
                            }
                        } else {
                            owl.goTo( index );
                        }
                    } );
                }
            } );
        },
    };

    // Initialization
    Rocks_Tools.construct( );
} );
