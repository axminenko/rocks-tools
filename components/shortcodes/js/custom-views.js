/*!
 * Create.Rocks Tools | Version 0.1.0 | http://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */

( function( $ ) {
    'use strict';

    // Button
    window.Rocks_Tools_VC_Button = vc.shortcode_view.extend( {
        events: function( ) {
            return _.extend( { 'click button': 'buttonClick' }, window.Rocks_Tools_VC_Button.__super__.events );
        },

        buttonClick: function ( e ) {
            e.preventDefault( );
        },

        changeShortcodeParams: function( model ) {
            var params = model.get( 'params' );

            window.Rocks_Tools_VC_Button.__super__.changeShortcodeParams.call( this, model );

            if ( _.isObject( params ) ) {
                var that = this.$el.find( 'button' );

                that.css( { backgroundColor: ( params.color ? params.color : '' ) } );
                that.removeClass( 'button-default button-large' ).addClass( 'button-' + params.size );

                that.find( 'i' ).remove( );

                if ( params.icon_type == 'fontawesome' && params.icon ) {
                    $( '<i />' ).addClass( params.icon ).prependTo( that );
                }
            }
        }
    } );

    // Alert
    window.Rocks_Tools_VC_Alert = vc.shortcode_view.extend( {
        changeShortcodeParams: function( model ) {
            var params = model.get( 'params' );

            window.Rocks_Tools_VC_Alert.__super__.changeShortcodeParams.call( this, model );

            if ( _.isObject( params ) ) {
                var that = this.$el.find( '.rocks-vcs-alert' );

                that.removeClass( 'alert-notice alert-warning alert-success alert-error alert-info' );

                if ( typeof params.type === 'undefined' || params.type == '_notice' ) {
                    that.addClass( 'alert-notice' );
                } else {
                    that.addClass( 'alert-' + params.type );
                }
            }
        }
    } );

    // Single Image
    window.Rocks_Tools_VC_Image = vc.shortcode_view.extend( {
        changeShortcodeParams: function( model ) {
            var params = model.get( 'params' );

            window.Rocks_Tools_VC_Image.__super__.changeShortcodeParams.call( this, model );

            if ( _.isObject( params ) ) {
                var that     = this.$el,
                    image_id = parseInt( params.id );

                if ( image_id > 0 ) {
                    $.get( ajaxurl, { action: 'rocks_tools_shortcodes_helpers', name: 'get_image', image: image_id }, function( src ) {
                        that.find( '.vc_element-icon' ).remove( );
                        that.find( '.rocks-vcs-image' ).remove( );

                        $( '<img />' ).attr( 'src', $.trim( src ) ).addClass( 'vc_element-icon rocks-vcs-image' ).appendTo( that.find( '.wpb_element_title' ) );
                    } );
                } else {
                    that.find( '.vc_element-icon' ).remove( );
                    that.find( '.rocks-vcs-image' ).remove( );

                    $( '<i />' ).addClass( 'vc_element-icon icon-wpb-single-image' ).appendTo( that.find( '.wpb_element_title' ) );
                }
            }
        }
    } );

    // Slider
    window.Rocks_Tools_VC_Slider = vc.shortcode_view.extend( {
        changeShortcodeParams: function( model ) {
            var params = model.get( 'params' );

            window.Rocks_Tools_VC_Slider.__super__.changeShortcodeParams.call( this, model );

            if ( _.isObject( params ) ) {
                var that      = this.$el,
                    images_id = params.images,
                    holder;

                if ( images_id && images_id.length > 0 ) {
                    $.get( ajaxurl, { action: 'rocks_tools_shortcodes_helpers', name: 'get_images', images: images_id }, function( data ) {
                        that.find( '.rocks-vcs-slider' ).remove( );

                        data   = $.parseJSON( $.trim( data ) );
                        holder = $( '<div />' ).addClass( 'rocks-vcs-slider' );

                        $.each( data, function( i, src ) {
                            holder.appendTo( that.find( '.wpb_element_wrapper' ) );
                            $( '<img />' ).attr( 'src', src ).appendTo( holder );
                        } );
                    } );
                } else {
                    that.find( '.rocks-vcs-slider' ).remove( );
                }
            }
        }
    } );
} ) ( window.jQuery );
