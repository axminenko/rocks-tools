<?php global $image; ?>

<img src="<?php esc_attr_e( $image[0] ); ?>" width="<?php esc_attr_e( $image[1] ); ?>" height="<?php esc_attr_e( $image[2] ); ?>" alt="">