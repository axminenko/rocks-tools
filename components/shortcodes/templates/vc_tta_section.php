<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

global $rocks_tools_vcs_section, $rocks_tools_vcs_counter;

$atts = vc_map_get_attributes( $this->getShortcode( ), $atts );
$this->resetVariables( $atts, $content );
$this::$self_count ++;
$this::$section_info[] = $atts;

$section = $rocks_tools_vcs_section;
$rocks_tools_vcs_counter ++;

$output  = '<div class="' . $section . '-section' . ( ( $section == 'accordion' and $rocks_tools_vcs_counter == 1 ) ? ' accordion-active' : '' ) . '">';

if ( $section == 'accordion' ) {
	$output .= '<div class="' . $section . '-heading">';
	$output .= $this->getTemplateVariable( 'heading' );
	$output .= '</div>';

	$output .= '<div class="' . $section . '-content">';
	$output .= '<div class="' . $section . '-holder">';
	$output .= $this->getTemplateVariable( 'content' );
	$output .= '</div>';
	$output .= '</div>';

	$find = array(
		'#(\s?)data-vc-accordion data-vc-container=".vc_tta-container"#',
		'#<h4 class="vc_tta-panel-title">#',
		'#</h4>#',
	);

	$output = preg_replace( $find, '', $output );
} else {
	$output .= $this->getTemplateVariable( 'content' );
}

$output .= '</div>';

echo $output;