<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

global $rocks_tools_vcs_section, $rocks_tools_vcs_counter;

$attributes = $atts;
$atts = vc_map_get_attributes( $this->getShortcode( ), $atts );

$this->resetVariables( $atts, $content );
$this->setGlobalTtaInfo( );

$section = preg_replace( '/vc_tta_/', '', $this->shortcode );
$rocks_tools_vcs_section = $section;

wp_enqueue_script( 'rocks-tools-shortcodes' );

if ( $section == 'pageable' ) {
    wp_enqueue_script( 'owl-carousel' );

    if ( ! array_key_exists( 'navigation', $attributes ) ) {
        $attributes['navigation'] = 'slides';
    }
}

$rocks_tools_vcs_counter = 0;
$prepareContent = $this->getTemplateVariable( 'content' );
$rocks_tools_vcs_counter = 0;

$class  = empty( $attributes['class'] ) ? '' : ' ' . $attributes['class'];

if ( $section == 'pageable' and $attributes['navigation'] == 'arrows' ) {
    $class .= ' with-arrows';
}

$output = '<div class="' . esc_attr( $section ) . esc_attr( $class ) . '">';

$output .= $this->getTemplateVariable( 'tabs-list-top' );
$output .= $this->getTemplateVariable( 'tabs-list-left' );

if ( $section != 'accordion' ) {
    $pagination = '';

    if ( $section == 'pageable' ) {
        $pagination = ' data-container=".pageable" data-navigation-mode="' . ( $attributes['navigation'] == 'arrows' ? 'arrows' : 'slides' ) . '" data-navigation=".pageable-heading"';
    }

    $output .= '<div class="' . esc_attr( $section ) . '-content' . ( $section == 'pageable' ? ' carousel' : '' ) . '"' . ( $section != 'pageable' ? '' : ( ! empty( $attributes['autoplay'] ) ? ' data-autoplay="' . esc_attr( $attributes['autoplay'] ) . '"' : '' ) . ' data-items="1"' . $pagination ) . '>';
}

$output .= $prepareContent;

if ( $section != 'accordion' ) {
    $output .= '</div>';
}

if ( $section == 'pageable' ) {
    $output .= '<div class="' . esc_attr( $section ) . '-heading">';

    $output .= $this->getTemplateVariable( 'pagination-top' );
    $output .= $this->getTemplateVariable( 'pagination-bottom' );

    $output .= '</div>';
}

$output .= '</div>';

$find = array(
    '#vc_tta-tabs-container#',
    '#vc_active#',
    '#<span class="vc_tta-title-text">([^<]+)</span>#',
    '#="\\#([^"]+)"#',
    '#<ul class="vc_([^"]+)"#',
    '#(\s?)class="vc_tta-tabs-list"#',
    '#vc_tta-tab(\s?)#',
    '#(\s?)class=""#',
    '#(\s?)data-vc-tabs data-vc-container=".vc_tta"#',
    '#(\s?)data-vc-tab#',
    '#vc_pagination-item(\s?)#',
    '#vc_pagination-trigger#',
    '#(\s?)class="([<"]+)#',
);

$replace = array(
    esc_attr( $section ) . '-heading',
    esc_attr( $section ) . '-active',
    '$1',
    '="#"',
    '<ul',
);

echo preg_replace( $find, $replace, $output );
