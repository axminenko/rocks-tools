<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

global $wp_embed;

$embed = $output  = '';
$mode  = $preview = $link = $el_class = $css = $css_animation = '';
$atts  = vc_map_get_attributes( $this->getShortcode( ), $atts );

extract( $atts );

if ( ! is_object( $wp_embed ) or empty( $link ) ) {
    return '';
}

$el_class   = $this->getExtraClass( $el_class ) . $this->getCSSAnimation( $css_animation );
$el_classes = array(
    $el_class,
    vc_shortcode_custom_css_class( $css, ' ' ),
    ( $mode == 'lightbox' ? 'with-lightbox' : 'direct-insert' ),
);

$css_class = implode( ' ', $el_classes );
$css_class = trim( apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $css_class, $this->getShortcode( ), $atts ) );

$output .= '<div class="embed-video' . ( empty( $css_class ) ? '' : ' ' . esc_attr( $css_class ) ) . '">';

if ( $mode == 'lightbox' ) {
    wp_enqueue_script( 'lity' );

    $image   = wp_get_attachment_image_src( absint( $preview ), 'large' );
    $preview = is_array( $image ) ? $image[0] : '';

    $output .= '
    <div class="embed-video-preview"' . ( empty( $preview ) ? '' : ' style="background-image: url(\'' . esc_attr( $preview ) . '\');"' ) . '>
        <a href="' . esc_url( $link ) . '" data-lity><i class="fa fa-play"></i></a>
    </div>';
} else {
    $output .= $wp_embed->run_shortcode( '[embed]' . $link . '[/embed]' );
}

$output .= '</div>';

echo $output;
