<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * WordPress shortcodes list
 *
 * @package    Create_Rocks_Tools
 * @subpackage Shortcodes_Twitter
 */
class Rocks_Tools_Shortcodes_Twitter {
    /**
     * Stored key in database
     *
     * @var string
     * @access private
     */
    private $stored = 'crt_twitter';

    /**
     * Twitter user ID
     *
     * @var string
     * @access private
     */
    private $user_id;

    /**
     * Extracted fields
     *
     * @var array
     * @access private
     */
    private $details = array(
        'text',
        'in_reply_to_user_id',
    );

    /**
     * Twitter API
     *
     * @var string
     * @access private
     */
    private $api = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

    /**
     * Token key
     *
     * @var string
     * @access private
     */
    private $token_key = '1236240270-ZvbPZs8bK214dMovpvaQon2TyokxVMjjXgiTcxY';

    /**
     * Token secret
     *
     * @var string
     * @access private
     */
    private $token_secret = 'abk155BDNOeHFWJB9efRjrCel2o14oL0zBhRu2SZfPLAO';

    /**
     * Customer key
     *
     * @var string
     * @access private
     */
    private $customer_key = 'jYFa49Ttklo6uS1rUb2T4P3Gn';

    /**
     * Customer secret
     *
     * @var string
     * @access private
     */
    private $customer_secret = '0cHhdU0YCK90MIenjUhLKddwq6VNsqIE348HYgaAsXSOlLrCDZ';

    /**
     * Cache state
     *
     * @var boolean
     * @access private
     */
    private $is_cache = false;

    /**
     * Constructor
     *
     * @param string $user_id
     * @access public
     */
    function __construct( $user_id, $limit = 10 ) {
        $this->user_id = $user_id;
        $this->limit   = $limit < 1 ? 10 : absint( $limit );
        $this->stored  = $this->stored . '_' . sanitize_key( $user_id ) . '_' . $this->limit;
    }

    /**
     * Get feed
     *
     * @param int $limit
     * @return array|boolean
     * @access public
     */
    public function get_feed( ) {
        $tweets = get_transient( $this->stored );

        if ( ! $tweets ) {
            if ( $tweets = $this->__get_feed( $this->limit ) ) {
                set_transient( $this->stored, $tweets, 60 * 10 );
            }
        } else {
            $this->is_cache = true;
        }

        if ( $tweets ) {
            return array_map( array( &$this, 'prepare_text' ), $tweets );
        }

        return false;
    }

    /**
     * Get and parse feed
     *
     * @param int $limit
     * @return array|boolean
     * @access public
     */
    private function __get_feed( $limit ) {
        $query = array(
            'screen_name' => $this->user_id,
        );

        $oauth = array(
            'oauth_consumer_key'     => $this->customer_key,
            'oauth_token'            => $this->token_key,
            'oauth_nonce'            => ( string ) mt_rand( ),
            'oauth_timestamp'        => time( ),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_version'          => '1.0',
        );

        $oauth = array_map( 'rawurlencode', $oauth );
        $query = array_map( 'rawurlencode', $query );
        $query = array_merge( $oauth, $query );

        ksort( $query );

        $api = $this->api;

        $query_string = urldecode( http_build_query( $query, '', '&' ) );
        $base_string  = 'GET&' . rawurlencode( $api ) . '&' . rawurlencode( $query_string );
        $unique_key   = rawurlencode( $this->customer_secret ) . '&' . rawurlencode( $this->token_secret );
        $signature    = rawurlencode( base64_encode( hash_hmac( 'sha1', $base_string, $unique_key, true ) ) );
        $api         .= '?' . http_build_query( $query );

        $oauth['oauth_signature'] = $signature;

        ksort( $oauth );

        $oauth = array_map( array( &$this, 'add_quotes' ), $oauth );
        $oauth = 'OAuth ' . urldecode( http_build_query( $oauth, '', ', ' ) );

        $query = wp_remote_get( $api, array( 'headers' => array(
            'Authorization'   => $oauth,
            'Accept-Encoding' => '',
        ) ) );

        if ( ! is_wp_error( $query ) and is_array( $query ) and array_key_exists( 'body', $query ) ) {
            $tweets = json_decode( $query['body'] );
            $parsed = array( );

            if ( count( $tweets ) < 1 ) {
                return false;
            }

            for ( $i = 0; $i < $limit; $i ++ ) {
                $parsed[] = $this->extract_details( $tweets[$i] );
            }

            return $parsed;
        }

        return false;
    }

    /**
     * Add quotes (before and after)
     *
     * @param string $string
     * @return string
     * @access private
     */
    private function add_quotes( $string ) {
        return '"' . $string . '"';
    }

    /**
     * Prepare text
     *
     * @param string $tweet
     * @return string
     * @access private
     */
    private function prepare_text( $tweet ) {
        // Concatenate lines
        $tweet['text'] = preg_replace( array( "/\\r/", "/\\n/" ), array( '', ' ' ), $tweet['text'] );

        // Twitter users
        $tweet['text'] = preg_replace( '/@(\w+)/', "<a href=\"https://twitter.com/$1\">@$1</a>", $tweet['text'] );

        // Links in tweets
        $tweet['text'] = preg_replace( '/https:\/\/t.co\/([0-9a-z]+)/i', '', $tweet['text'] );
        $tweet['text'] = $this->__popuplinks( make_clickable( $tweet['text'] ) );

        return $tweet;
    }

    /**
     * Extract tweet details from object
     *
     * @param object $tweet
     * @return array
     * @access private
     */
    private function extract_details( $tweet ) {
        $details = array( );

        if ( count( $this->details ) < 1 ) {
            return $details;
        }

        foreach ( $this->details as $name ) {
            if ( property_exists( $tweet, $name ) and ! empty( $tweet->$name ) ) {
                $details[$name] = $tweet->$name;
            }
        }

        return $details;
    }

    /**
     * Return cache state
     *
     * @return boolean
     * @access public
     */
    public function is_cache( ) {
        return $this->is_cache;
    }

    /**
     * Set token data
     *
     * @param string $key token key
     * @param string $secret token secret
     * @access public
     */
    public function set_token( $key, $secret ) {
        $this->token_key    = $key;
        $this->token_secret = $secret;
    }

    /**
     * Set customer data
     *
     * @param string $key customer key
     * @param string $secret customer secret
     * @access public
     */
    public function set_customer( $key, $secret ) {
        $this->customer_key    = $key;
        $this->customer_secret = $secret;
    }

    /**
     * Load token and custom data from WP options
     *
     * @param string $prefix
     * @return boolean
     * @access public
     */
    public function set_details( $prefix = '' ) {
        $token_key    = get_option( $prefix . 'token_key', '' );
        $token_secret = get_option( $prefix . 'token_secret', '' );

        $customer_key    = get_option( $prefix . 'customer_key', '' );
        $customer_secret = get_option( $prefix . 'customer_secret', '' );

        if ( ! empty( $token_key ) and ! empty( $token_secret ) and ! empty( $customer_key ) and ! empty( $customer_secret ) ) {
            $this->set_token( $token_key, $token_secret );
            $this->set_customer( $customer_key, $customer_secret );

            return true;
        }

        return false;
    }

    private function __popuplinks( $text ) {
        return preg_replace( '/<a (.+?)>/i', '<a ' . "$1" . ' target="_blank">', $text );
    }
}
