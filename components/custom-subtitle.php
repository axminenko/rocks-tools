<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Custom headers
 *
 * @package    Create_Rocks_Tools
 * @subpackage Custom_Subtitle
 */
class Rocks_Tools_Custom_Subtitle {
	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct( ) {
		add_action( 'add_meta_boxes', array( &$this, 'metabox_init' ) );
		add_action( 'save_post',      array( &$this, 'metabox_save' ) );
	}

	/**
	 * Initialization
	 *
	 * @return Create_Rocks_Tools
	 * @access public
	 * @static
	 */
	public static function init( ) {
		return new self( );
	}

	/**
	 * Initialization metabox
	 *
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function metabox_init( ) {
		global $rocks_tools;

		// Pages
		if ( current_user_can( 'edit_pages' ) ) {
			add_meta_box( 'rocks_tools_custom_subtitle', __( 'Custom Subtitle', 'rocks' ), array( &$this, 'metabox_content' ), 'page', 'side' );
		}

		// Posts
		if ( current_user_can( 'edit_posts' ) ) {
			add_meta_box( 'rocks_tools_custom_subtitle', __( 'Custom Subtitle', 'rocks' ), array( &$this, 'metabox_content' ), 'post', 'side' );
		}
	}

	/**
	 * Metabox content
	 *
	 * @param  WP_Post $post
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function metabox_content( $post ) {
		global $rocks_tools;

		wp_nonce_field( 'rocks_tools_custom_subtitle_nonce', 'rocks_tools_custom_subtitle_nonce_safe' );

		echo '
		<p><input type="text" name="rocks_tools_custom_subtitle" value="' . esc_attr( get_post_meta( $post->ID, 'rocks_tools_custom_subtitle', true ) ) . '" placeholder="' . esc_attr__( 'Subtitle', 'rocks' ) . '" style="width: 100%;"></p>
		<p class="howto">' . esc_attr__( 'Add a subtitle for the post or page.', 'rocks' ) . '</p>';
	}

	/**
	 * Update metabox details
	 *
	 * @param  int $post_id
	 * @access public
	 */
	public function metabox_save( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['rocks_tools_custom_subtitle_nonce_safe'] ) or ! wp_verify_nonce( $_POST['rocks_tools_custom_subtitle_nonce_safe'], 'rocks_tools_custom_subtitle_nonce' ) ) {
			return;
		}

		if ( ! empty( $_POST['post_type'] ) and $_POST['post_type'] == 'page' ) {
			if ( ! current_user_can( 'edit_pages' ) ) {
				return;
			}
		} else {
			if ( ! current_user_can( 'edit_posts' ) ) {
				return;
			}
		}

		if ( isset( $_POST['rocks_tools_custom_subtitle'] ) ) {
			update_post_meta( $post_id, 'rocks_tools_custom_subtitle', sanitize_text_field( $_POST['rocks_tools_custom_subtitle'] ) );
		}
	}
}

// Initialization
add_action( 'rocks_tools_loaded', array( 'Rocks_Tools_Custom_Subtitle', 'init' ) );
