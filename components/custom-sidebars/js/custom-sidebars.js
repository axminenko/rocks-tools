/*!
 * Create.Rocks Tools | Version 0.1.0 | http://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */

// Custom Sidebars
var Rocks_Tools_Custom_Sidebars = ( function( ) {
	"use strict";

	// Contructor
	function Rocks_Tools_Custom_Sidebars( ) {
		this.widget_wrap      = jQuery( '.widget-liquid-right' );
		this.widget_area      = jQuery( '#widgets-right' );
		this.widget_add       = jQuery( '#rocks-tools-widget-area-template' );
		this.sidebar_position = jQuery( '.rocks_tools_custom_sidebar_position' );

		this.create_form( );
		this.add_elements( );
		this.events( );
	}

	// Creating form
	Rocks_Tools_Custom_Sidebars.prototype.create_form = function( ) {
		this.widget_wrap.find( '.sidebars-column-1' ).eq( 0 ).append( this.widget_add.html( ) );

		this.widget_name = this.widget_wrap.find( 'input[name="rocks-tools-add-widget"]' );
		this.nonce       = this.widget_wrap.find( 'input[name="rocks-tools-nonce"]' ).val( );
	};

	// Add the delete button
	Rocks_Tools_Custom_Sidebars.prototype.add_elements = function( ) {
		this.widget_area.find( '.sidebar-rocks-custom' ).append( '<span class="rocks-tools-sidebar-area-delete">&#10006;</span>' );
	};

	// Delete widget area action
	Rocks_Tools_Custom_Sidebars.prototype.delete_sidebar = function( e ) {
		var obj, spinner, title, widget, widget_name;

		widget      = jQuery( e.currentTarget ).parents( '.widgets-holder-wrap' );
		title       = ( widget.find( '.sidebar-name h2' ).length > 0 ) ? widget.find( '.sidebar-name h2' ) : widget.find( '.sidebar-name h3' );
		spinner     = title.find( '.spinner' );
		widget_name = jQuery.trim( title.text( ) );
		obj         = this;
	
		if ( confirm( rocks_tools_custom_sidebars.delete_sidebar ) ) {
			jQuery.ajax( {
				type: 'POST',
				url:  window.ajaxurl,
				data: {
					action: 'rocks_tools_delete_custom_sidebar',
					name: widget_name,
					_wpnonce: obj.nonce
				},
				beforeSend: function( ) {
					spinner.addClass( 'activate' );
				},
				success: function( response ) {
					if ( jQuery.trim( response ) == 'sidebar-deleted' ) {
						widget.fadeOut( 100, function( ) {
							jQuery( '.widget-control-remove', widget ).trigger( 'click' );

							widget.remove( );
							wpWidgets.saveOrder( );
						} );
					}
				}
			} );
		}
	};

	// Change sidebar position
	Rocks_Tools_Custom_Sidebars.prototype.set_sidebar_position = function( e ) {
		var current, old, image;

		current = jQuery( e.currentTarget );
		old = current.parent( ).find( '.checked' );

		// Latest position
		image = old.find( 'img' ).attr( 'src' ).replace( '_selected.png', '.png' );

		old.removeClass( 'checked' );
		old.find( 'input' ).removeAttr( 'checked' );
		old.find( 'img' ).attr( 'src', image );

		// New position
		image = current.find( 'img' ).attr( 'src' ).replace( '.png', '_selected.png' );

		current.addClass( 'checked' );
		current.find( 'img' ).attr( 'src', image );
		current.find( 'input' ).attr( 'checked', true );
	};

	// Needed events
	Rocks_Tools_Custom_Sidebars.prototype.events = function( ) {
		this.widget_wrap.on( 'click', '.rocks-tools-sidebar-area-delete', jQuery.proxy( this.delete_sidebar, this ) );
		this.sidebar_position.on( 'click', 'span', jQuery.proxy( this.set_sidebar_position, this ) );
	};

	// Return instance
	return Rocks_Tools_Custom_Sidebars;
} ) ( );

// Initialization
jQuery( function( ) {
	return new Rocks_Tools_Custom_Sidebars( );
} );