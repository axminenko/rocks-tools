<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Custom sidebars
 *
 * @package    Create_Rocks_Tools
 * @subpackage Custom_Sidebars
 */
class Rocks_Tools_Custom_Sidebars {
	/**
	 * Stored key in database
	 * 
	 * @var    string
	 * @access protected
	 * @static
	 */
	protected static $__stored = 'crt_custom_sidebars';

	/**
	 * Sidebars array
	 * 
	 * @var    array
	 * @access public
	 */
	public $sidebars = array( );

	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		add_action( 'widgets_init', array( &$this, 'register_custom_sidebars' ), 1000 );
		add_action( 'admin_footer', array( &$this, 'template_custom_widget_area' ), 200 );
		add_action( 'customize_controls_print_scripts', array( &$this, 'customize_controls_print_scripts' ) );

		add_action( 'load-widgets.php', array( &$this, 'load_scripts_styles' ), 5 );
		add_action( 'load-widgets.php', array( &$this, 'add_sidebar_area' ), 100 );

		add_action( 'wp_ajax_rocks_tools_delete_custom_sidebar', array( &$this, 'delete_sidebar_area' ), 1000 );

		add_action( 'add_meta_boxes', array( &$this, 'metabox_init' ) );
		add_action( 'save_post', array( &$this, 'metabox_save' ) );
	}

	/**
	 * Initialization
	 * 
	 * @return Rocks_Tools_Custom_Sidebars
	 * @access public
	 * @static
	 */
	public static function init( ) {
		return new self( );
	}

	/**
	 * Get stored key
	 *
	 * @return string
	 * @access public
	 * @static
	 */
	public static function get_stored_key( ) {
		return self::$__stored;
	}

	/**
	 * Register needed scripts and styles
	 * 
	 * @access public
	 * @static
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public static function load_scripts_styles( ) {
		global $rocks_tools;

		wp_enqueue_style( 'rocks-tools-custom-sidebars', plugins_url( 'css/custom-sidebars.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );
		wp_enqueue_script( 'rocks-tools-custom-sidebars', plugins_url( 'js/custom-sidebars.min.js', __FILE__ ), array( 'jquery' ), $rocks_tools->get_version( ), true );
		
		wp_localize_script( 'rocks-tools-custom-sidebars', 'rocks_tools_custom_sidebars', array(
			'delete_sidebar' => __( 'Are you sure you want to delete this sidebar?', 'rocks' )
		) );
	}

	/**
	 * Template for Custom Widget Area
	 * 
	 * @access public
	 */
	public function template_custom_widget_area( ) {
		echo '
		<script type="text/html" id="rocks-tools-widget-area-template">
			<div class="widgets-holder-wrap rocks-tools-add-widget">
				<form method="post">
					<div class="sidebar-name">
						<h3>' . __( 'Custom Widget Area', 'rocks' ) . '</h3>
					</div>
					<input type="hidden" name="rocks-tools-nonce" value="' . esc_attr( wp_create_nonce( 'rocks-tools-nonce' ) ) . '">
					<input type="text" name="rocks-tools-add-widget" value="" placeholder="' . esc_attr__( 'Name of the new widget area', 'rocks' ) . '" required>
					<p class="submit">
						<input type="submit" name="rocks-tools-custom-sidebar-submit" id="rocks-tools-custom-sidebar-submit" class="button button-primary button-large" value="' . esc_attr__( 'Add Widget Area', 'rocks' ) . '">
					</p>
				</form>
			</div>
		</script>';
	}

	/**
	 * Create new sidebar area
	 * 
	 * @access public
	 */
	public function add_sidebar_area( ) {
		if ( ! empty( $_POST['rocks-tools-add-widget'] ) ) {
			$this->sidebars = get_option( $this->get_stored_key( ) );

			$name = $this->get_name( $_POST['rocks-tools-add-widget'] );
			$this->sidebars[] = sanitize_title_with_dashes( $name );

			update_option( $this->get_stored_key( ), $this->sidebars );
			wp_redirect( admin_url( 'widgets.php' ) );

			die( );
		}
	}

	/**
	 * Delete the sidebar area
	 * 
	 * @access public
	 */
	public function delete_sidebar_area( ) {
		check_ajax_referer( 'rocks-tools-nonce' );

		if ( ! empty( $_POST['name'] ) ) {
			$name = sanitize_title_with_dashes( stripslashes( $_POST['name'] ) );
			$this->sidebars = get_option( $this->get_stored_key( ) );

			if ( in_array( $name, $this->sidebars ) ) {
				if ( ( $key = array_search( $name, $this->sidebars ) ) !== false ) {
					unset( $this->sidebars[$key] );
				}

				update_option( $this->get_stored_key( ), $this->sidebars );
				unregister_sidebar( $name );

				echo 'sidebar-deleted';
			}
		}

		die( );
	}

	/**
	 * Check submited details of sidebar
	 * 
	 * @param  string $name
	 * @return string
	 * @access public
	 */
	public function get_name( $name ) {
		if ( empty( $GLOBALS['wp_registered_sidebars'] ) ) {
			return $name;
		}

		$name = trim( $name );

		if ( empty( $name ) ) {
			$name = __( 'Widget Area', 'rocks' );
		}

		$taken = array( );

		foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
			$taken[] = $sidebar['name'];
		}

		if ( empty( $this->sidebars ) ) {
			$this->sidebars = array( );
		}

		$taken = array_merge( $taken, $this->sidebars );

		if ( in_array( $name, $taken ) ) {
			$counter  = substr( $name, -1 );
			$new_name = '';

			if ( ! is_numeric( $counter ) ) {
				$new_name = $name . ' 1';
			} else {
				$new_name = substr( $name, 0, -1 ) . ( ( int ) $counter + 1 );
			}

			$name = $this->get_name( $new_name );
		}

		return $name;
	}

	/**
	 * Register all custom sidebars
	 * 
	 * @access public
	 */
	public function register_custom_sidebars( ) {
		$sidebars = get_option( $this->get_stored_key( ) );

		$args = apply_filters( 'rocks_tools_custom_sidebars_widget_args',
			array(
				'description'   => __( 'Add widgets here to appear in your sidebar.', 'rocks' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		if ( is_array( $sidebars ) ) {
			foreach ( $sidebars as $id => $sidebar ) {
				$args['name']  = $this->get_formatted_name( $sidebar );
				$args['id']    = sanitize_title_with_dashes( $sidebar );
				$args['class'] = 'rocks-custom';

				register_sidebar( $args );
			}
		}
	}

	/**
	 * Output CSS styles for WordPress customizer
	 * 
	 * @access public
	 */
	public function customize_controls_print_scripts( ) {
		if ( false === ( $sidebars = get_option( $this->get_stored_key( ) ) ) ) {
			return;
		}

		if ( ! is_array( $sidebars ) ) {
			return;
		}

		echo '<style type="text/css">' . "\n";

		foreach ( $sidebars as $id ) {
			echo '#accordion-section-sidebar-widgets-' . $id . ' { display: list-item !important; height: auto !important; }' . "\n";
			echo '#accordion-section-sidebar-widgets-' . $id . ' .widget-top { opacity: 1 !important; }' . "\n";
		}

		echo '</style>' . "\n";
	}

	/**
	 * Initialization custom sidebar metabox
	 * 
	 * @access public
	 */
	public function metabox_init( ) {
		// Pages
		if ( current_user_can( 'edit_pages' ) ) {
			add_meta_box( 'rocks_tools_custom_sidebar', __( 'Custom Sidebar', 'rocks' ), array( &$this, 'metabox_content' ), 'page', 'side' );
		}

		// Posts
		if ( current_user_can( 'edit_posts' ) ) {
			add_meta_box( 'rocks_tools_custom_sidebar', __( 'Custom Sidebar', 'rocks' ), array( &$this, 'metabox_content' ), 'post', 'side' );
		}
	}

	/**
	 * Get formatted name
	 *
	 * @param  string $name
	 * @return string
	 * @access public
	 */
	public function get_formatted_name( $name ) {
		return ucwords( str_replace( array( '-', '_' ), ' ', $name ) );
	}

	/**
	 * Metabox content
	 * 
	 * @param  WP_Post $post
	 * @access public
	 */
	public function metabox_content( $post ) {
		self::load_scripts_styles( );

		$stored   = get_option( $this->get_stored_key( ) );
		$sidebars = array( );

		if ( count( $GLOBALS['wp_registered_sidebars'] ) > 0 ) {
			foreach ( $GLOBALS['wp_registered_sidebars'] as $id => $sidebar ) {
				$sidebars[$sidebar['id']] = $sidebar['name'];
			}
		}

		$list = '';

		$current  = get_post_meta( $post->ID, 'rocks_tools_custom_sidebar_id', true );
		$position = get_post_meta( $post->ID, 'rocks_tools_custom_sidebar_position', true );

		if ( empty( $current ) or ! array_key_exists( $current, $sidebars ) ) {
			$current = 'default';
		}

		if ( empty( $position ) or ! in_array( $position, array( 'left', 'none', 'right' ) ) ) {
			$position = 'none';
		}

		foreach ( $sidebars as $sidebar_id => $sidebar ) {
			$list .= '<option value="' . esc_attr( $sidebar_id ) . '" ' . selected( $sidebar_id, $current, false ) . '>' . esc_html( $this->get_formatted_name( $sidebar ) ) . '</option>';
		}

		wp_nonce_field( 'rocks_tools_custom_sidebars_nonce', 'rocks_tools_custom_sidebars_nonce_safe' );

		echo '
		<p><strong>' . __( 'Sidebar Layout', 'rocks' ) . '</strong></p>
		<p><select name="rocks_tools_custom_sidebar_id">' . $list . '</select></p>
		<p class="howto">' . sprintf( __( 'Select a sidebar or <a href="%s" target="_blank">create new one</a>.', 'rocks' ), 'widgets.php' ) . '</p>
		<p><strong>' . __( 'Sidebar Position', 'rocks' ) . '</strong></p>
		<div class="rocks_tools_custom_sidebar_position">
			<span' . ( $position == 'left' ? ' class="checked"' : '' ) . '>
				<img src="' . plugins_url( 'images/left' . ( $position == 'left' ? '_selected' : '' ) . '.png', __FILE__ ) . '" alt="' . esc_attr__( 'Left', 'rocks' ) . '" title="' . esc_attr__( 'Left', 'rocks' ) . '">
				<input type="radio" name="rocks_tools_custom_sidebar_position" value="left"' . ( $position == 'left' ? ' checked="checked"' : '' ) . '>
				<i>' . __( 'Left', 'rocks' ) . '</i>
			</span>
			<span' . ( $position == 'none' ? ' class="checked"' : '' ) . '>
				<img src="' . plugins_url( 'images/none' . ( $position == 'none' ? '_selected' : '' ) . '.png', __FILE__ ) . '" alt="' . esc_attr__( 'None', 'rocks' ) . '" title="' . esc_attr__( 'None', 'rocks' ) . '">
				<input type="radio" name="rocks_tools_custom_sidebar_position" value="none"' . ( $position == 'none' ? ' checked="checked"' : '' ) . '>
				<i>' . __( 'None', 'rocks' ) . '</i>
			</span>
			<span' . ( $position == 'right' ? ' class="checked"' : '' ) . '>
				<img src="' . plugins_url( 'images/right' . ( $position == 'right' ? '_selected' : '' ) . '.png', __FILE__ ) . '" alt="' . esc_attr__( 'Right', 'rocks' ) . '" title="' . esc_attr__( 'Right', 'rocks' ) . '">
				<input type="radio" name="rocks_tools_custom_sidebar_position" value="right"' . ( $position == 'right' ? ' checked="checked"' : '' ) . '>
				<i>' . __( 'Right', 'rocks' ) . '</i>
			</span>
		</div>';
	}

	/**
	 * Update metabox details
	 * 
	 * @param  int $post_id
	 * @access public
	 */
	public function metabox_save( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['rocks_tools_custom_sidebars_nonce_safe'] ) or ! wp_verify_nonce( $_POST['rocks_tools_custom_sidebars_nonce_safe'], 'rocks_tools_custom_sidebars_nonce' ) ) {
			return;
		}

		if ( ! empty( $_POST['post_type'] ) and $_POST['post_type'] == 'page' ) {
			if ( ! current_user_can( 'edit_pages' ) ) {
				return;
			}
		} else {
			if ( ! current_user_can( 'edit_posts' ) ) {
				return;
			}
		}

		if ( isset( $_POST['rocks_tools_custom_sidebar_id'] ) ) {
			update_post_meta( $post_id, 'rocks_tools_custom_sidebar_id', sanitize_text_field( $_POST['rocks_tools_custom_sidebar_id'] ) );
		}

		if ( isset( $_POST['rocks_tools_custom_sidebar_position'] ) ) {
			if ( ! in_array( $_POST['rocks_tools_custom_sidebar_position'], array( 'left', 'none', 'right' ) ) ) {
				$_POST['rocks_tools_custom_sidebar_position'] = 'none';
			}

			update_post_meta( $post_id, 'rocks_tools_custom_sidebar_position', sanitize_text_field( $_POST['rocks_tools_custom_sidebar_position'] ) );
		}
	}
}

// Initialization
add_action( 'rocks_tools_loaded', array( 'Rocks_Tools_Custom_Sidebars', 'init' ) );