/*!
 * Create.Rocks Tools | Version 0.1.0 | http://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */

jQuery( document ).ready( function( $ ) {
	/**
	 * Hide/unhide block
	 */
	$( '.rocks_tools_custom_headers_option.title' ).on( 'click', function( ) {
		var $block = $( this ).next( );

		if ( $block.hasClass( 'visible' ) ) {
			$( this ).find( 'i.fa' ).removeClass( 'fa-angle-up' ).addClass( 'fa-angle-down' );
			$block.removeClass( 'visible' );
		} else {
			$( this ).find( 'i.fa' ).removeClass( 'fa-angle-down' ).addClass( 'fa-angle-up' );
			$block.addClass( 'visible' );
		}
	} );

	/**
	 * Reset
	 */
	$( '.rocks_tools_custom_headers_reset' ).on( 'click', function( ) {
		var	$block = $( this ).parent( ),
			$title  = $block.prev( );

		$block.find( 'li.checked' ).removeClass( 'checked' );
		$title.find( 'input' ).val( '' );
		$title.find( 'span' ).text( 'Default' );
		$title.click( );
	} );

	/**
	 * Select option
	 */
	$( '.rocks_tools_custom_headers_previews' ).on( 'click', 'img', function( ) {
		var $customize = $( this ).parents( 'ul' ).hasClass( 'customizer' );

		if ( ! $customize ) {
			var $block = $( this ).parents( 'ul' ).parent( ),
				$title = $block.prev( );

			$title.find( 'input' ).val( $( this ).parent( ).attr( 'data-id' ) );
			$title.find( 'span' ).text( 'Custom' );

			$( this ).parents( 'ul' ).find( 'li.checked' ).removeClass( 'checked' );
		} else {
			$( this ).parent( ).parent( ).parent( ).find( 'input' ).val( $( this ).parent( ).attr( 'data-id' ) ).keyup( );
			$( this ).parent( ).parent( ).find( 'li.checked' ).removeClass( 'checked' );
		}

		$( this ).parent( ).addClass( 'checked' );
	} );
} );