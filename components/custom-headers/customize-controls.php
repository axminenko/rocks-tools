<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

if ( ! class_exists( 'WP_Customize_Control' ) ) {
    return null;
}

/**
 * Header layout (Customize API control)
 *
 * @package    Create_Rocks_Tools
 * @subpackage Customize_Header_Control
 */
class Rocks_Tools_Customize_Header_Control extends WP_Customize_Control {
	/**
	 * Control type
	 *
	 * @var    string
	 * @access public
	 */
	public $type = 'header_layout';

	/**
	 * Directory name
	 *
	 * @var    string
	 * @access public
	 */
	public $directory = 'normal';

	/**
	 * Render content
	 *
	 * @access public
	 */
	public function render_content( ) {
		$list     = '';
		$current  = $this->value( );
		$previews = Rocks_Tools_Custom_Headers::get_files_list( $this->directory );

		if ( ! is_array( $previews ) ) {
			return null;
		}

		foreach ( $previews as $name ) {
			$list .= '<li data-id="' . esc_attr( $name ) . '"' . ( $name == $current ? ' class="checked"' : '' ) . '><img src="' . get_template_directory_uri( ) . '/components/custom-headers/' . esc_attr( $this->directory ) . '/' . esc_attr( $name ) . '.png"></li>';
		}

		echo '
		<label>
			<input type="text" class="rocks_tools_custom_headers_input" ' . $this->get_link( ) . '>
			<span class="customize-control-title">' . esc_html( $this->label ) . '</span>
			<ul class="rocks_tools_custom_headers_previews customizer">' . $list . '</ul>
		</label>';
	}

	/**
	 * Enqueue styles and scripts
	 *
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function enqueue( ) {
		global $rocks_tools;

		wp_enqueue_style( 'rocks-tools-custom-headers', plugins_url( 'css/custom-headers.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );
		wp_enqueue_script( 'rocks-tools-custom-headers', plugins_url( 'js/custom-headers.min.js', __FILE__ ), array( 'jquery' ), $rocks_tools->get_version( ), true );
	}
}

/**
 * Mobile header layout (Customize API control)
 *
 * @package    Create_Rocks_Tools
 * @subpackage Customize_Mobile_Header_Control
 */
class Rocks_Tools_Customize_Mobile_Header_Control extends Rocks_Tools_Customize_Header_Control {
	/**
	 * Control type
	 *
	 * @var    string
	 * @access public
	 */
	public $type = 'mobile_header_layout';

	/**
	 * Directory name
	 *
	 * @var    string
	 * @access public
	 */
	public $directory = 'mobile';
}

/**
 * Footer layout (Customize API control)
 *
 * @package    Create_Rocks_Tools
 * @subpackage Customize_Footer_Control
 */
class Rocks_Tools_Customize_Footer_Control extends Rocks_Tools_Customize_Header_Control {
	/**
	 * Control type
	 *
	 * @var    string
	 * @access public
	 */
	public $type = 'footer_layout';

	/**
	 * Directory name
	 *
	 * @var    string
	 * @access public
	 */
	public $directory = 'footers';
}