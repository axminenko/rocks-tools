<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Custom headers
 *
 * @package    Create_Rocks_Tools
 * @subpackage Custom_Headers
 */
class Rocks_Tools_Custom_Headers {
    /**
     * Constructor
     *
     * @access public
     */
    function __construct( ) {
        if ( ! is_dir( get_template_directory( ) . '/components/custom-headers' ) ) {
            return;
        }

        //add_action( 'add_meta_boxes',     array( &$this, 'metabox_init' ) );
        //add_action( 'save_post',          array( &$this, 'metabox_save' ) );
        add_action( 'customize_register', array( &$this, 'customize_register' ) );
    }

    /**
     * Initialization
     *
     * @return Rocks_Tools_Custom_Headers
     * @access public
     * @static
     */
    public static function init( ) {
        return new self( );
    }

    /**
     * Customize register
     *
     * @param  WP_Customize $wp_customize
     * @access public
     *
     * @global Rocks_Tools $rocks_tools
     */
    public function customize_register( $wp_customize ) {
        global $rocks_tools;

        // Custom controls for Customize API
        require plugin_dir_path( __FILE__ ) . 'customize-controls.php';

        $wp_customize->add_section( 'header', array(
            'title'    => __( 'Headers', 'rocks' ),
            'priority' => 30,
        ) );

        // Header Layout
        $wp_customize->add_setting( 'header_layout', array(
            'default' => 'header-1',
        ) );

        $wp_customize->add_control( new Rocks_Tools_Customize_Header_Control( $wp_customize, 'header_layout', array(
            'label'    => __( 'Header Layout', 'rocks' ),
            'section'  => 'header',
            'priority' => 10,
        ) ) );

        // Mobile Header Layout
        if ( $rocks_tools->get_option( 'custom-mobile-headers', false ) ) {
            $wp_customize->add_setting( 'mobile_header_layout', array(
                'default' => 'mobile-1',
            ) );

            $wp_customize->add_control( new Rocks_Tools_Customize_Mobile_Header_Control( $wp_customize, 'mobile_header_layout', array(
                'label'    => __( 'Mobile Header Layout', 'rocks' ),
                'section'  => 'header',
                'priority' => 20,
            ) ) );
        }

        // Footer Layout
        if ( $rocks_tools->get_option( 'custom-footers', false ) ) {
            $wp_customize->add_section( 'footer', array(
                'title'    => __( 'Footers', 'rocks' ),
                'priority' => 90,
            ) );

            $wp_customize->add_setting( 'footer_layout', array(
                'default' => 'footer-1',
            ) );

            $wp_customize->add_control( new Rocks_Tools_Customize_Footer_Control( $wp_customize, 'footer_layout', array(
                'label'    => __( 'Footer Layout', 'rocks' ),
                'section'  => 'footer',
                'priority' => 10,
            ) ) );
        }
    }

    /**
     * Register needed scripts and styles
     *
     * @access public
     * @static
     *
     * @global Rocks_Tools $rocks_tools
     */
    public static function load_scripts_styles( ) {
        global $rocks_tools;

        wp_enqueue_style( 'rocks-tools-custom-headers', plugins_url( 'css/custom-headers.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );
        wp_enqueue_script( 'rocks-tools-custom-headers', plugins_url( 'js/custom-headers.min.js', __FILE__ ), array( 'jquery' ), $rocks_tools->get_version( ), true );
    }

    /**
     * Initialization custom header metabox
     *
     * @access public
     *
     * @global Rocks_Tools $rocks_tools
     */
    public function metabox_init( ) {
        global $rocks_tools;

        $title = __( 'Custom Header & Footer', 'rocks' );

        // Custom footer support
        if ( ! $rocks_tools->get_option( 'custom-footers', false ) ) {
            $title = __( 'Custom Header', 'rocks' );
        }

        // Pages
        if ( current_user_can( 'edit_pages' ) ) {
            add_meta_box( 'rocks_tools_custom_header', $title, array( &$this, 'metabox_content' ), 'page', 'side' );
        }

        // Posts
        if ( current_user_can( 'edit_posts' ) ) {
            add_meta_box( 'rocks_tools_custom_header', $title, array( &$this, 'metabox_content' ), 'post', 'side' );
        }
    }

    /**
     * Get files list
     *
     * @param  string $directory
     * @access public
     * @static
     */
    public static function get_files_list( $directory = 'normal' ) {
        $files = array( );
        $path  = get_template_directory( ) . '/components/custom-headers/' . $directory;

        if ( ! is_dir( $path ) ) {
            return false;
        }

        $list = scandir( $path );

        if ( ! is_array( $list ) ) {
            return false;
        }

        foreach ( $list as $name ) {
            if ( $name == '.' or $name == '..' ) {
                continue;
            }

            $files[] = substr( $name, 0, strrpos( $name, '.' ) );
        }

        if ( count( $files ) > 0 ) {
            return $files;
        }

        return false;
    }

    /**
     * Get formatted name
     *
     * @param  string $name
     * @return string
     * @access public
     */
    public function get_formatted_name( $name ) {
        return ucwords( str_replace( array( '-', '_' ), ' ', $name ) );
    }

    /**
     * Metabox content
     *
     * @param  WP_Post $post
     * @access public
     *
     * @global Rocks_Tools $rocks_tools
     */
    public function metabox_content( $post ) {
        global $rocks_tools;

        self::load_scripts_styles( );

        $list_headers   = $list_footers = '';
        $headers        = $footers = array( );
        $allow_footers  = $rocks_tools->get_option( 'custom-footers', false );

        $current_header = get_post_meta( $post->ID, 'rocks_tools_custom_header_id', true );
        $current_footer = get_post_meta( $post->ID, 'rocks_tools_custom_footer_id', true );

        $previews = $this->get_files_list( );

        if ( $previews !== false ) {
            foreach ( $previews as $name ) {
                $list_headers .= '<li data-id="' . esc_attr( $name ) . '"' . ( $name == $current_header ? ' class="checked"' : '' ) . '><img src="' . get_template_directory_uri( ) . '/components/custom-headers/normal/' . esc_attr( $name ) . '.png"></li>';
            }

            wp_nonce_field( 'rocks_tools_custom_headers_nonce', 'rocks_tools_custom_headers_nonce_safe' );

            echo '
            <div class="rocks_tools_custom_headers_option title">
                <input type="hidden" name="rocks_tools_custom_header_id" value="' . esc_attr( $current_header ) . '">
                <strong>
                    ' . __( 'Header', 'rocks' ) . ' &ndash;
                    <span>' . ( empty( $current_header ) ? __( 'Default', 'rocks' ) : __( 'Custom', 'rocks' ) ) . '</span>
                </strong>
                <i class="fa fa-angle-down"></i>
            </div>

            <div class="rocks_tools_custom_headers_option block">
                <input class="button rocks_tools_custom_headers_reset" value="' . __( 'Set Defaults', 'rocks' ) . '" type="button">
                <ul class="rocks_tools_custom_headers_previews">' . $list_headers . '</ul>
            </div>';
        }

        if ( $allow_footers ) {
            $previews = $this->get_files_list( 'footers' );

            if ( $previews !== false ) {
                foreach ( $previews as $name ) {
                    $list_footers .= '<li data-id="' . esc_attr( $name ) . '"' . ( $name == $current_footer ? ' class="checked"' : '' ) . '><img src="' . get_template_directory_uri( ) . '/components/custom-headers/footers/' . esc_attr( $name ) . '.png"></li>';
                }

                echo '
                <div class="rocks_tools_custom_headers_option title">
                    <input type="hidden" name="rocks_tools_custom_footer_id" value="' . esc_attr( $current_footer ) . '">
                    <strong>
                        ' . __( 'Footer', 'rocks' ) . ' &ndash;
                        <span>' . ( empty( $current_footer ) ? __( 'Default', 'rocks' ) : __( 'Custom', 'rocks' ) ) . '</span>
                    </strong>
                    <i class="fa fa-angle-down"></i>
                </div>

                <div class="rocks_tools_custom_headers_option block">
                    <input class="button rocks_tools_custom_headers_reset" value="' . __( 'Set Defaults', 'rocks' ) . '" type="button">
                    <ul class="rocks_tools_custom_headers_previews">' . $list_footers . '</ul>
                </div>';
            }
        }

        $howto = $allow_footers ? __( 'You can change defaults header and footer on the <a href="%s" target="_blank">customization page</a>.', 'rocks' ) : __( 'You can change default header on the <a href="%s" target="_blank">customization page</a>.', 'rocks' );

        echo '<p class="howto rocks_tools_custom_header_howto">' . sprintf( $howto, 'customize.php' ) . '</p>';
    }

    /**
     * Update metabox details
     *
     * @param  int $post_id
     * @access public
     */
    public function metabox_save( $post_id ) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        if ( ! isset( $_POST['rocks_tools_custom_headers_nonce_safe'] ) or ! wp_verify_nonce( $_POST['rocks_tools_custom_headers_nonce_safe'], 'rocks_tools_custom_headers_nonce' ) ) {
            return;
        }

        if ( ! empty( $_POST['post_type'] ) and $_POST['post_type'] == 'page' ) {
            if ( ! current_user_can( 'edit_pages' ) ) {
                return;
            }
        } else {
            if ( ! current_user_can( 'edit_posts' ) ) {
                return;
            }
        }

        if ( isset( $_POST['rocks_tools_custom_header_id'] ) ) {
            update_post_meta( $post_id, 'rocks_tools_custom_header_id', sanitize_text_field( $_POST['rocks_tools_custom_header_id'] ) );
        }

        if ( isset( $_POST['rocks_tools_custom_footer_id'] ) ) {
            update_post_meta( $post_id, 'rocks_tools_custom_footer_id', sanitize_text_field( $_POST['rocks_tools_custom_footer_id'] ) );
        }
    }
}

// Initialization
add_action( 'rocks_tools_loaded', array( 'Rocks_Tools_Custom_Headers', 'init' ) );
