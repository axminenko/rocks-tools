<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Post views class
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Post_Views
 */
class Rocks_Tools_Post_Views {
	/**
	 * Stored key in database
	 * 
	 * @var    string
	 * @access protected
	 * @static
	 */
	protected static $stored = 'ctr_post_views';

	/**
	 * Get views counter
	 * 
	 * @param  int     $post_id
	 * @param  boolean $formatted
	 * @return int
	 * @access public
	 * @static
	 */
	public static function get( $post_id, $formatted = true ) {
		$count = get_post_meta( $post_id, self::$stored, true );

		if ( empty( $count ) ) {
			$count = 0;

			delete_post_meta( $post_id, self::$stored );
			add_post_meta( $post_id, self::$stored, '0' );
		} else {
			$count = absint( $count );
		}

		if ( $formatted ) {
			return number_format( $count );
		}

		return $count;
	}

	/**
	 * Increment count
	 * 
	 * @param  int $post_id
	 * @return int
	 * @access public
	 * @static
	 */
	public static function inc( $post_id ) {
		$count = get_post_meta( $post_id, self::$stored, true );

		if ( empty( $count ) ) {
			delete_post_meta( $post_id, self::$stored );
			add_post_meta( $post_id, self::$stored, '1' );
		} else {
			$count = absint( $count );

			update_post_meta( $post_id, self::$stored, ++ $count );
		}

		return $count;
	}
}