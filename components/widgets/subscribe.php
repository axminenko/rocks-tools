<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Subscribe widget
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widget_Subscribe
 */
class Rocks_Widget_Subscribe extends WP_Widget {
	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		parent::__construct( 'rocks_widget_subscribe', __( 'Subscribe', 'rocks' ), array( 'description' => 'Subscribe to your website via FeedBurner.', 'classname' => 'widget-subscribe' ) );
	}

	/**
	 * Register widget
	 * 
	 * @access public
	 * @static
	 */
	public static function register( ) {
		register_widget( __CLASS__ );
	}

	/**
	 * Widget content
	 * 
	 * @param  array $args
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function widget( $args, $instance ) {
		$title  = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Subscribe', 'rocks' ) : $instance['title'], $instance, $this->id_base );
		$text   = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
		$feedId = apply_filters( 'widget_text', empty( $instance['feedId'] ) ? '' : $instance['feedId'], $instance );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];

		if ( ! empty( $text ) ) {
			echo wpautop( $text );
		}

		echo '
		<form class="feedemail-form" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open( \'http://feedburner.google.com/fb/a/mailverify?uri=' . esc_js( $feedId ) . '\', \'popupwindow\', \'scrollbars=yes, width=600, height=550\' ); return true;">
			<input type="text" class="feedemail-input" name="email" maxlength="150" value="" placeholder="' . esc_attr__( 'your.email@address', 'rocks' ) . '" />
			<input type="hidden" value="' . esc_attr( $feedId ) . '" name="uri" />
			<input type="hidden" name="loc" value="' . esc_attr( str_replace( '-', '_', get_bloginfo( 'language' ) ) ) . '"/>
			<input type="submit" value="' . esc_attr__( 'Subscribe', 'rocks' ) . '" />
		</form>';

		echo $args['after_widget'];
	}

	/**
	 * Widget options
	 * 
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance,
			array(
				'title'  => '',
				'text'   => __( 'Sign up for our newsletter to receive the latest news and event postings.', 'rocks' ),
				'feedId' => '',
			)
		);

		$title  = strip_tags( $instance['title'] );
		$text   = esc_textarea( $instance['text'] );
		$feedId = strip_tags( $instance['feedId'] );

		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '" style="padding-bottom: 10px;">' . __( 'Title:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<textarea class="widefat" rows="4" cols="20" id="' . $this->get_field_id( 'text' ) . '" name="' . $this->get_field_name( 'text' ) . '">' . $text . '</textarea>
		<p>
			<label for="' . $this->get_field_id( 'feedId' ) . '" style="padding-bottom: 10px;">' . __( 'Feedburner ID:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'feedId' ) . '" name="' . $this->get_field_name( 'feedId' ) . '" type="text" value="' . esc_attr( $feedId ) . '" />
		</p>';
	}

	/**
	 * Update widget options
	 * 
	 * @param  array $instance
	 * @param  array $old_instance
	 * @return array
	 * @access public
	 */
	public function update( $instance, $old_instance ) {
		return array(
			'title'  => strip_tags( $instance['title'] ),
			'text'   => stripslashes( wp_filter_post_kses( addslashes( $instance['text'] ) ) ),
			'feedId' => sanitize_text_field( $instance['feedId'] ),
		);
	}
}

// Register widget
add_action( 'widgets_init', array( 'Rocks_Widget_Subscribe', 'register' ) );