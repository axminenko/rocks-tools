<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Get in touch widget
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widget_Get_In_Touch
 */
class Rocks_Widget_Get_In_Touch extends WP_Widget {
	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		parent::__construct( 'rocks_widget_get_in_touch', __( 'Get In Touch', 'rocks' ), array( 'description' => 'Share your contact information.', 'classname' => 'widget-info' ) );
	}

	/**
	 * Register widget
	 * 
	 * @access public
	 * @static
	 */
	public static function register( ) {
		register_widget( __CLASS__ );
	}

	/**
	 * Widget content
	 * 
	 * @param  array $args
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function widget( $args, $instance ) {
		$title   = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Get In Touch', 'rocks' ) : $instance['title'], $instance, $this->id_base );
		$text    = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
		$phone   = apply_filters( 'widget_text', empty( $instance['phone'] ) ? '' : $instance['phone'], $instance );
		$time    = apply_filters( 'widget_text', empty( $instance['time'] ) ? '' : $instance['time'], $instance );
		$address = apply_filters( 'widget_text', empty( $instance['address'] ) ? '' : $instance['address'], $instance );
		$email   = apply_filters( 'widget_text', empty( $instance['email'] ) ? '' : $instance['email'], $instance );
		$name    = apply_filters( 'widget_text', empty( $instance['name'] ) ? '' : $instance['name'], $instance );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];

		if ( ! empty( $text ) ) {
			echo wpautop( $text );
		}

		echo '<address>';

		if ( ! empty( $phone ) ) {
			echo '<p class="widget-info-phone"><span>' . __( 'Phone:', 'rocks' ) . '</span> ' . esc_html( $phone ) . '</p>';
		}

		if ( ! empty( $time ) ) {
			echo '<p class="widget-info-time"><span>' . __( 'Time:', 'rocks' ) . '</span> ' . esc_html( $time ) . '</p>';
		}

		if ( ! empty( $address ) ) {
			echo '<p class="widget-info-address"><span>' . __( 'Address:', 'rocks' ) . '</span> ' . esc_html( $address ) . '</p>';
		}

		if ( ! empty( $email ) ) {
			echo '<p class="widget-info-email"><span>' . __( 'Email:', 'rocks' ) . '</span> <a href="mailto:' . esc_attr( antispambot( $email ) ) . '">' . esc_html( antispambot( $email ) ) . '</a></p>';
		}

		if ( ! empty( $name ) ) {
			echo '<p class="widget-info-name"><span>' . __( 'Name:', 'rocks' ) . '</span> ' . esc_html( $name ) . '</p>';
		}

		echo '</address>';

		echo $args['after_widget'];
	}

	/**
	 * Widget options
	 * 
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance,
			array(
				'title'   => '',
				'text'    => '',
				'address' => __( '5 Park Avenue, New York, NY 10016', 'rocks' ),
				'phone'   => __( '(123) 45678901', 'rocks' ),
				'time'    => __( 'Mon - Sat: 9:00 - 18:00', 'rocks' ),
				'email'   => __( 'john@doe.com', 'rocks' ),
				'name'    => __( 'John Doe', 'rocks' ),
			)
		);

		$title   = strip_tags( $instance['title'] );
		$text    = esc_textarea( $instance['text'] );
		$address = strip_tags( $instance['address'] );
		$phone   = strip_tags( $instance['phone'] );
		$time    = strip_tags( $instance['time'] );
		$email   = strip_tags( $instance['email'] );
		$name    = strip_tags( $instance['name'] );

		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '" style="padding-bottom: 10px;">' . __( 'Title:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<textarea class="widefat" rows="4" cols="20" id="' . $this->get_field_id( 'text' ) . '" name="' . $this->get_field_name( 'text' ) . '">' . $text . '</textarea>

		<p>
			<label for="' . $this->get_field_id( 'phone' ) . '" style="padding-bottom: 10px;">' . __( 'Phone:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'phone' ) . '" name="' . $this->get_field_name( 'phone' ) . '" type="text" value="' . esc_attr( $phone ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'time' ) . '" style="padding-bottom: 10px;">' . __( 'Time:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'time' ) . '" name="' . $this->get_field_name( 'time' ) . '" type="text" value="' . esc_attr( $time ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'address' ) . '" style="padding-bottom: 10px;">' . __( 'Address:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'address' ) . '" name="' . $this->get_field_name( 'address' ) . '" type="text" value="' . esc_attr( $address ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'email' ) . '" style="padding-bottom: 10px;">' . __( 'Email:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'email' ) . '" name="' . $this->get_field_name( 'email' ) . '" type="text" value="' . esc_attr( $email ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'name' ) . '" style="padding-bottom: 10px;">' . __( 'Name:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'name' ) . '" name="' . $this->get_field_name( 'name' ) . '" type="text" value="' . esc_attr( $name ) . '" />
		</p>';
	}

	/**
	 * Update widget options
	 * 
	 * @param  array $instance
	 * @param  array $old_instance
	 * @return array
	 * @access public
	 */
	public function update( $instance, $old_instance ) {
		return array(
			'title'   => strip_tags( $instance['title'] ),
			'text'    => stripslashes( wp_filter_post_kses( addslashes( $instance['text'] ) ) ),
			'address' => sanitize_text_field( $instance['address'] ),
			'phone'   => sanitize_text_field( $instance['phone'] ),
			'time'    => sanitize_text_field( $instance['time'] ),
			'email'   => sanitize_text_field( $instance['email'] ),
			'name'    => sanitize_text_field( $instance['name'] ),
		);
	}
}

// Register widget
add_action( 'widgets_init', array( 'Rocks_Widget_Get_In_Touch', 'register' ) );