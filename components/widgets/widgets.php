<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Enqueue widget styles
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widgets
 */
function rocks_tools_widgets_styles( ) {
	global $rocks_tools;
	
	wp_enqueue_style( 'rocks-tools-widgets', plugins_url( 'css/widgets.min.css', __FILE__ ), false, $rocks_tools->get_version( ) );
}

// Register widgets styles
add_action( 'admin_enqueue_scripts', 'rocks_tools_widgets_styles' );