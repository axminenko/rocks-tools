<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Recent posts widget
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widget_Recent_Posts
 */
class Rocks_Widget_Recent_Posts extends WP_Widget {
	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		parent::__construct( 'Rocks_widget_recent_posts', __( 'Recent Posts with Thumbnails', 'rocks' ), array( 'description' => 'Your site’s most recent Posts with thumbnails.', 'classname' => 'widget-posts' ) );
	}

	/**
	 * Register widget
	 * 
	 * @access public
	 * @static
	 */
	public static function register( ) {
		register_widget( __CLASS__ );
	}

	/**
	 * Widget content
	 * 
	 * @param  array $args
	 * @param  array $instance
	 * @return string
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function widget( $args, $instance ) {
		global $rocks_tools;

		$title     = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts', 'rocks' ) : $instance['title'], $instance, $this->id_base );
		$type      = apply_filters( 'widget_text', empty( $instance['type'] ) ? 'recent' : $instance['type'], $instance );
		$timeframe = apply_filters( 'widget_text', empty( $instance['timeframe'] ) ? 'all' : $instance['timeframe'], $instance );
		$cats      = apply_filters( 'widget_text', empty( $instance['cats'] ) ? '' : $instance['cats'], $instance );
		$qty       = intval( empty( $instance['qty'] ) ? 5 : $instance['qty'] );
		$thumb     = ! empty( $instance['thumb'] ) ? '1' : '0';
		$date      = ! empty( $instance['date'] ) ? '1' : '0';
		$excerpt   = ! empty( $instance['excerpt'] ) ? '1' : '0';

		if ( ! empty( $cats ) ) {
			$cats_array = explode( ',', $cats );

			if ( is_array( $cats_array ) ) {
				foreach ( $cats_array as & $name ) {
					$name = trim( $name );
				}

				$cats = implode( ',', $cats_array );
			} else {
				$cats = '';
			}
		}

		$args_query = array(
			'post_type'           => 'post',
			'posts_per_page'      => intval( $qty ),
			'post_status'         => 'publish',
			'orderby'             => 'date',
			'order'               => 'DESC',
			'post_status'         => 'publish',
			'category_name'       => $cats,
			'date_query'          => ( $timeframe != 'all' ? array( array( 'column' => 'post_date_gmt', 'after'  => '1 ' . $timeframe . ' ago' ) ) : '' ),
			'ignore_sticky_posts' => 1,
		);

		if ( $type == 'most_viewed' and $rocks_tools->theme_support( 'post-views', 'components' ) ) {
			$args_query['orderby']  = 'meta_value_num';
			$args_query['meta_key'] = 'post_views_count';
		} else if ( $type == 'most_commented' ) {
			$args_query['orderby'] = 'comment_count';
		}

		$query = get_transient( 'crt_widget_' . $type );

		if ( $query === false ) {
			$query = new WP_Query( $args_query );

			set_transient( 'crt_widget_' . $type, $query, 60 * 60 * 4 );
		}

		if ( ! $query->have_posts( ) ) {
			return;
		}

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];

		while ( $query->have_posts( ) ) {
			$query->the_post( );

			$image   = $comments_info = $views_info = '';
			$classes = array( 'widget-posts-post-box' );

			$post_title = get_the_title( );
			$attr_title = the_title_attribute( array( 'before' => '', 'after' => '', 'echo' => false ) );

			if ( $thumb ) {
				if ( has_post_thumbnail( ) ) {
					$classes[] = 'widget-posts-post-box-with-thumb';
					$image     = '<a href="' . esc_url( get_permalink( ) ) . '" title="' . $attr_title . '" class="widget-posts-image">' . get_the_post_thumbnail( get_the_ID( ), 'thumbnail' ) . '</a>';
				} else {
					$classes[] = 'widget-posts-post-box-with-icon';
					$image     = '<a href="' . esc_url( get_permalink( ) ) . '" title="' . $attr_title . '" class="widget-posts-icon widget-posts-icon-' . esc_attr( $this->get_post_format( get_the_ID( ) ) ) . '"></a>';
				}
			}

			echo '<div>';

			if ( ! empty( $image ) ) {
				echo $image;
			}

			echo '<div' . ( count( $classes ) > 0 ? ' class="' . implode( ' ', $classes ) . '"' : '' ) . '>';

			if ( ! empty( $post_title ) ) {
				echo '<a class="widget-posts-title" href="' . esc_url( get_permalink( ) ) . '" title="' . $attr_title . '">' . esc_html( $post_title ) . '</a>';
			}

			if ( $date ) {
				echo '<div class="widget-posts-date">' . get_the_time( get_option( 'date_format' ) ) . '</div>';
			}

			if ( $type == 'most_commented' and get_comments_number( ) > 0 ) {
				echo '<div class="widget-posts-comments">' . comments_number( __( 'No Comments', 'rocks' ), __( '1 Comment', 'rocks' ), __( '% Comments', 'rocks' ) ) . '</div>';
			} else if ( $type == 'most_viewed' ) {
				echo '<div class="widget-posts-views">' . sprintf( __( '%s Views', 'rocks' ), number_format( get_post_meta( get_the_ID( ), 'post_views_count', true ) ) ) . '</div>';
			}

			if ( $excerpt ) {
				echo '<div class="widget-posts-excerpt">' . wpautop( get_the_excerpt( ) ) . '</div>';
			}

			echo '</div>';

			echo '</div>';
		}

		wp_reset_postdata( );

		echo $args['after_widget'];
	}

	/**
	 * Widget options
	 * 
	 * @param  array $instance
	 * @return string
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function form( $instance ) {
		global $rocks_tools;

		$instance = wp_parse_args( ( array ) $instance,
			array(
				'title'     => '',
				'type'      => 'recent',
				'timeframe' => 'all',
				'cats'      => '',
				'qty'       => '5'
			)
		);

		$title     = strip_tags( $instance['title'] );
		$type      = strip_tags( $instance['type'] );
		$timeframe = strip_tags( $instance['timeframe'] );
		$cats      = strip_tags( $instance['cats'] );
		$qty       = intval( $instance['qty'] );
		$thumb     = isset( $instance['thumb'] ) ? ( bool ) $instance['thumb'] : false;
		$date      = isset( $instance['date'] ) ? ( bool ) $instance['date'] : false;
		$excerpt   = isset( $instance['excerpt'] ) ? ( bool ) $instance['excerpt'] : false;

		if ( $rocks_tools->theme_support( 'post-views', 'components' ) ) {
			$most_viewed = '<option value="most_viewed" ' . selected( $type, 'most_viewed', false ) . '>' . __( 'Most viewed', 'rocks' ) . '</option>';
		} else {
			$most_viewed = '';
		}

		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '" style="padding-bottom: 10px;">' . __( 'Title:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'type' ) . '" style="padding-bottom: 10px;">' . __( 'Posts:', 'rocks' ) . '</label>&nbsp;
			<select name="' . $this->get_field_name( 'type' ) . '" id="' . $this->get_field_id( 'type' ) . '" class="widefat">
				<option value="recent" ' . selected( $type, 'recent', false ) . '>' . __( 'Recent', 'rocks' ) . '</option>
				' . $most_viewed . '
				<option value="most_commented" ' . selected( $type, 'most_commented', false ) . '>' . __( 'Most commented', 'rocks' ) . '</option>
			</select>
		</p>
		<p>
			<label for="' . $this->get_field_id( 'timeframe' ) . '" style="padding-bottom: 10px;">' . __( 'Time Frame:', 'rocks' ) . '</label>&nbsp;
			<select name="' . $this->get_field_id( 'timeframe' ) . '" id="' . $this->get_field_id( 'timeframe' ) . '" class="widefat">
				<option value="all"' . selected( $timeframe, 'all', false ) . '>' . __( 'All', 'rocks' ) . '</option>
				<option value="year"' . selected( $timeframe, 'year', false ) . '>' . __( 'Year', 'rocks' ) . '</option>
				<option value="month"' . selected( $timeframe, 'month', false ) . '>' . __( 'Month', 'rocks' ) . '</option>
				<option value="week"' . selected( $timeframe, 'week', false ) . '>' . __( 'Week', 'rocks' ) . '</option>
			</select>
		</p>
		<p>
			<label for="' . $this->get_field_id( 'cats' ) . '" style="padding-bottom: 10px;">' . __( 'Categories (optional, slugs separated by comma):', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'cats' ) . '" name="' . $this->get_field_name( 'cats' ) . '" type="text" value="' . esc_attr( $cats ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'qty' ) . '" style="padding-right: 5px;">' . __( 'Number of posts to show:', 'rocks' ) . '</label>
			<input id="' . $this->get_field_id( 'qty' ) . '" name="' . $this->get_field_name( 'qty' ) . '" type="text" value="' . esc_attr( $qty ) . '" size="3" />
		</p>
		<p>
			<input class="checkbox" id="' . $this->get_field_id( 'thumb' ) . '" name="' . $this->get_field_name( 'thumb' ) . '" type="checkbox" ' . checked( $thumb, true, false ) . ' />
			<label for="' . $this->get_field_id( 'thumb' ) . '">' . __( 'Display thumbnails', 'rocks' ) . '</label>
			<br>
			<input class="checkbox" id="' . $this->get_field_id( 'date' ) . '" name="' . $this->get_field_name( 'date' ) . '" type="checkbox" ' . checked( $date, true, false ) . ' />
			<label for="' . $this->get_field_id( 'date' ) . '">' . __( 'Show post date', 'rocks' ) . '</label>
			<br>
			<input class="checkbox" id="' . $this->get_field_id( 'excerpt' ) . '" name="' . $this->get_field_name( 'excerpt' ) . '" type="checkbox" ' . checked( $excerpt, true, false ) . ' />
			<label for="' . $this->get_field_id( 'excerpt' ) . '">' . __( 'Show post excerpt', 'rocks' ) . '</label>
		</p>';
	}

	/**
	 * Update widget options
	 * 
	 * @param  array $instance
	 * @param  array $old_instance
	 * @return array
	 * @access public
	 */
	public function update( $instance, $old_instance ) {
		delete_transient( 'rocks_tools_widget_' . sanitize_text_field( $instance['type'] ) );

		return array(
			'title'     => strip_tags( $instance['title'] ),
			'type'      => sanitize_text_field( $instance['type'] ),
			'timeframe' => sanitize_text_field( $instance['timeframe'] ),
			'cats'      => sanitize_text_field( $instance['cats'] ),
			'qty'       => intval( $instance['qty'] ),
			'thumb'     => ! empty( $instance['thumb'] ) ? 1 : 0,
			'date'      => ! empty( $instance['date'] ) ? 1 : 0,
			'excerpt'   => ! empty( $instance['excerpt'] ) ? 1 : 0,
		);
	}

	/**
	 * Get post format type
	 * 
	 * @param  int $post_id
	 * @return string
	 * @access private
	 */
	private function get_post_format( $post_id ) {
		$format = get_post_format( $post_id );

		if ( $format === false ) {
			$format = 'standard';
		}

		return $format;
	}
}

// Register widget
add_action( 'widgets_init', array( 'Rocks_Widget_Recent_Posts', 'register' ) );