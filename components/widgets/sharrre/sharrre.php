<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Sharrre widget
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widget_Sharrre
 */
class Rocks_Widget_Sharrre extends WP_Widget {
	/**
	 * Social networks array
	 * 
	 * @var    array
	 * @access private
	 */
	private $networks = null;

	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		$this->networks = array(
			'googlePlus'  => __( 'Google+', 'rocks' ),
			'facebook'    => __( 'Facebook', 'rocks' ),
			'twitter'     => __( 'Twitter', 'rocks' ),
			'digg'        => __( 'Digg', 'rocks' ),
			'delicious'   => __( 'Delicious', 'rocks' ),
			'stumbleupon' => __( 'StumbleUpon', 'rocks' ),
			'linkedin'    => __( 'LinkedIn', 'rocks' ),
			'pinterest'   => __( 'Pinterest', 'rocks' ),
		);

		parent::__construct( 'rocks_widget_sharrre', __( 'Sharrre', 'rocks' ), array( 'description' => 'Sharing for Facebook, Twitter, Google Plus and more.', 'classname' => 'widget-sharrre' ) );
	}

	/**
	 * Register widget
	 * 
	 * @access public
	 * @static
	 */
	public static function register( ) {
		register_widget( __CLASS__ );
	}

	/**
	 * Widget content
	 * 
	 * @param  array $args
	 * @param  array $instance
	 * @return string
	 * @access public
	 *
	 * @global WP_Query    $wp_query
	 * @global Rocks_Tools $rocks_tools
	 */
	public function widget( $args, $instance ) {
		global $wp_query, $rocks_tools;

		$title    = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$networks = array( );

		foreach ( $this->networks as $network => $label ) {
			$networks[$network] = ! empty( $instance[$network] ) ? true : false;
		}

		$lang_origin   = get_bloginfo( 'language' );
		$lang_facebook = str_replace('-', '_', $lang_origin );
		$lang_twitter  = explode( '-', $lang_origin );
		$lang_twitter  = reset( $lang_twitter );

		$image     = false;
		$permalink = $text = $description = '';

		if ( ! isset( $wp_query ) or $wp_query === null ) {
			return;
		}

		if ( is_page( ) or is_single( ) ) {
			$permalink   = get_permalink( $wp_query->post->ID );
			$text        = wptexturize( wp_kses( $wp_query->post->post_title, array( ) ) );
			$image       = $rocks_tools->get_post_image_src( $wp_query->post );
			$description = wptexturize( wp_kses( $wp_query->post->post_excerpt, array( ) ) );
		} elseif ( isset( $wp_query->queried_object ) and isset( $wp_query->queried_object->taxonomy ) and ! empty( $wp_query->queried_object->taxonomy ) ) {
			$term        = get_term( $wp_query->queried_object->term_id, $wp_query->queried_object->taxonomy );
			$permalink   = get_term_link( $term->term_id, $term->taxonomy );
			$text        = wptexturize( wp_kses( $term->name, array( ) ) );
			$description = wptexturize( wp_kses( $term->description, array( ) ) );
		} else {
			return;
		}
		
		wp_enqueue_script( 'sharrre', plugins_url( 'js/jquery.sharrre.min.js', __FILE__ ), array( 'jquery' ), '1.3.5', true );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];

		echo '<div class="sharrre-wrapper">';

		foreach ( $networks as $network => $state ) {
			if ( $state !== true ) {
				continue;
			}

			echo '
			<div id="sharrre_' . esc_attr( $network ) . '" class="sharrre" data-url="' . esc_url( $permalink ) . '" data-text="' . esc_attr( $text ) . '" data-title="' . esc_attr( $this->networks[$network] ) . '">
				<a href="#" class="box"><div class="count">0</div><div class="share"><span></span>' . esc_html( $this->networks[$network] ) . '</div></a>
			</div>';
		}

		echo '</div>';

		echo '
		<script type="text/javascript">
		/* <![CDATA[ */
		jQuery( function( ) {';
		
		foreach ( $networks as $network => $state ) {
			if ( $network == 'title' or $state !== true ) {
				continue;
			}

			if ( $network == 'googlePlus' or $network == 'stumbleupon' ) {
				$urlCurl = 'urlCurl: \'' . esc_js( plugins_url( 'js/sharrre.php', __FILE__ ) ) . '\',';
			} else {
				$urlCurl = '';
			}

			echo '
			jQuery( "#sharrre_' . esc_js( $network ) . '" ).sharrre( {
				share: { ' . esc_js( $network ) . ': true },
				template: \'<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>' . esc_js( $this->networks[$network] ) . '</div></a>\',
				enableHover: false,
				click: function( api, options ) {
					api.simulateClick( );
					api.openPopup( \'' . esc_js( $network ) . '\' );
				},
				' . $urlCurl . '
				buttons: {';

			if ( $network == 'googlePlus' ) {
				echo 'googlePlus : { url: \'' . esc_js( $permalink ) . '\', urlCount: false, size: "medium", lang: \'' . esc_js( $lang_origin ) . '\', annotation: \'' . esc_js( $description ) . '\' }';
			} else if ( $network == 'facebook' ) {
				echo 'facebook: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, action: "like", layout: "button_count", width: "", send: "false", faces: "false", colorscheme: "", font: "", lang: \'' . esc_js( $lang_facebook ) . '\' }';
			} else if ( $network == 'twitter' ) {
				echo 'twitter: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, count: "horizontal", hashtags: "", via: "", related: "", lang: \'' . esc_js( $lang_twitter ) . '\' }';
			} else if ( $network == 'digg' ) {
				echo 'digg: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, type: "DiggCompact" }';
			} else if ( $network == 'delicious' ) {
				echo 'delicious: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, size: "medium" }';
			} else if ( $network == 'stumbleupon' ) {
				echo 'stumbleupon: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, layout: "1" }';
			} else if ( $network == 'linkedin' ) {
				echo 'linkedin: { url: \'' . esc_js( $permalink ) . '\', urlCount: false, counter: "" }';
			} else if ( $network == 'pinterest' ) {
				echo 'pinterest: { url: \'' . esc_js( $permalink ) . '\', media: \'' . ( is_array( $image ) ? esc_js( $image[0] ) : '' ) . '\', description: \'' . esc_js( $description ) . '\', layout: "horizontal" }';
			}

			echo ' } } );';
		}

		echo ' } );
		/* ]]> */
		</script>';

		echo $args['after_widget'];
	}

	/**
	 * Widget options
	 * 
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance,
			array(
				'title' => '',
			)
		);

		$title = strip_tags( $instance['title'] );

		$networks = array( );
		$output   = '';
		$count    = count( $this->networks );
		$i        = 0;

		foreach ( $this->networks as $network => $label ) {
			$i ++;
			$checked = isset( $instance[$network] ) ? ( bool ) $instance[$network] : false;

			$output .= '
			<input class="checkbox" id="' . $this->get_field_id( $network ) . '" name="' . $this->get_field_name( $network ) . '" type="checkbox" ' . checked( $checked, true, false ) . ' />
			<label for="' . $this->get_field_id( $network ) . '">' . $label . '</label>';

			if ( $i != $count ) {
				$output .= '<br>';
			}
		}

		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '" style="padding-bottom: 10px;">' . __( 'Title:', 'rocks' ) . '</label> 
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>' . __( 'Social Networks:', 'rocks' ) . '<br style="margin-bottom: 4px;">' . $output . '</p>';
	}

	/**
	 * Update widget options
	 * 
	 * @param  array $instance
	 * @param  array $old_instance
	 * @return array
	 * @access public
	 */
	public function update( $instance, $old_instance ) {
		$fields = array(
			'title' => strip_tags( $instance['title'] )
		);

		foreach ( $this->networks as $network => $label ) {
			$fields[$network] = ! empty( $instance[$network] ) ? 1 : 0;
		}

		return $fields;
	}
}

// Register widget
add_action( 'widgets_init', array( 'Rocks_Widget_Sharrre', 'register' ) );