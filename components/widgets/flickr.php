<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 * 
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Flickr widget
 * 
 * @package    Create_Rocks_Tools
 * @subpackage Widget_Flickr
 */
class Rocks_Widget_Flickr extends WP_Widget {
	/**
	 * Constructor
	 * 
	 * @access public
	 */
	function __construct( ) {
		parent::__construct( 'rocks_widget_flickr', __( 'Flickr Feed', 'rocks' ), array( 'description' => 'Your photos from Flickr.', 'classname' => 'widget-sharrre' ) );
	}

	/**
	 * Register widget
	 * 
	 * @access public
	 * @static
	 */
	public static function register( ) {
		register_widget( __CLASS__ );
	}

	/**
	 * Widget content
	 * 
	 * @param  array $args
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Flickr', 'rocks' ) : $instance['title'], $instance, $this->id_base );
		$id    = apply_filters( 'widget_text', empty( $instance['id'] ) ? '52617155@N08' : $instance['id'], $instance );
		$type  = apply_filters( 'widget_text', empty( $instance['type'] ) ? 'user' : $instance['type'], $instance );
		$qty   = intval( empty( $instance['qty'] ) ? 6 : $instance['qty'] );
		$order = apply_filters( 'widget_text', empty( $instance['order'] ) ? 'latest' : $instance['order'], $instance );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];

		echo '
		<div class="flickr">
			<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=' . intval( $qty ) . '&amp;display=' . esc_attr( $order ) . '&amp;size=s&amp;layout=x&amp;source=' . esc_attr( $type ) . '&amp;' . esc_attr( $type ) . '=' . esc_attr( $id ) . '"></script>
		</div>';

		echo $args['after_widget'];
	}

	/**
	 * Widget options
	 * 
	 * @param  array $instance
	 * @return string
	 * @access public
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance,
			array(
				'title' => '',
				'id'    => '52617155@N08',
				'type'  => 'user',
				'qty'   => '6',
				'order' => 'latest'
			)
		);

		$title = strip_tags( $instance['title'] );
		$id    = strip_tags( $instance['id'] );
		$type  = strip_tags( $instance['type'] );
		$qty   = intval( $instance['qty'] );
		$order = strip_tags( $instance['order'] );

		echo '
		<p>
			<label for="' . $this->get_field_id( 'title' ) . '" style="padding-bottom: 10px;">' . __( 'Title:', 'rocks' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'id' ) . '" style="padding-bottom: 10px;">' . __( 'Flickr Account ID:', 'rocks' ) . '</label>
			<input class="widefat" id="' . $this->get_field_id( 'id' ) . '" name="' . $this->get_field_name( 'id' ) . '" type="text" value="' . esc_attr( $id ) . '" />
		</p>
		<p>
			<label for="' . $this->get_field_id( 'type' ) . '" style="padding-bottom: 10px;">' . __( 'Account Type:', 'rocks' ) . '</label>&nbsp;
			<select name="' . $this->get_field_name( 'type' ) . '" id="' . $this->get_field_id( 'type' ) . '" class="widefat">
				<option value="user" ' . selected( $type, 'user', false ) . '>' . __( 'User', 'rocks' ) . '</option>
				<option value="group" ' . selected( $type, 'group', false ) . '>' . __( 'Group', 'rocks' ) . '</option>
			</select>
		</p>
		<p>
			<label for="' . $this->get_field_id( 'order' ) . '" style="padding-bottom: 10px;">' . __( 'Sort:', 'rocks' ) . '</label>&nbsp;
			<select name="' . $this->get_field_name( 'order' ) . '" id="' . $this->get_field_id( 'order' ) . '" class="widefat">
				<option value="latest" ' . selected( $order, 'latest', false ) . '>' . __( 'Recent', 'rocks' ) . '</option>
				<option value="random" ' . selected( $order, 'random', false ) . '>' . __( 'Random order', 'rocks' ) . '</option>
			</select>
		</p>
		<p>
			<label for="' . $this->get_field_id( 'qty' ) . '" style="padding-right: 5px;">' . __( 'Number of photos to show:', 'rocks' ) . '</label>
			<input id="' . $this->get_field_id( 'qty' ) . '" name="' . $this->get_field_name( 'qty' ) . '" type="text" value="' . esc_attr( $qty ) . '" size="3" />
		</p>';
	}

	/**
	 * Update widget options
	 * 
	 * @param  array $instance
	 * @param  array $old_instance
	 * @return array
	 * @access public
	 */
	public function update( $instance, $old_instance ) {
		return array(
			'title' => strip_tags( $instance['title'] ),
			'id'    => sanitize_text_field( $instance['id'] ),
			'type'  => sanitize_text_field( $instance['type'] ),
			'qty'   => intval( $instance['qty'] ),
			'order' => sanitize_text_field( $instance['order'] ),
		);
	}
}

// Register widget
add_action( 'widgets_init', array( 'Rocks_Widget_Flickr', 'register' ) );