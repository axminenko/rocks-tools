/*!
 * Create.Rocks Tools | Version 0.1.0 | http://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */

jQuery( document ).ready( function( $ ) {
	/**
	 * On load page
	 */
	$( '.rocks-tools-demo-importer-template input' ).removeAttr( 'checked disabled' );

	/**
	 * On select layout
	 */
	$( '.rocks-tools-demo-importer-template input' ).on( 'change', function( ) {
		$( '#rocks_tools_demo_importer_current' ).val( $( this ).val( ) );
		$( '#rocks_tools_demo_importer_button' ).removeClass( 'button-disabled' );
	} );

	/**
	 * Start import
	 */
	$( '#rocks_tools_demo_importer_button' ).on( 'click', function( e ) {
		$( '.rocks-tools-demo-importer-template input' ).each( function( ) {
			$( this ).attr( 'disabled', true );
		} );

		$( this ).hide( );
		$( '.rocks-tools-demo-importer-process' ).show( );

		$.post( ajaxurl, { action: 'rocks_tools_demo_importer_start', layout: $( '#rocks_tools_demo_importer_current' ).val( ) }, function( response ) {
			if ( response.indexOf( 'All done' ) > 0 ) {
				response = '<strong>' + rocks_tools_demo_importer.completed + '</strong><p><a href="index.php">' + rocks_tools_demo_importer.dashboard + '</a></p>';
				$( '.rocks-tools-demo-importer-process' ).addClass( 'updated' ).removeClass( 'notice' ).html( response );
			} else {
				$( '.rocks-tools-demo-importer-process' ).addClass( 'error' ).removeClass( 'notice' ).html( response );
			}
		} );
	} );
} );