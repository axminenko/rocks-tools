<?php
/**
 * Create.Rocks Tools
 * A poweful plugin to extend functionality to your WordPress themes offering shortcodes, font icons and useful widgets.
 *
 * @package   Create_Rocks_Tools
 * @author    Create.Rocks Team <support@create.rocks>
 * @copyright 2014 - 2016 Create.Rocks
 * @license   http://www.gnu.org/licenses/gpl-2.0.html  GPLv2
 * @version   0.1.0
 * @link      http://create.rocks/plugin/tools
 */

/**
 * Demo importer
 *
 * @package    Create_Rocks_Tools
 * @subpackage Demo_Importer
 */
class Rocks_Tools_Demo_Importer {
	/**
	 * Plugin instance
	 *
	 * @var    Rocks_Tools
	 * @access private
	 */
	private $__plugin_instance;

	/**
	 * Allow export widgets and options
	 *
	 * @var    boolean
	 * @access private
	 */
	private $__allow_export;

	/**
	 * Constructor
	 *
	 * @param  Rocks_Tools $plugin_instance
	 * @access public
	 */
	function __construct( $plugin_instance ) {
		if ( ! is_dir( get_template_directory( ) . '/components/demo-importer' ) ) {
			return;
		}

		$this->__plugin_instance = $plugin_instance;
		$this->__allow_export    = false;

		add_action( 'admin_menu', array( &$this, 'menu_item' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts' ) );
		add_action( 'wp_ajax_rocks_tools_demo_importer_start', array( &$this, 'import' ) );
	}

	/**
	 * Create class instance
	 *
	 * @param  Rocks_Tools $plugin_instance
	 * @return Rocks_Tools_Demo_Importer
	 * @access public
	 * @static
	 */
	public static function instance( $plugin_instance = null ) {
		return new self( $plugin_instance );
	}

	/**
	 * Add menu item
	 *
	 * @access public
	 */
	public function menu_item( ) {
		add_theme_page( __( 'Demo Importer', 'rocks' ), __( 'Demo Importer', 'rocks' ), 'import', 'demo-importer', array( &$this, 'page_content' ) );
	}

	/**
	 * Enqueue scripts
	 *
	 * @param  string $hook
	 * @access public
	 *
	 * @global Rocks_Tools $rocks_tools
	 */
	public function enqueue_scripts( $hook ) {
		global $rocks_tools;

		if ( $hook != 'appearance_page_demo-importer' ) {
			return;
		}

		wp_enqueue_style( 'rocks-tools-demo-importer', plugins_url( 'css/demo-importer.min.css', __FILE__ ), array( ), $rocks_tools->get_version( ) );
		wp_enqueue_script( 'rocks-tools-demo-importer', plugins_url( 'js/demo-importer.min.js', __FILE__ ), array( 'jquery' ), $rocks_tools->get_version( ), true );

		wp_localize_script( 'rocks-tools-demo-importer', 'rocks_tools_demo_importer', array(
			'completed' => __( 'Completed. Have fun!', 'rocks' ),
			'dashboard' => __( 'Go to Dashboard &rarr; Home', 'rocks' ),
		) );
	}

	/**
	 * Parse and return demo layouts
	 *
	 * @return boolean|array
	 * @access private
	 */
	private function __parse_layouts( ) {
		$path = get_template_directory( ) . '/components/demo-importer';
		$list = scandir( $path );

		if ( ! is_array( $list ) ) {
			return false;
		}

		$layouts = array( );

		foreach ( $list as $name ) {
			$current = $path . '/' . $name;

			if ( $name == '.' or $name == '..' or ! is_dir( $current ) or ! file_exists( $current . '/content.xml' ) ) {
				continue;
			}

			// Layout
			$layouts[$name]['title'] = ucwords( str_replace( '-', ' ', $name ) );

			if ( file_exists( $current . '/screenshot.png' ) ) {
				$layouts[$name]['screenshot'] = get_template_directory_uri( ) . '/components/demo-importer/' . $name . '/screenshot.png';
			}

			// Content
			$layouts[$name]['content'] = $current . '/content.xml';

			// Widgets
			if ( file_exists( $current . '/widgets.json' ) ) {
				$layouts[$name]['widgets'] = $current . '/widgets.json';
			} else if ( file_exists( $path . 'widgets.json' ) ) {
				$layouts[$name]['widgets'] = $path . 'widgets.json';
			}

			// Theme options
			if ( file_exists( $current . '/options.json' ) ) {
				$layouts[$name]['options'] = $current . '/options.json';
			}
		}

		return ( count( $layouts ) > 0 ? $layouts : false );
	}

	/**
	 * Start import
	 *
	 * @access public
	 */
	public function import( ) {
		$current = array( );
		$layouts = $this->__parse_layouts( );
		$key     = sanitize_key( $_REQUEST['layout'] );

		if ( ! is_array( $layouts ) or ! array_key_exists( $key, $layouts ) ) {
			die( );
		} else {
			$current = &$layouts[$key];
		}

		// Content
		$this->__import_content( $current['content'] );

		// Widgets
		if ( isset( $current['widgets'] ) ) {
			$this->__import_widgets( $current['widgets'] );
		}

		// Theme options
		if ( isset( $current['options'] ) ) {
			$this->__import_options( $current['options'] );
		}

		die( );
	}

	/**
	 * Import WordPress content
	 *
	 * @param  string $filepath
	 * @access private
	 */
	private function __import_content( $filepath ) {
		if ( ! defined( 'WP_LOAD_IMPORTERS' ) ) {
			define( 'WP_LOAD_IMPORTERS', true );
		}

		require plugin_dir_path( __FILE__ ) . '../../vendor/wordpress-importer/wordpress-importer.php';

		$wp_import = new WP_Import( );
		$wp_import->fetch_attachments = true;
		$wp_import->import( $filepath );
	}

	/**
	 * Import theme options
	 *
	 * @param  string $filepath
	 * @return boolean
	 * @access private
	 */
	private function __import_options( $filepath ) {
		$body = $this->__plugin_instance->get_file_body( $filepath );

		if ( $body === false ) {
			return false;
		}

		$options = json_decode( $body, true );

		if ( ! is_array( $options ) ) {
			return false;
		}

		$theme      = wp_get_theme( )->get_stylesheet( );
		$theme_mods = get_option( 'theme_mods_' . $theme, array( ) );

		return update_option( 'theme_mods_' . $theme, array_merge( $theme_mods, $options ) );
	}

	/**
	 * Import widgets
	 *
	 * @param  string $filepath
	 * @access private
	 */
	private function __import_widgets( $filepath ) {
		$body = $this->__plugin_instance->get_file_body( $filepath );

		if ( $body === false ) {
			return false;
		}

		$data = json_decode( $body, true );

		if ( ! is_array( $data ) ) {
			return false;
		}

		$wp_widgets      = array( );
		$wp_sidebars     = get_option( 'sidebars_widgets', array( ) );
		$array_version   = $wp_sidebars['array_version'];
		$custom_sidebars = array( );

		unset( $wp_sidebars['array_version'] );

		foreach ( $wp_sidebars as $sidebar => $widgets ) {
			if ( is_array( $widgets ) and count( $widgets ) > 0 ) {
				foreach ( $widgets as $widget ) {
					$delemiter = strrpos( $widget, '-' );
					$id_base   = substr( $widget, 0, $delemiter );

					$wp_widgets[$id_base] = get_option( 'widget_' . $id_base );
				}
			}
		}

		foreach ( $data as $sidebar => $widgets ) {
			if ( isset( $wp_sidebars[$sidebar] ) and count( $wp_sidebars[$sidebar] ) > 0 ) {
				foreach ( $wp_sidebars[$sidebar] as $widget ) {
					$delemiter = strrpos( $widget, '-' );
					$id_base   = substr( $widget, 0, $delemiter );
					$key       = substr( $widget, $delemiter + 1 );

					unset( $wp_widgets[$id_base][$key] );
				}
			}
		}

		foreach ( $data as $sidebar => $widgets ) {
			$wp_sidebars[$sidebar] = array( );

			if ( ! isset( $GLOBALS['wp_registered_sidebars'][$sidebar] ) and $sidebar != 'wp_inactive_widgets' ) {
				$custom_sidebars[] = $sidebar;
			}

			foreach ( $widgets as $widget => $options ) {
				$key = $this->__get_widget_key( $widget, $wp_widgets );

				$wp_sidebars[$sidebar][] = $widget . '-' . $key;
				$wp_widgets[$widget][$key] = $options;
			}
		}

		$wp_sidebars['array_version'] = $array_version;

		if ( count( $custom_sidebars ) > 0 ) {
			$sidebars = get_option( Rocks_Tools_Custom_Sidebars::get_stored_key( ), array( ) );

			if ( count( $sidebars ) > 0 ) {
				foreach ( $custom_sidebars as $sidebar ) {
					if ( ! in_array( $sidebar, $sidebars ) ) {
						$sidebars[] = $sidebar;
					}
				}
			} else {
				$sidebars = $custom_sidebars;
			}

			update_option( Rocks_Tools_Custom_Sidebars::get_stored_key( ), $sidebars );
		}

		foreach ( $wp_widgets as $id_base => &$options ) {
			if ( isset( $options['_multiwidget'] ) ) {
				unset( $options['_multiwidget'] );
			}

			$options['_multiwidget'] = 1;

			update_option( 'widget_' . $id_base, $options );
		}

		return update_option( 'sidebars_widgets', $wp_sidebars );
	}

	/**
	 * Get the widget unical key
	 *
	 * @param  string $widget
	 * @param  array $wp_widgets
	 * @return int
	 * @access private
	 */
	private function __get_widget_key( $widget, $wp_widgets ) {
		unset( $wp_widgets[$widget]['_multiwidget'] );

		if ( ! isset( $wp_widgets[$widget] ) or count( $wp_widgets[$widget] ) < 1 ) {
			return 2;
		}

		end( $wp_widgets[$widget] );

		return key( $wp_widgets[$widget] ) + 1;
	}

	/**
	 * Export widgets
	 *
	 * @return string
	 * @access private
	 */
	private function __export_widgets( ) {
		$export   = array( );
		$sidebars = get_option( 'sidebars_widgets', array( ) );

		if ( count( $sidebars ) < 1 ) {
			return false;
		}

		foreach ( $sidebars as $sidebar => $widgets ) {
			if ( ! is_array( $widgets ) or count( $widgets ) < 1 ) {
				continue;
			}

			$export[$sidebar] = array( );

			foreach ( $widgets as $key => $name ) {
				$posi = strrpos( $name, '-' );
				$id   = substr( $name, $posi + 1 );
				$name = substr( $name, 0, $posi );

				$widget = get_option( 'widget_' . $name );

				if ( ! isset( $widget[$id] ) ) {
					continue;
				} else {
					$widget = $widget[$id];
				}

				$export[$sidebar][$name] = $widget;
			}
		}

		return json_encode( $export );
	}

	/**
	 * Export options
	 *
	 * @return string
	 * @access private
	 */
	private function __export_options( ) {
		$theme      = wp_get_theme( )->get_stylesheet( );
		$theme_mods = get_option( 'theme_mods_' . $theme, array( ) );

		return json_encode( $theme_mods );
	}

	/**
	 * Output import page content
	 *
	 * @access public
	 */
	public function page_content( ) {
		$list    = '';
		$layouts = $this->__parse_layouts( );

		if ( is_array( $layouts ) and count( $layouts ) > 0 ) {
			foreach ( $layouts as $id => $details ) {
				$list .= '
				<div class="rocks-tools-demo-importer-template">
					' . ( isset( $details['screenshot'] ) ? '<img src="' . esc_url( $details['screenshot'] ) . '" alt="' . __( 'Preview', 'rocks' ) . '">' : '' ) . '
					<label>
						<input type="radio" name="layout" value="' . esc_attr( $id ) . '">
						<span>' . esc_html( $details['title'] ) . '</span>
					</label>
				</div>';
			}
		} else {
			echo '
			<div class="wrap">
				<h2>' . __( 'Import Demo Content',' rocks' ) . '</h2>
				<div class="notice error below-h2 rocks-tools-demo-importer-notice">
					<p>' . __( 'Oops! Layouts not found.', 'rocks' ) . '</p>
				</div>
			</div>';

			die( );
		}

		$export = '
		<div style="margin-top:20px">
			<h2>' . __( 'Export Settings',' rocks' ) . '</h2>
			<div class="notice below-h2" style="margin-top:20px;padding-bottom:5px">
				<h4>' . __( 'Widgets', 'rocks' ) . '</h4>
				<p>' . $this->__export_widgets( ) . '</p>
				<h4>' . __( 'Theme options', 'rocks' ) . '</h4>
				<p>' . $this->__export_options( ) . '</p>
			</div>
		</div>';

		echo '
		<div class="wrap">
			<h2>' . __( 'Select a Demo Layout', 'rocks' ) . '</h2>
			<div class="rocks-tools-demo-importer">
				' . $list . '
			</div>
			<div class="clear"></div>

			<h2>' . __( 'Import Demo Content',' rocks' ) . '</h2>
			<div class="notice below-h2 rocks-tools-demo-importer-notice">
				<h4>' . __( 'Please read the notice below before proceeding further:', 'rocks' ) . '</h4>
				<ul>
					<li>' . __( 'If you import the same file twice, menu items or any other data may be duplicated', 'rocks' ) . '</li>
					<li>' . __( 'No existing posts, categories, images, tags, widgets or any other data will be created or modified', 'rocks' ) . '</li>
					<li>' . __( 'Some images and videos will be downloaded from our server, these images are copyrighted and are for demo use only', 'rocks' ) . '</li>
				</ul>
			</div>

			<input type="button" id="rocks_tools_demo_importer_button" value="' . __( 'Import', 'rocks' ) . '" class="button button-primary button-large button-disabled">
			<input type="hidden" name="current" id="rocks_tools_demo_importer_current" value="">

			<div class="notice below-h2 rocks-tools-demo-importer-process">
				<img src="' . site_url( ) . '/wp-admin/images/spinner.gif" alt="">
				<span>' . __( 'Please be patient while we are importing data, this process may take a couple of minutes', 'rocks' ) . '</span>
			</div>

			' . ( $this->__allow_export ? $export : '' ) . '
		</div>';
	}
}

// Initialization
add_action( 'rocks_tools_loaded', array( 'Rocks_Tools_Demo_Importer', 'instance' ) );
