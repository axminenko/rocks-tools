/*!
 * Create.Rocks Tools | Version 0.1.0 | https://create.rocks
 * License:   http://www.gnu.org/licenses/gpl-2.0.html
 * Copyright: 2014 - 2016 Create.Rocks
 */
var pkg          = require( './package.json' );
var gulp         = require( 'gulp' ),
    rename       = require( 'gulp-rename' ),
    replace      = require( 'gulp-replace' ),
    uglify       = require( 'gulp-uglify' ),
    cleancss     = require( 'gulp-clean-css' ),
    autoprefixer = require( 'gulp-autoprefixer' ),
    jshint       = require( 'gulp-jshint' ),
    imagemin     = require( 'gulp-imagemin' ),
    plumber      = require( 'gulp-plumber' ),
    notify       = require( 'gulp-notify' ),
    zip          = require( 'gulp-zip' ),
    rmdir        = require( 'rmdir' ),
    runsequence  = require( 'run-sequence' );

// Scripts
gulp.task( 'scripts', function( ) {
    return gulp.src( [
        'components/**/*.js',
        '!components/**/*.min.js',
    ] )
    .pipe( plumber( {
       errorHandler: notify.onError( 'Something happend with scripts' )
    } ) )
    .pipe( jshint( ) )
    .pipe( jshint.reporter( 'jshint-stylish' ) )
    .pipe( uglify( {
       preserveComments: 'license',
    } ) )
    .pipe( rename( {
       suffix: '.min',
    } ) )
    .pipe( gulp.dest( 'components/' ) );
} );

// Styles
gulp.task( 'styles', function( ) {
    return gulp.src( [
        'components/**/*.css',
        '!components/**/*.min.css',
    ] )
    .pipe( plumber( {
       errorHandler: notify.onError( 'Something happend with styles' )
    } ) )
    .pipe( autoprefixer( {
       browsers: [ 'last 1 version' ],
    } ) )
    .pipe( cleancss( ) )
    .pipe( rename( {
       suffix: '.min',
    } ) )
    .pipe( gulp.dest( 'components/' ) );
} );

// Images
gulp.task( 'images', function( ) {
    return gulp.src( [
        'components/**/*.{jpg,png,gif}',
    ] )
    .pipe( imagemin( {
       optimizationLevel: 3,
       progressive: true,
       interlaced: true,
    } ) )
    .pipe( gulp.dest( 'components/' ) );
} );

// Version
gulp.task( 'version', function( ) {
    return gulp.src( [
        '**/*.php',
        '**/*.js',
        'readme.txt',
        '!**/jquery.livequery.min.js',
        '!**/vendor{,/**}',
        '!node_modules{,/**}',
    ] )
    .pipe( replace( /(Version:?\s*)([0-9.]+)/, '$1' + pkg.version ) ) // Version: x.x.x
    .pipe( replace( /(\|?\s*Version?\s*)([0-9.]+)/, '$1' + pkg.version ) ) // | Version x.x.x
    .pipe( replace( /(@version?\s*)([0-9.]+)/, '$1' + pkg.version ) ) // @version x.x.x
    .pipe( replace( /(\$__version?\s*=?\s*['"])([0-9.]+)/, '$1' + pkg.version ) ) // $__version = "x.x.x"
    .pipe( replace( /(Stable\stag:?\s*)([0-9.]+)/, '$1' + pkg.version ) ) // Stable tag: x.x.x
    .pipe( gulp.dest( '.' ) );
} );

// Watch
gulp.task( 'watch', function( ) {
    gulp.watch( [
        'components/**/*.js',
        '!components/**/*.min.js',
    ], [
        'scripts',
    ] );

    gulp.watch( [
        'components/**/*.css',
        '!components/**/*.min.css',
    ], [
        'styles',
    ] );

    gulp.watch( [
        'components/**/*.{jpg,png,gif}',
    ], [
        'images',
    ] );
} );

// Archive: Step 1
gulp.task( 'archive_preparing', function( ) {
    return gulp.src( [
        '**/*',
        '!*.zip',
        '!languages/default.mo',
        '!gulpfile.js',
        '!package.json',
        '!node_modules{,/**}',
    ] )
    .pipe( gulp.dest( '.build/' + pkg.name + '/' ) );
} );

// Archive: Step 2
gulp.task( 'archive_creating', function( ) {
    return gulp.src( [
        '.build/**',
    ] )
    .pipe( zip( pkg.name + '-' + pkg.version + '.zip' ) )
    .pipe( gulp.dest( './' ) );
} );

// Archive: Step 3
gulp.task( 'archive_cleaning', function( ) {
    return rmdir( '.build/' );
} );

// Archive
gulp.task( 'archive', function( ) {
    return runsequence( 'archive_preparing', 'archive_creating', 'archive_cleaning' );
} );

// Build
gulp.task( 'build', function( ) {
    return runsequence( 'version', [
        'scripts',
        'styles',
        'images',
    ], 'archive' );
} );

// Default
gulp.task( 'default', [
    'version',
    'scripts',
    'styles',
    'images',
] );
